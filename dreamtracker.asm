; The main application which is responsible for calling different modules

.include "includes.inc"

;.segment "RODATA"
.segment "CODE"

; Initial run once stuff
start:
.IF CPU_65816
  ; Enable ca65 to track 8/16-bit switching
  .smart
  ; Just in case it's not that smart
  ;.a8
  ;.i8
  ; Set native mode
  native

  ; Set 8-bit
  ; Commented out since this is the default
  ; sep #P_MEMORY_AND_INDEX ; put the 65C816 in 8-bit accumulator and index mode
.ENDIF

	; Loading / Setup
	ldx #LOADING_MODULE
	lda module_filename_lengths_jump_table,x
	sta zp_ARG0
	lda module_filenames_jump_table_lo,x
	sta zp_ARG1
	lda module_filenames_jump_table_hi,x
	sta zp_ARG2
	jsr files::load_module
	jsr MODULE_INIT

	stz help_submodule
	ldx #HELP_MODULE
	jmp init_jump

; Main module
main:
main_application_loop:
	cli
	wai
; Check for input from the keyboard, and do something if
; a key was pressed (return value is something other than 0)
check_keyboard:
	;sei
	; call ps2data_fetch
	lda #$08
	jsr extapi
	jsr SCNKEY
	jsr GETIN  ;keyboard
	beq main_application_loop
	sta zp_KEY_PRESSED
	;cli
; Check global keys - these keys do the same thing for all modules
check_global_keys:
; Help menu
@f1_key:
	cmp #F1
	bne @f2_key
	ldx #HELP_MODULE
	;bra init_jump
	jmp init_jump
; Edit Pattern Module
@f2_key:
	cmp #F2
	bne @f5_key
	ldx #EDIT_PATTERN_MODULE
	bra init_jump
; Play full song
@f5_key:
	cmp #F5
	bne @f10_key
	stz zp_ROW_NUMBER
	stz zp_ORDER_NUMBER
	ldx #PLAY_SONG_MODULE
	bra init_jump
; Load/Save
@f10_key:
	cmp #F10
	bne @f11_key
	ldx #FILE_MODULE
	bra init_jump
; Orders
@f11_key:
	cmp #F11
	bne @f12_key
	ldx #ORDERS_MODULE
	bra init_jump
; Song Options
@f12_key:
	cmp #F12
	bne @f3_key
	ldx #SONG_OPTIONS_MODULE
	bra init_jump
; Samples
@f3_key:
	cmp #F3
	bne @f4_key
	ldx #SAMPLES_MODULE
	bra init_jump
; Vera / FM Instruments
@f4_key:
	cmp #F4
	bne @f9_key
	bra @instrument_menu_jump
; Envelopes
@f9_key:
	cmp #F9
	bne @f8_key
	bra @envelopes_tables_macros_menu_jump
; Stop song (doesn't change module)
@f8_key:
	cmp #F8
	bne @alt_q
	jsr tracker::stop_song
	bra main_application_loop
@alt_q:
	cmp #ALT_Q
	bne @module_loop
	jmp exit

; If we're in the VERA instruments module and we press F4 again
; switch to FM instruments
@instrument_menu_jump:
	ldx zp_CURRENT_MODULE
	cpx #VERA_INSTRUMENTS_MODULE
	beq @goto_fm_module
@goto_vera_module:
	ldx #VERA_INSTRUMENTS_MODULE
	bra init_jump
@goto_fm_module:
	ldx #FM_INSTRUMENTS_MODULE
	bra init_jump

; If we're in the Evenlopes instruments module and we press F9 again
; switch to FM instruments
@envelopes_tables_macros_menu_jump:
	ldx zp_CURRENT_MODULE
	cpx #ENVELOPES_MODULE
	; Disabled for now since it's not done yet
	;beq @goto_tables_module
	;cpx #TABLES_MODULE
	beq @goto_macros_module
@goto_envelopes_module:
	ldx #ENVELOPES_MODULE
	bra init_jump
@goto_tables_module:
	ldx #TABLES_MODULE
	bra init_jump
@goto_macros_module:
	ldx #MACROS_MODULE
	bra init_jump

; If no global key was pressed, we pass control to the current module's loop
@module_loop:
	jmp (MODULE_LOOP)

; When we change modules, we need to run some init code, but only once for
; each module. Thereafter we just need to loop for keyboard
; which is handled in the loop_jump_table
init_jump: 
	stx zp_CURRENT_MODULE
	bra load_module

; Jump to the loop of the current module
loop_jump:
	jmp (MODULE_LOOP)
	;ldx zp_CURRENT_MODULE
	;jmp (loop_jump_table,x)


.proc exit
; Exit the program
exit:
	; Restore ROM bank 7
	;rombank #$04
	;clc
	;jmp ENTER_BASIC
	; Reboot the system via the SMC
	ldx #$42  ; System Management Controller
	ldy #$02  ; magic location for system reset
	lda #$00  ; magic value for system poweroff
	jmp i2c_write_byte ; reset system
.endproc

; See data.inc and variables.inc for module #'s and filenames
; x = module
.proc load_module
	jsr graphics::drawing::cursor_unplot
	lda module_filename_lengths_jump_table,x
	sta zp_ARG0
	lda module_filenames_jump_table_lo,x
	sta zp_ARG1
	lda module_filenames_jump_table_hi,x
	sta zp_ARG2

	jsr files::load_module

	jmp (MODULE_INIT)
	rts
	.endproc