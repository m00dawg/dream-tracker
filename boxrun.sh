#!/bin/bash

ROM="rom.bin"
SCALE=2

make
#./x16emu -rom $ROM -scale $SCALE -prg DREAMTRK.PRG -ram 2048 -run
./mount-sd.sh
sudo cp -a *.PRG SCR SAMPLES SONGS DAT /mnt
./unmount-sd.sh
./box16 -rom $ROM -scale $SCALE -prg DT.PRG -ram 2048 -sdcard sdcard.img -run -nobinds -scale 2 
