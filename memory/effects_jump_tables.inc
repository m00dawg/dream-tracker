.pushseg
.segment "RODATA"
vera_shared_effects_jump_table:
  .word sound::vera::effects::pitch_slide_down
  .word sound::vera::effects::pitch_slide_up
  .word sound::vera::effects::pitch_sweep_down
  .word sound::vera::effects::pitch_sweep_up
  .word sound::vera::effects::volume_slide
  .word sound::vera::effects::volume_sweep
  .word sound::vera::effects::glide
  .word sound::vera::effects::legato
  .word sound::vera::effects::vibrato
  .word sound::vera::effects::tremolo
  .word sound::vera::effects::arpeggio
  .word sound::vera::effects::arp_dirspeed
  .word tracker::effects::null_effect   ; Panolo
  .word tracker::effects::null_effect   ; Vol
  .word sound::vera::effects::pan
  .word tracker::effects::null_effect   ; Note Cut
  .word sound::vera::effects::note_delay
  .word sound::vera::effects::channel_attenuation ; $11
  .word sound::vera::effects::reset_channel
  .word tracker::effects::null_effect   ; Set Table
  .word tracker::effects::null_effect   ; Set Table Speed
  .word tracker::effects::null_effect   ; Set Table Loop Mode
  .word tracker::effects::null_effect   ; Run Macro

; Pattern effect start value is $44
vera_unique_effects_jump_table:
  .word sound::vera::effects::pwm_set_value
  .word sound::vera::effects::pwm_slide
  .word sound::vera::effects::pwm_sweep
  .word sound::vera::effects::pulsolo
  .word sound::vera::effects::finetune
  .word sound::vera::effects::linear_finetune
  .word sound::vera::effects::pitch_speed
  .word tracker::effects::null_effect   ; Pitch speed multi
  .word tracker::effects::null_effect   ; Waveform
  .word tracker::effects::null_effect   ;
  .word tracker::effects::null_effect   ;
  .word tracker::effects::null_effect   ; 
  .word tracker::effects::null_effect   ; vol env number
  .word sound::vera::effects::vol_env_speed   ; $51
  .word tracker::effects::null_effect   ; flags
  .word tracker::effects::null_effect   ; length
  .word tracker::effects::null_effect   ; loop start
  .word tracker::effects::null_effect   ; loop end
  .word tracker::effects::null_effect   ; wave
  .word sound::vera::effects::wave_env_speed   
  .word tracker::effects::null_effect   ; flags
  .word tracker::effects::null_effect   ; length
  .word tracker::effects::null_effect   ; loop start
  .word tracker::effects::null_effect   ; loop end
  .word tracker::effects::null_effect   ; pitch
  .word sound::vera::effects::pitch_env_speed

midi_shared_effects_jump_table:
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word sound::midi::effects::legato    ; $07
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word sound::midi::effects::channel_attenuation ; $11

midi_unique_effects_jump_table:
  .word sound::midi::effects::midi_channel
  .word sound::midi::effects::cc_param
  .word sound::midi::effects::cc_value
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word sound::midi::effects::sam_cutoff
  .word sound::midi::effects::sam_ressonance


fm_shared_effects_jump_table:
  .word sound::fm::effects::pitch_slide_down  ; $00
  .word sound::fm::effects::pitch_slide_up
  .word sound::fm::effects::pitch_sweep_down
  .word sound::fm::effects::pitch_sweep_up
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word sound::fm::effects::glide
  .word sound::fm::effects::legato
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect  
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word sound::fm::effects::note_delay    ; $10
  .word sound::fm::effects::channel_attenuation ; $11
  .word sound::fm::effects::reset_channel ; $12
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect
  .word tracker::effects::null_effect

; Starts at $5A
fm_unique_effects_jump_table:
  .word tracker::effects::null_effect ; Pan, Feedback, Algo
  .word tracker::effects::null_effect ; Mod Sense (Vib/Trem)
  .word sound::fm::effects::lfo_waveform
  .word sound::fm::effects::lfo_frequency
  .word sound::fm::effects::lfo_depth
  .word sound::fm::effects::noise_control
  .word tracker::effects::null_effect ; Global to Op

global_effects_jump_table:
  .word tracker::effects::stop_song
  .word tracker::effects::set_speed
  .word tracker::effects::pattern_break
  .word tracker::effects::jump_to_order
  .word tracker::effects::null_effect     ; jump to row
  .word tracker::effects::set_global_transpose
  .word tracker::effects::null_effect     ; Set BPM (VIA)
  .word tracker::effects::null_effect     ; Set Engine Speed (VIA/MIDI)
  .word tracker::effects::change_background_color_r    ; Change background Red
  .word tracker::effects::change_background_color_gb    ; Change background Green/Blue

.popseg