.pushseg
.segment "GOLDEN"

; Golden RAM also used in edit_pattern for saving block pattern data 
; across modules


help_submodule:    .byte $00

; Current Octave User Has Specified
user_octave:   .byte $00

key: .byte $00
cursor_x: .byte $00
cursor_y: .byte $00
cursor_color: .byte $00
cursor_old_color: .byte $00
cursor_layer: .byte $00
screen_x: .byte $00
screen_y: .byte $00

song_file_version: .byte $00

previous_isr_handler: .word $0000

; Entropy seed for RNG (e.g. randomizing instrument registers)
entropy_seed: .word $0000

.popseg