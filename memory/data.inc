.pushseg
.segment "RODATA"

; Module Filenames
LOADING_MODULE_FILENAME:
  .byte "dat/load.dat"
LOADING_MODULE_FILENAME_LENGTH = $0C

HELP_MODULE_FILENAME:
  .byte "dat/hm.dat"
HELP_MODULE_FILENAME_LENGTH = $0A

VERA_INSTRUMENTS_MODULE_FILENAME:
  .byte "dat/vim.dat"
VERA_INSTRUMENTS_MODULE_FILENAME_LENGTH = $0B

PLAY_SONG_MODULE_FILENAME:
  .byte "dat/psm.dat"
PLAY_SONG_MODULE_FILENAME_LENGTH = $0B

PATTERN_EDITOR_MODULE_FILENAME:
  .byte "dat/pem.dat"
PATTERN_EDITOR_MODULE_FILENAME_LENGTH = $0B

FILE_MODULE_FILENAME:
  .byte "dat/fm.dat"
FILE_MODULE_FILENAME_LENGTH = $0A

ORDERS_MODULE_FILENAME:
  .byte "dat/om.dat"
ORDERS_MODULE_FILENAME_LENGTH = $0A

TABLES_MODULE_FILENAME:
  .byte "dat/tm.dat"
TABLES_MODULE_FILENAME_LENGTH = $0A

MACROS_MODULE_FILENAME:
  .byte "dat/mm.dat"
MACROS_MODULE_FILENAME_LENGTH = $0A

ENVELOPES_MODULE_FILENAME:
  .byte "dat/envm.dat"
ENVELOPES_MODULE_FILENAME_LENGTH = $0C

SAMPLES_MODULE_FILENAME:
  .byte "dat/sm.dat"
SAMPLES_MODULE_FILENAME_LENGTH = $0A

FM_INSTRUMENTS_MODULE_FILENAME:
  .byte "dat/fim.dat"
FM_INSTRUMENTS_MODULE_FILENAME_LENGTH = $0B

SONG_OPTIONS_MODULE_FILENAME:
  .byte "dat/som.dat"
SONG_OPTIONS_MODULE_FILENAME_LENGTH = $0B

module_filenames_jump_table_lo:
  .byte <LOADING_MODULE_FILENAME
  .byte <HELP_MODULE_FILENAME
  .byte <VERA_INSTRUMENTS_MODULE_FILENAME
  .byte <PLAY_SONG_MODULE_FILENAME
  .byte <PATTERN_EDITOR_MODULE_FILENAME
  .byte <FILE_MODULE_FILENAME
  .byte <ORDERS_MODULE_FILENAME
  .byte <TABLES_MODULE_FILENAME
  .byte <ENVELOPES_MODULE_FILENAME
  .byte <SAMPLES_MODULE_FILENAME
  .byte <FM_INSTRUMENTS_MODULE_FILENAME
  .byte <SONG_OPTIONS_MODULE_FILENAME
  .byte <MACROS_MODULE_FILENAME

module_filenames_jump_table_hi:
  .byte >LOADING_MODULE_FILENAME
  .byte >HELP_MODULE_FILENAME
  .byte >VERA_INSTRUMENTS_MODULE_FILENAME
  .byte >PLAY_SONG_MODULE_FILENAME
  .byte >PATTERN_EDITOR_MODULE_FILENAME
  .byte >FILE_MODULE_FILENAME
  .byte >ORDERS_MODULE_FILENAME
  .byte >TABLES_MODULE_FILENAME
  .byte >ENVELOPES_MODULE_FILENAME
  .byte >SAMPLES_MODULE_FILENAME
  .byte >FM_INSTRUMENTS_MODULE_FILENAME
  .byte >SONG_OPTIONS_MODULE_FILENAME
  .byte >MACROS_MODULE_FILENAME

module_filename_lengths_jump_table:
  .byte LOADING_MODULE_FILENAME_LENGTH
  .byte HELP_MODULE_FILENAME_LENGTH
  .byte VERA_INSTRUMENTS_MODULE_FILENAME_LENGTH
  .byte PLAY_SONG_MODULE_FILENAME_LENGTH
  .byte PATTERN_EDITOR_MODULE_FILENAME_LENGTH
  .byte FILE_MODULE_FILENAME_LENGTH
  .byte ORDERS_MODULE_FILENAME_LENGTH
  .byte TABLES_MODULE_FILENAME_LENGTH
  .byte ENVELOPES_MODULE_FILENAME_LENGTH
  .byte SAMPLES_MODULE_FILENAME_LENGTH
  .byte FM_INSTRUMENTS_MODULE_FILENAME_LENGTH
  .byte SONG_OPTIONS_MODULE_FILENAME_LENGTH
  .byte MACROS_MODULE_FILENAME_LENGTH

; Override the 16 color palette
; gb,r


; 0RGB
palette:
  .word $0001     ; super dark blue
  .word $0FFF     ; white
  .word $0F00     ; red
  .word $048F     ; cyan
  .word $0A06     ; purple
  .word $0040     ; dark green
  .word $052F     ; blue
  .word $0FF0     ; yellow
  .word $0F50     ; orange
  .word $0333     ; slightly lighter grey (darker highlights)
  .word $0F55     ; light red
  .word $0112     ; dark bluish grey (normally the background of the top layer)
  .word $0555     ; grey
  .word $05F5     ; light green
  .word $0A9F     ; light blue
  .word $0888     ; light gray


; Note value to name lookup
; Starting from C (MIDI note 0 = C)
note_names:  .byte "ccddeffggaab-^-."
note_sharps: .byte "-#-#--#-#-#--^-."

; If starting from A
;note_names:   .byte "aabccddeffgg-^-."
;note_sharps:  .byte "-#--#-#--#-#-^-."


; Screencode to note number
; Note, if the actual note has a #, we add 1 to the value
; within the code and do not account for it here.
; The index is the screencode 
; (https://sta.c64.org/cbm64scr.html)
; So C = 1, D = 2, etc. and then we return the numerical
; note we want to actually use
screencode_to_note:
      ; NUL, A    B    C    D    E    F    G
  .byte $FF, $09, $0B, $00, $02, $04, $05, $07
  ; If starting from A
  ;.byte $FF, $00, $02, $03, $05, $07, $08, $0A

; PETSCII to pattern note lookup
; Subtract $30 from PETSCII code
; Map the PETSCII values to the pattern storage format
; (oct, note #)
; The array index is the $30 offset.
; '0' in PETSCII is $30, so if we subtract 30 by 30, we 
; get the first index of the array (which is D#2)
; This returns MIDI values (C-0 = 00) which are raw
; notes and do not have the note/oct split like before.
; FF = NOTENULL
; FE = NOTEOFF
; FD = NOTEREL
petscii_to_note:
  ; Numbers 
  ;     0    1    2    3    4    5    6    7    8    9
  ;     D#2, OFF, C#1, D#1, NOP, F#1, G#1, A#1, NOP, C#2
  .byte $1B, $FE, $0D, $0F, $FF, $12, $14, $16, $FF, $19
  
  ; Unused Keys (:, ;, ... 'a')
  .byte $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
  
  ; 'b' through 'l'
  ;     b    c    d    e    f    g    h    i    j    k    l
  ;     G-0, E-0, D#0, E-1, NOP, F#0, G#0, C-2, A#0, NOP, NOP
  .byte $07, $04, $03, $10, $FF, $06, $08, $18, $0A, $FF, $FF
  
  ; 'm' through 'w'
  ;     m    n    o    p    q    r    s    t    u    v    w
  ;     B-0, A-0, D-2, E-2, C-1, F-1, C#0, G-1, B-2, F-0, D-1
  .byte $0B, $09, $1A, $1C, $0C, $11, $01, $13, $17, $05, $0E
  
  ; 'x-z, special chars, then left arrow
  ;     x    y    z    NOP  Pound  NOP  NOP  
  ;     D-0, A-1, C-0, NOP, NOP, NOP, NOP, REL
  .byte $02, $15, $00, $FD, $FD, $FD, $FD, $FD

; This is the old way when splitting note/oct
; before just storing the raw MIDI value
;petscii_to_note:
  ; Numbers 
  ;     0    1    2    3    4    5    6    7    8    9
  ;     D#2, OFF, C#1, D#1, NOP, F#1, G#1, A#1, NOP, C#2
  ;.byte $23, $0E, $11, $13, $FF, $16, $18, $1A, $FF, $21
  ; Unused Keys (:, ;, ... 'a')
  ;.byte $00, $00, $00, $00, $00, $00, $00, $00
  ; 'b' through 'l'
  ;     b    c    d    e    f    g    h    i    j    k    l
  ;     G-0, E-0, D#0, E-1, NOP, F#0, G#0, C-2, A#0, NOP, NOP
  ;.byte $07, $04, $03, $14, $FF, $06, $08, $20, $0A, $FF, $FF
  ; 'm' through 'w'
  ;     m    n    o    p    q    r    s    t    u    v    w
  ;     B-1, A-1, D-2, E-2, C-1, F-1, C#0, G-1, B-1, F-0, D-1
  ;.byte $1B, $19, $22, $24, $10, $15, $01, $17, $1B, $05, $12
  ; 'x-z, special chars, then left arrow
  ;     x    y    z    NOP  NOP  NOP  NOP  
  ;     D-0, A-1, C-0, NOP, NOP, NOP, NOP, REL
  ;.byte $02, $19, $00, $FF, $FF, $FF, $FF, $0D

.segment "DATA"

; When we need to reference the OS filename which includes the
; @: to indicate file overwriting
full_filename:   .byte "@:songs/"
filename:        .byte "                  ",0


;.incbin "./timing.dat"


.popseg