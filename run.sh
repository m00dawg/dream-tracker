#!/bin/bash

PRG="DT.PRG"
ROM="rom.bin"
SCALE=2
SPEED=8
#RAM=512
RAM=2048
#CPU="-c816" 
CPU="-c02" 

make
#./x16emu -rom $ROM -scale $SCALE -prg $PRG  -abufs 16 -run -mhz $SPEED -ram $RAM $CPU
#./x16emu -rom $ROM -scale $SCALE -prg $PRG  -abufs 16 -run -mhz $SPEED -ram $RAM $CPU -midicard 9FC0 -sf2 8MBGMSFX.SF2
./x16emu -rom $ROM -scale $SCALE -prg $PRG  -abufs 16 -run -mhz $SPEED -ram $RAM $CPU -midicard 9FC0 -sf2 UHD3.sf2

