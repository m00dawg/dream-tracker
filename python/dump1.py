#!/usr/bin/env python

# A program which outputs a Dream Tracker save file
# for version 2 of the saves

import cbmcodecs2

null_value = 255

note_alpha = [
    'C-', 'C#', 'D-', 'D#', 'E-', 'F-', 
    'F#', 'G-', 'G#', 'A-', 'A#', 'B-'
]

def note_to_text (note):
    if note == 0xFF:
        return "..."
    if note == 0xFE:
        return "==="
    if note == 0xFD:
        return "^^^"

    octave = int(note) // 12
    note_number = note - (octave * 12)
    return(note_alpha[note_number] + str(octave))
    
def print_effect (effect):
    if effect == 0xFF:
        return ".."
    return f'{effect:0{2}x}'

def print_hex(value):
    return f'{value:0{2}x}'


filename="SONGS/TEARS.DTR"

file = open(filename, "rb")

version = int.from_bytes(file.read(1), byteorder='little', signed=False)
song_speed = int.from_bytes(file.read(1), byteorder='little', signed=False)
title = file.read(16) 
composer = file.read(16)
orders = file.read(256)
envelopes = file.read(8192)
vera_instruments = file.read(8192)

# 8193 becuase the first byte is the pattern number
patterns = []
while(pattern := file.read(8193)):
    patterns.append(pattern)
file.close()


print("File Version: " + print_hex(version))
print("Song Title: " + title.decode('screencode_c64_uc'))
print("Composer: " + composer.decode('screencode_c64_uc'))
print("Speed: " + print_hex(song_speed))

print("Orders:")
for order in orders:
    print(print_hex(order) + " ", end="")
print()


print("Envelopes:")
count = 0
for i in range(32):
    print("Envelope " + print_hex(i))
    for j in range(256):
        print(print_hex(envelopes[count]) + " ", end=" ")
        count += 1
    print()

pattern_number = 0
for pattern in patterns:
    # This would be the final 0 byte after all the patterns have been read
    if len(pattern) == 1:
        print("End of Patterns")
    if len(pattern) == 8193:
        counter = 0
        print("Pattern: " + (print_effect(pattern[counter])))
        counter += 1
        for row in range(0x40):
            print(print_hex(row) + ": ", end="")
            for channel in range(25):
                # Note
                print(note_to_text(pattern[counter]), end=" ")
                counter += 1
                # Inst
                print(print_effect(pattern[counter]), end=" ")
                counter += 1          
                # Vol
                print(print_effect(pattern[counter]), end=" ")
                counter += 1
                # Effect Parameter
                print(print_effect(pattern[counter]), end="")
                counter += 1
                # Effect Value
                print(print_effect(pattern[counter]), end=" ")
                counter += 1
            print()
        pattern_number += 1



