; Update pitch sweep for channel, if required
; x = Channel

.proc update_pitch_sweep
    channel_pitch = EFFECT_TEMP1_VALUE
    sweep_value = EFFECT_TEMP2_VALUE
    channel_flags = EFFECT_TEMP3_VALUE
    result = EFFECT_TEMP4_VALUE
    channel = EFFECT_TEMP5_VALUE
    update_note = UPDATE_FM_NOTE

@start:
    ; Only update sweep if it's non-zero
    lda sound::fm::channel_pitch_sweep_value,x
    sta sweep_value
    bne @continue
    rts
@continue:
    stx channel
    lda sound::fm::channel_semitone,x
    sta channel_pitch
    lda sound::fm::channel_note,x
    sta channel_pitch + 1
@sweep_direction:
    lda sound::fm::channel_flags,x
    sta channel_flags
    ; Bit 5 of channel flags is pitch (see sound.asm)
    bbr 5, channel_flags, @sweep_down
@sweep_up:
    add16to8 channel_pitch, sweep_value, result
    bra @update_channel
@sweep_down:
    sub16from8 channel_pitch, sweep_value, result
@update_channel:
    lda result
    sta sound::fm::channel_semitone,x
    lda result + 1
    sta sound::fm::channel_note,x
@end:
    lda #$01
    sta update_note
    rts
.endproc
