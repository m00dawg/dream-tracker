; Update arpeggiator for channel, if required
; x = Channel


.proc update_arp
offset = EFFECT_TEMP1_VALUE
arp = EFFECT_TEMP2_VALUE
speed = EFFECT_TEMP3_VALUE


    ; Only update arp if it's non-zero
    lda sound::vera::channel_arp_value,x
    sta arp
    bne @arp_set
    rts
@arp_set:

; If speed is >0, we need to wait X ticks
@check_speed:
    lda sound::vera::channel_arp_dirspeed,x
    and #%00001111
    sta speed
    beq @play_arp
    inc sound::vera::channel_arp_speed_counter,x
    lda sound::vera::channel_arp_speed_counter,x
    cmp speed
    beq @speed_reached
    rts
@speed_reached:
    stz sound::vera::channel_arp_speed_counter,x

@play_arp:
    ; If channel_arp_position = 0, play base
    ; If 1 offset by top 4-bits
    ; If 2 offset by bottom 4-bits
    lda sound::vera::channel_arp_position,x
    cmp #$00
    beq @play_base
    cmp #$01
    beq @play_top_bits
    ; 02
@play_bottom_bits:
    ; Set back to 0 for next trigger
    ;lda #$00
    ;sta sound::vera::channel_arp_position,x
    lda arp
    and #%00001111
    bra @add_offset
@play_top_bits:
    ; Inc to 2 for next trigger
    ;inc sound::vera::channel_arp_position,x
    lda arp
    lsr
    lsr
    lsr
    lsr
    bra @add_offset

@play_base:
    ; Inc to 1 for next trigger
    ; inc sound::vera::channel_arp_position,x
    lda #$00 ; Set offset to 0 since we want to play the base note

; Add the offset
@add_offset:
    ;sta offset
    ;lda sound::vera::channel_arp_base_note,x
    clc
    adc sound::vera::channel_arp_base_note,x
    sta offset
; If glide is enabled, store the glide note instead
    lda sound::vera::channel_glide_speed,x
    bne @glide
@no_glide:
    lda offset
    sta sound::vera::channel_note,x
    bra @check_direction
@glide:
    lda offset
    sta sound::vera::channel_glide_note,x
    stz sound::vera::channel_glide_semitone,x

@check_direction:
    lda sound::vera::channel_arp_dirspeed,x
    and #%11110000
    bne @reverse
@forward:
    inc sound::vera::channel_arp_position,x
    lda sound::vera::channel_arp_position,x
    cmp #$03
    beq @forward_rollover
    rts
@forward_rollover:
    stz sound::vera::channel_arp_position,x
    rts

@reverse:
    dec sound::vera::channel_arp_position,x
    lda sound::vera::channel_arp_position,x
    cmp #$FF
    beq @reverse_rollover
    rts
@reverse_rollover:
    lda #$02
    sta sound::vera::channel_arp_position,x
    rts

@end:
    rts
.endproc