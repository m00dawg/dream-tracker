; Update pitch envelope for channel, if required
; not ready yet
; we need to use the full height of the envelope
; and do 2's compliment (for -/+ pitch)

; x = Channel
.proc update_pitch_env
    channel_pitch = EFFECT_TEMP0_VALUE
    glide_pitch = EFFECT_TEMP1_VALUE
    pitch_value = EFFECT_TEMP2_VALUE
    result = EFFECT_TEMP3_VALUE

    ; Only update pitch envelope if it's enabled
    lda sound::vera::channel_pitch_envelope_flags,x
    sta FLAGS
    bbs 7, FLAGS,@check_speed_nonzero
    rts

; If speed is >0, we need to wait X ticks
@check_speed_nonzero:
    lda sound::vera::channel_pitch_envelope_speed,x
    beq @check_end_of_env
; See if speed counter is zero. If so, check evalute env
; otherwise decrement
@evalute_speed:
    lda sound::vera::channel_pitch_envelope_speed_counter,x
    beq @speed_reached
    dec sound::vera::channel_pitch_envelope_speed_counter,x
    rts
@speed_reached:
    lda sound::vera::channel_pitch_envelope_speed,x
    sta sound::vera::channel_pitch_envelope_speed_counter,x


; If we aren't looping, check if we're at the end of the 
; specified length and stop
@check_end_of_env:
    ; Check if we've reached the 
    ; specified length to evaluate the env
    ; otherwise increment envelope
    lda sound::vera::channel_pitch_envelope_position,x
    ; If we're at the envelope length, do not progress further
    cmp sound::vera::channel_pitch_envelope_length,x
    blt @update_envelope
    rts

@update_envelope:
    sta ENV_POSITION_ADDRESS
    ; Add the env number to the BRAM base
    lda sound::vera::channel_pitch_envelope_num,x
    adda #BANK_ADDRESS_TOP
    sta ENV_POSITION_ADDRESS + 1
    ; Load value at env # and position
    lda (ENV_POSITION_ADDRESS)

@check_pitch_speed:
    ; Shift by the sweep speed
    sta pitch_value
    cmp #$80    ; two's compliment
    bge @negative
@positive:
    stz pitch_value + 1
    bra @get_pitch_speed
; Sign-extend if negative
@negative:
    lda #$FF
    sta pitch_value + 1

@get_pitch_speed:
    lda sound::vera::channel_pitch_speed,x
    tay
    beq @fetch_channel_pitch
@pitch_speed_loop:
    asl16 pitch_value
    dey
    bne @pitch_speed_loop

@fetch_channel_pitch:
    lda sound::vera::channel_semitone,x
    sta channel_pitch
    lda sound::vera::channel_note,x
    sta channel_pitch + 1
    add16 channel_pitch, pitch_value, result
    lda result
    sta sound::vera::channel_semitone,x
    lda result + 1
    sta sound::vera::channel_note,x

; This is so if we're also gliding, we adjust the glide in the same way as the main
; pitch
@fetch_glide_pitch:
    lda sound::vera::channel_glide_semitone,x
    sta glide_pitch
    lda sound::vera::channel_glide_note,x
    sta glide_pitch + 1
    add16 glide_pitch, pitch_value, result
    lda result
    sta sound::vera::channel_glide_semitone,x
    lda result + 1
    sta sound::vera::channel_glide_note,x

; Evaluate for next pass
@evaluate_next_value:
; Check if loop is enabled
@check_loop_enable:
    lda sound::vera::channel_pitch_envelope_flags,x
    sta FLAGS
    bbr 6, FLAGS, @inc_envelope
; Check if note-rel
@loop_enabled_check_note_rel:
    lda sound::vera::channel_flags,x
    sta FLAGS
    bbs 6, FLAGS, @inc_envelope
; Loop is enabled and we're not in a note release 
; Check if forward only or ping-pong
@check_loop_type:
    lda sound::vera::channel_pitch_envelope_flags,x
    sta FLAGS
    bbr 5, FLAGS, @forward_loop
; Ping-pong loop
@ping_pong_loop:
    ; Check loop direction (0 = forward, 1 = reverse)
    lda sound::vera::channel_pitch_envelope_flags,x
    sta FLAGS
    bbs 4, FLAGS, @ping_pong_reverse
@ping_pong_forward:
    lda sound::vera::channel_pitch_envelope_position,x
    cmp sound::vera::channel_pitch_envelope_loop_end,x
    beq @ping_pong_reset_backwards
    bra @inc_envelope 
@ping_pong_reset_backwards:
    lda sound::vera::channel_pitch_envelope_flags,x
    ora #%00010000
    sta sound::vera::channel_pitch_envelope_flags,x
    lda sound::vera::channel_pitch_envelope_position,x
    rts
@ping_pong_reverse:
    lda sound::vera::channel_pitch_envelope_position,x
    cmp sound::vera::channel_pitch_envelope_loop_start,x
    beq @ping_pong_reset_forwards
    dec sound::vera::channel_pitch_envelope_position,x
    rts
@ping_pong_reset_forwards:
    lda sound::vera::channel_pitch_envelope_flags,x
    and #%11101111
    sta sound::vera::channel_pitch_envelope_flags,x
    lda sound::vera::channel_pitch_envelope_position,x
    rts
; Forward only loop
@forward_loop:
    ; For now we only do forward, so if we reach the end
    ; of the loop end, set position to loop start
    lda sound::vera::channel_pitch_envelope_position,x
    cmp sound::vera::channel_pitch_envelope_loop_end,x
    beq @reset_loop
    bra @inc_envelope
@reset_loop:
    lda sound::vera::channel_pitch_envelope_loop_start,x
    sta sound::vera::channel_pitch_envelope_position,x
    rts


@inc_envelope:
    inc sound::vera::channel_pitch_envelope_position,x
@end:
    rts

.endproc