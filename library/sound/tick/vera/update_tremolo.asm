; Update tremolo for channel, if required
; x = Channel
.proc update_tremolo
volume = EFFECT_TEMP1_VALUE
tremolo_depth = EFFECT_TEMP2_VALUE
length = EFFECT_TEMP3_VALUE

@start:
    ; Only update tremolo if depth is non-zero
    lda sound::vera::channel_tremolo_depth,x
    bne @get_registers
    rts

; Get current tremolo and depth for later on
@get_registers:
    and #%00111111
    sta tremolo_depth
    lda sound::vera::channel_vol,x
    and #%00111111
    sta volume

@evaluate_tremolo:
; Look at the direction
; if 0, ramp up (add)
; if 1, ramp down (subtract)
    lda sound::vera::channel_tremolo_direction,x
    bne @subtract_volume
@add_volume:
    add volume, tremolo_depth
    cmp #%00111111
    bge @set_add_max
    bra @store_volume
@set_add_max:
    lda #%00111111
    bra @store_volume
@subtract_volume:
    sub volume, tremolo_depth
    bmi @set_sub_min
    bra @store_volume
@set_sub_min:
    lda #%00000000
    ;bra @store_volume
@store_volume:
    sta volume
    lda sound::vera::channel_vol,x
    and #%11000000
    ora volume 
    sta sound::vera::channel_vol,x
@update_loop_position:
    lda sound::vera::channel_tremolo_loop_position,x
    cmp sound::vera::channel_tremolo_loop_length,x
    bne @store_loop_position
@flip_direction:
    lda sound::vera::channel_tremolo_direction,x
    eor #%10000000
    sta sound::vera::channel_tremolo_direction,x
    stz sound::vera::channel_tremolo_loop_position,x
    rts
@store_loop_position:
    inc sound::vera::channel_tremolo_loop_position,x
    rts
.endproc