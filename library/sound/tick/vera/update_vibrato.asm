; Update virbato for channel, if required
; x = Channel
.proc update_vibrato
;pitch = EFFECT_TEMP1_VALUE
;pitch_result = EFFECT_TEMP2_VALUE
;vibrato_depth = EFFECT_TEMP3_VALUE
;length = EFFECT_TEMP4_VALUE

pitch = EFFECT_TEMP0_VALUE
pitch_result = EFFECT_TEMP1_VALUE
glide_pitch = EFFECT_TEMP2_VALUE
glide_pitch_result = EFFECT_TEMP3_VALUE
vibrato_depth = EFFECT_TEMP4_VALUE
length = EFFECT_TEMP5_VALUE


@start:
    ; Only update vibrato if depth is non-zero
    lda sound::vera::channel_vibrato_depth,x
    bne @get_registers
    rts

; Get current pitch and depth for later on
@get_registers:
    lda sound::vera::channel_semitone,x
    sta pitch
    lda sound::vera::channel_note,x
    sta pitch + 1
    lda sound::vera::channel_glide_semitone,x
    sta glide_pitch
    lda sound::vera::channel_glide_note,x
    sta glide_pitch + 1
    lda sound::vera::channel_vibrato_depth,x
    sta vibrato_depth
    stz vibrato_depth + 1

; Check the channel's pitch speed and apply
; the shift to the glide speed
@check_pitch_speed:
    ldy sound::vera::channel_pitch_speed,x
    beq @evaluate_vibrato
@pitch_speed_loop:
    asl16 vibrato_depth
    dey
    bne @pitch_speed_loop

@evaluate_vibrato:
; Look at the direction
; if 0, ramp up (add)
; if 1, ramp down (subtract)
    lda sound::vera::channel_vibrato_direction,x
    bne @subtract_pitch
@add_pitch:
    add16 pitch, vibrato_depth, pitch_result
    add16 glide_pitch, vibrato_depth, glide_pitch_result
    bra @store_pitch
@subtract_pitch:
    sub16 pitch, vibrato_depth, pitch_result
    sub16 glide_pitch, vibrato_depth, glide_pitch_result
@store_pitch:
    lda pitch_result
    sta sound::vera::channel_semitone,x
    lda pitch_result + 1
    sta sound::vera::channel_note,x
    lda glide_pitch_result
    sta sound::vera::channel_glide_semitone,x
    lda glide_pitch_result + 1
    sta sound::vera::channel_glide_note,x

; inc then if at end of loop, reset to 0    
@update_loop_position:
    lda sound::vera::channel_vibrato_loop_position,x
    cmp sound::vera::channel_vibrato_loop_length,x
    bne @store_loop_position
@flip_direction:
    lda sound::vera::channel_vibrato_direction,x
    eor #%10000000
    sta sound::vera::channel_vibrato_direction,x
    stz sound::vera::channel_vibrato_loop_position,x
    rts
@store_loop_position:
    inc sound::vera::channel_vibrato_loop_position,x
    rts
.endproc