; Calculate number of samples that will be written to the ZSM
; Uses:
; zp_TMP0, x, a

.proc calculate_number_of_samples
@start:
  sample_state = zp_TMP0
  rambank zp_BANK_MISC
  ldx #$00
@loop:
  lda sample_states,x
  sta sample_state
  bbs 7, sample_state, @active
@end:
  stx num_samples
  rts
@active:
  ; If active, we just need to increment x
  inx
  bra @loop
.endproc