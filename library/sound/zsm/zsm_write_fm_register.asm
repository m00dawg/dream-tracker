; Write FM register activity to ZSM
; Uses
; a = address
; y = data
.proc zsm_write_fm_register
  pha
  bbs 0, zp_ZSM_ENABLE,@enabled
  pla
  rts
@enabled:
  lda #ZSM_TICK_ACTIVE
  sta zp_ZSM_TICK_ACTIVE
  ; We only write one FM byte at a time
  ; this could be optimized
  lda #FM_SINGLE_WRITE
  jsr write_byte
  
  pla
  jsr write_byte
  tya
  jsr write_byte
  rts
.endproc