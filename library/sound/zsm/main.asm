.scope zsm

; ZSM Export File Format
; https://github.com/tildearrow/furnace/blob/4bc5f49fe60e254ffa99da79449c84d21185d3a2/papers/zsm-format.md

; Constants
MAGIC_HEADER_LOW = $7A
MAGIC_HEADER_HIGH = $6D  ; 'zm'
VERSION = $01
ZSM_NON_PCM_EOF = $80
DELAY_MAX = $FF
DELAY_START = $81
EXTENSION_COMMAND = $40
; bit 7-6: Channe (00 = PCM)
; bits 5-0: Number of bytes to follow (6)
PCM_EXTENSION_COMMAND = $06
PCM_AUDIO_CTRL_COMMAND = $00
PCM_AUDIO_RATE_COMMAND = $01
PCM_INSTRUMENT_TRIGGER_COMMAND = $02
; 8-bit mono
PCM_AUDIO_CTRL_BITMASK = %00000000
; bit 7 = loop
; but we're not using that at the moment, hence why this is a constant
PCM_FEATURE_BITMASK = %00000000

PCM_HEADER1 = $50 ;'P'
PCM_HEADER2 = $43 ;'C'
PCM_HEADER3 = $4d ;'M'

; 60 Hz
TICK_RATE_LOW = $3C
TICK_RATE_HIGH = $00
RESERVED_LOW = $00
RESERVED_HIGH = $00

; File Constants
ZSM_FILE_NUMBER = $02
DEVICE = $08  ; set to 8 for host system (emulator)
SECONDARY_ADDR_WRITE = $01 ; Ignore file header, overwrite
SECONDARY_ADDR_READ = $02 ; Ignore file header
NUMBER_OF_VERA_REGISTERS = $40
ZSM_TICK_ACTIVE = $01
ZSM_PCM_OFFSET_HEADER_LOCATION = $06


FM_SINGLE_WRITE = $41

; Variables
; Set loop to start of song (right after the header)
; In the future, arbitrary points may be possible but
; this works for now.
loop_point:       .byte $10,$00,$00
pcm_offset:       .byte $00,$00,$00
fm_channel_mask:  .byte $FF
psg_channel_mask: .byte $FF,$FF
; $2C is comma
zsm_filename:     .byte "@:out.zsm,s,m"
ZSM_RANDOM_IO_FILE_LENGTH = $0D
ZSM_FILE_LENGTH = $09

; Track the offset of each sample in the PCM data blog
; In our case this is 8k (16k if CRAM) for each sample
; but we still need to store the full offset for the PCM header
; Low byte will always be zero (8192 = $C000)
; See next_sample_offset proc for more info
sample_offset:    .byte $00,$00,$00

seek_command: .byte "p",SECONDARY_ADDRESS_RANDOM_IO,$00,$00,$00,$00
ZSM_SEEK_COMMAND_LENGTH = $06

; Number of samples to write to ZSM
num_samples: .byte $00

; Luts (oof circular dependency hell)
psg_channel_address_lut:
  .byte $00, $04, $08, $0C, $10, $14, $18, $1C
  .byte $20, $24, $28, $2C, $30, $34, $38, $3C

; Copies of the registers for comparing state changes
; This is because VERA PSG registers are written to
; constantly as part of the tick routine to make the
; code smaller (even if there are no changes)
vera_shadow_registers: .res $40

.include "library/sound/zsm/init.asm"
.include "library/sound/zsm/stop.asm"
.include "library/sound/zsm/write_header.asm"
.include "library/sound/zsm/write_vera_registers.asm"
.include "library/sound/zsm/zsm_write_fm_register.asm"
.include "library/sound/zsm/open_zsm_file.asm"
.include "library/sound/zsm/update_delay.asm"
.include "library/sound/zsm/write_delay.asm"
.include "library/sound/zsm/write_byte.asm"
.include "library/sound/zsm/calculate_number_of_samples.asm"
.include "library/sound/zsm/write_pcm_data.asm"
.include "library/sound/zsm/update_zsm_header.asm"
.include "library/sound/zsm/trigger_pcm.asm"

.export zsm_write_fm_register

.endscope