; Write 4 VERA registers from given channel
; Assumes VERA stride already set
; Inputs:
; x: channel-number
.proc write_vera_registers
  bbs 0, zp_ZSM_ENABLE,@enabled
  rts
@enabled:
  lda #%00010001
	sta VERA_addr_high
	lda #sound::vera::HIGH_BYTE
	sta VERA_addr_med
  lda sound::vera::channel_address_lut,x
  sta VERA_addr_low

  lda psg_channel_address_lut,x
  tay
  lda VERA_data0
  jsr write_vera_register

  iny
  lda VERA_data0
  jsr write_vera_register

  iny
  lda VERA_data0
  jsr write_vera_register

  iny
  lda VERA_data0
  jsr write_vera_register

  rts
.endproc

; Write VERA register to ZSM file, if it's been changed
; x = channel
; y = register
; a = data
; zp_TMP0 = register_data
.proc write_vera_register
  register_data = zp_TMP0
@start:
  sta register_data
  lda vera_shadow_registers,y
  cmp register_data
  bne @update_register
  rts
@update_register:
  lda #ZSM_TICK_ACTIVE
  sta zp_ZSM_TICK_ACTIVE
  tya
  jsr write_byte
  lda register_data
  sta vera_shadow_registers,y
  jsr write_byte
  rts
.endproc