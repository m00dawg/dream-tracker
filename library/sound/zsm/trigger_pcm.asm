.proc trigger_pcm
@check_zsm_active:
  bbs 0, zp_ZSM_ENABLE,@check_note_active
  rts
@check_note_active:
  bbs 7, zp_NOTE_FLAGS,@active
  rts
@active:
  lda #EXTENSION_COMMAND
  jsr write_byte
  lda #PCM_EXTENSION_COMMAND
  jsr write_byte

  lda #PCM_AUDIO_CTRL_COMMAND
  jsr write_byte
  lda zp_PCM_VOLUME
  jsr write_byte
  
  lda #PCM_AUDIO_RATE_COMMAND
  jsr write_byte
  lda zp_PCM_SAMPLE_RATE
  jsr write_byte

  lda #PCM_INSTRUMENT_TRIGGER_COMMAND
  jsr write_byte
  lda zp_PCM_SAMPLE_NUMBER
  jsr write_byte

  rts
.endproc