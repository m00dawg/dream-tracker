; Write ZSM file header

.proc write_header
  lda #MAGIC_HEADER_LOW
  jsr write_byte
  lda #MAGIC_HEADER_HIGH
  jsr write_byte
  lda #VERSION
  jsr write_byte
  
  lda loop_point
  jsr write_byte
  lda loop_point + 1
  jsr write_byte
  lda loop_point + 2
  jsr write_byte

  lda pcm_offset
  jsr write_byte
  lda pcm_offset + 1
  jsr write_byte
  lda pcm_offset + 2
  jsr write_byte


  lda fm_channel_mask
  jsr write_byte

  lda psg_channel_mask
  jsr write_byte
  lda psg_channel_mask + 1
  jsr write_byte
  
  lda #TICK_RATE_LOW
  jsr write_byte
  lda #TICK_RATE_HIGH
  jsr write_byte

  lda #RESERVED_LOW
  jsr write_byte
  lda #RESERVED_HIGH
  jsr write_byte

  rts
.endproc
