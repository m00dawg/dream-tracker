; Write to FM, noting busy flag
; Uses
; a = address
; y = data
.proc write_register
.import zsm_write_fm_register
    pha
; If bit 7 of status is set, chip is busy
; so we loop
@busy_loop:
    bit YM_data
    bmi @busy_loop
@set_register:
    pla
    sta YM_reg

    ; No nops needed if we check for ZSM writes 
    ; while we wait on the YM2151
    jsr zsm_write_fm_register
    ; 18 nops for the needed FM wait state
    ; but the jsr to the zsm_write takes 19 so we don't need these
    ;nop
    ;nop
    ;nop
    ;nop
    ;nop
    ;nop
    ;nop
    ;nop
    ;nop
@write_data:
    sty YM_data
    rts
.endproc

; Doing nop's based on chip type
; Thanks to MooingLemur
;  stx YM_reg
;  bit chip_type ; 4
;  bpl is2151 ; 2 (3 when taken)
;  nop ; 2
  ;nop
;  nop
;is2151:
  ;nop
  ;nop ; total 18 if bit 7 is set in chip_type, 11 if bit 7 is clear
  ;sta YM_datacan get without testing on a real CPU. It's a really low risk thoug