; YM2151 Specific Stuff
  .scope fm
    ; Constants
    NUM_CHANNELS = $08
    START_CHANNEL = $10
    MAX_EFFECT = $10
    
    MAX_SHARED_EFFECT = $15  ; Includes global effects, see data.inc
    MAX_UNIQUE_EFFECT = $5F  ; Includes global effects, see data.inc

    ; First set of unique effect range are the 
    ; direct register effects ($40-59)
    REGISTER_EFFECTS_START = $40
    REGISTER_EFFECTS_STOP = $57
    UNIQUE_EFFECTS_START = REGISTER_EFFECTS_STOP + 1

    ; Not currently used since channel att is 80-FF in the vol col
    ;VOLUME_COLUMN_LEGATO = $E0

    GLIDE_EFFECT_NUMBER = $06

    ; From bank.inc
    BASE_NAMES_ADDRESS = $A000
    BASE_CHANNELS_ADDRESS = $A800
    BASE_CHANNELS_SIZE = $1000
    NUM_INSTRUMENTS = $80
    MAX_VOLUME = $7F

    ; Global Register Addresses
    LFO_RESET = $01
    NOISE_CONTROL = $0F
    LFO_FREQUENCY = $18
    LFO_AMPLITUDE = $19
    LFO_WAVEFORM = $1B


    ; How many registers are in each instrument
    ; (Not the number of channel registers)
    ; Used by the random patch generator
    ;NUM_INSTRUMENT_REGISTERS = $1C ; 28 Dec
    NUM_INSTRUMENT_REGISTERS = $1C ; 28 Dec


    ; Effect table used
    channel_table:                           .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
;    channel_table:                           .res $08
    ; If position > row size (16), it means table is stopped
    channel_table_position:                  .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
    channel_instrument:                      .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF

    ; For the future
    ;channel_mute_mask:                 .byte $00

    ; Non-pre initialized data
    ;.segment "GOLDEN"

    ; Bit 6: C2
    ; Bit 5: M2
    ; Bit 4: C1
    ; Bit 3: M1
    ; Bit 2-0: Channel #
    global_key_control:                      .byte $0
    ; Bit 7: Noise Enable
    ; Bit 4-0: Noise Frequency
    global_ch7_noise_control:                .byte $0

    ; Bit 7:
      ; 0 = Amplitude Modulation Depth
      ; 1 = Phase Modulation (Vibrato) Depth
    ; Bit 6-0: Value
    global_lfo_amplitude:                    .byte $0
    ; High Bits Unused
    ; Bit 1-0: LFO Waveform
      ; 0 = Saw
      ; 1 = Square
      ; 2 = Triangle
      ; 3 = Noise
    global_lfo_waveform:                     .byte $0

    global_lfo_frequency:                     .byte $0

    ; Bit 7 unused here
    ; We set that on write to determine if we are modifying AM or PM
    ; Bits 6-0: AM/PM
    global_lfo_am_depth:                     .byte $0
    global_lfo_pm_depth:                     .byte $0


    ; bit 7: active, rest are NOPs ATM
    ; bit 6: release
    ; bit 5: off
    ; bit 4: volume sweep direction (huh??)
    ; bit 3: legato
    channel_flags:                           .byte $00,$00,$00,$00,$00,$00,$00,$00

    ; Internal Tracker Note
    channel_note:                            .byte $00,$00,$00,$00,$00,$00,$00,$00
    ; Fractional Semitone Component
    ; (note.semitone)
    channel_semitone:                        .byte $00,$00,$00,$00,$00,$00,$00,$00
    
    ; Note: VOLUME not Attenuation!
    channel_volume:                          .byte $00,$00,$00,$00,$00,$00,$00,$00

    ; Actual channel attenuation
    channel_attenuation:                     .byte $00,$00,$00,$00,$00,$00,$00,$00

    ; Bit 6-4: Octave
    ; Bit 3-0: Semitone (Note, 0=C#, E=C)
    ; 0  1  2  4  5  6  8  9  A  C  D  E
    ; C# D  D# E  F  F# G  G# A  A# B  C     
    ;channel_key_code:                        .byte $00,$00,$00,$00,$00,$00,$00,$00

    ; Bit 7-2: Key Fraction (Fine-Tune)
    ; Same as channel_semitone
    ; channel_key_fraction:                    .byte $00,$00,$00,$00,$00,$00,$00,$00

    channel_global_to_op_volume:             .byte $00,$00,$00,$00,$00,$00,$00,$00
    
    ; Bit 7-6: Right/Left
    ; Bit 5-3: M1 Feedback
    ; Bit 2-0: Connection Algorithm
    channel_pan_feedback_algo:               .byte $00,$00,$00,$00,$00,$00,$00,$00

    ; Bit 6-4: Phase Modulation Sensitivity
    ; Bit 1-0: Amplitude Modulation Sensitivity
    channel_modulation_sensitivity:          .byte $00,$00,$00,$00,$00,$00,$00,$00

    channel_detune_fine_frequency_mul_m1:    .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_detune_fine_frequency_mul_m2:    .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_detune_fine_frequency_mul_c1:    .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_detune_fine_frequency_mul_c2:    .byte $00,$00,$00,$00,$00,$00,$00,$00
 
    channel_volume_m1:                       .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_volume_m2:                       .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_volume_c1:                       .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_volume_c2:                       .byte $00,$00,$00,$00,$00,$00,$00,$00

    channel_ks_attack_m1:                    .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_ks_attack_m2:                    .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_ks_attack_c1:                    .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_ks_attack_c2:                    .byte $00,$00,$00,$00,$00,$00,$00,$00

    channel_am_decay1_m1:                    .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_am_decay1_m2:                    .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_am_decay1_c1:                    .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_am_decay1_c2:                    .byte $00,$00,$00,$00,$00,$00,$00,$00

    channel_detune_coarse_decay2_m1:         .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_detune_coarse_decay2_m2:         .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_detune_coarse_decay2_c1:         .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_detune_coarse_decay2_c2:         .byte $00,$00,$00,$00,$00,$00,$00,$00

    channel_decay1_release_m1:               .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_decay1_release_m2:               .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_decay1_release_c1:               .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_decay1_release_c2:               .byte $00,$00,$00,$00,$00,$00,$00,$00

    channel_volume_sweep_value:              .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_pitch_sweep_value:               .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_glide_speed:                     .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_glide_note:                      .byte $00,$00,$00,$00,$00,$00,$00,$00   
    channel_glide_semitone:                  .byte $00,$00,$00,$00,$00,$00,$00,$00     

    ; Note delay
    channel_note_delay:                      .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_note_delay_note:                 .byte NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL
    
    ; LUT for looking up the actual FM registers from the register effects
    effect_to_fm_register_lut:
      .byte $40, $48, $50, $58  ; DTFMUL
      .byte $60, $68, $70, $78  ; Vol
      .byte $80, $88, $90, $98  ; KSAR
      .byte $A0, $A8, $B0, $B8  ; AMD1R
      .byte $C0, $C8, $D0, $D8  ; DTCD2R
      .byte $E0, $E8, $F0, $F8  ; D1LRR
      .byte $20                 ; Pan, Feedback, Conn and
      .byte $38                 ; PMS/AMS
    
    ; LUT for managing attenuation flips

    attenuation_mask_lut:
      .byte %01110000, %01110000, %01110000, %01110000 ; DTFMUL
      .byte %01111111, %01111111, %01111111, %01111111 ; Vol
      .byte %00011111, %00011111, %00011111, %00011111 ; KSAR
      .byte %00011111, %00011111, %00011111, %00011111 ; AMD1R
      .byte %00011111, %00011111, %00011111, %00011111 ; DTCD2R
      .byte %11111111, %11111111, %11111111, %11111111 ; D1LRR

    ; LUTs for looking up the channel (main memory) registers from the register effects
    effect_to_channel_register_lut_lo:
      .byte <channel_detune_fine_frequency_mul_m1
      .byte <channel_detune_fine_frequency_mul_m2
      .byte <channel_detune_fine_frequency_mul_c1
      .byte <channel_detune_fine_frequency_mul_c2
      .byte <channel_volume_m1
      .byte <channel_volume_m2
      .byte <channel_volume_c1
      .byte <channel_volume_c2
      .byte <channel_ks_attack_m1
      .byte <channel_ks_attack_m2
      .byte <channel_ks_attack_c1
      .byte <channel_ks_attack_c2
      .byte <channel_am_decay1_m1
      .byte <channel_am_decay1_m2
      .byte <channel_am_decay1_c1
      .byte <channel_am_decay1_c2
      .byte <channel_detune_coarse_decay2_m1
      .byte <channel_detune_coarse_decay2_m2
      .byte <channel_detune_coarse_decay2_c1
      .byte <channel_detune_coarse_decay2_c2
      .byte <channel_decay1_release_m1
      .byte <channel_decay1_release_m2
      .byte <channel_decay1_release_c1
      .byte <channel_decay1_release_c2
      .byte <channel_pan_feedback_algo
      .byte <channel_modulation_sensitivity
    effect_to_channel_register_lut_high:
      .byte >channel_detune_fine_frequency_mul_m1
      .byte >channel_detune_fine_frequency_mul_m2
      .byte >channel_detune_fine_frequency_mul_c1
      .byte >channel_detune_fine_frequency_mul_c2
      .byte >channel_volume_m1
      .byte >channel_volume_m2
      .byte >channel_volume_c1
      .byte >channel_volume_c2
      .byte >channel_ks_attack_m1
      .byte >channel_ks_attack_m2
      .byte >channel_ks_attack_c1
      .byte >channel_ks_attack_c2
      .byte >channel_am_decay1_m1
      .byte >channel_am_decay1_m2
      .byte >channel_am_decay1_c1
      .byte >channel_am_decay1_c2
      .byte >channel_detune_coarse_decay2_m1
      .byte >channel_detune_coarse_decay2_m2
      .byte >channel_detune_coarse_decay2_c1
      .byte >channel_detune_coarse_decay2_c2
      .byte >channel_decay1_release_m1
      .byte >channel_decay1_release_m2
      .byte >channel_decay1_release_c1
      .byte >channel_decay1_release_c2
      .byte >channel_pan_feedback_algo
      .byte >channel_modulation_sensitivity 

    .include "library/sound/fm/init.asm"
    .include "library/sound/fm/write_register.asm"
    .include "library/sound/fm/key_control.asm"
    .include "library/sound/fm/update_channel_instrument.asm"
    .include "library/sound/fm/preview_instrument.asm"
    .include "library/sound/fm/update_volume.asm"
    .include "library/sound/fm/update_note.asm"



    .scope effects
      .include "library/sound/fm/effects/lfo_waveform.asm"
      .include "library/sound/fm/effects/lfo_frequency.asm"
      .include "library/sound/fm/effects/lfo_depth.asm"
      .include "library/sound/fm/effects/noise_control.asm"
      .include "library/sound/fm/effects/pitch_slide_down.asm"
      .include "library/sound/fm/effects/pitch_slide_up.asm"
      .include "library/sound/fm/effects/pitch_sweep_down.asm"
      .include "library/sound/fm/effects/pitch_sweep_up.asm"
      .include "library/sound/fm/effects/glide.asm"
      .include "library/sound/fm/effects/legato.asm"
      .include "library/sound/fm/effects/reset_channel.asm"
      .include "library/sound/fm/effects/update_channel_register.asm"
      .include "library/sound/fm/effects/note_delay.asm"
      .include "library/sound/fm/effects/channel_attenuation.asm"

    .endscope

  .endscope
  