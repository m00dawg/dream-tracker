; Set LFO PM/AM Waveform
; 0: Sawtooth
; 1: Square (50% duty cycle)
; 2: Triangle
; 3: Noise
;
; y = waveform

.proc lfo_waveform
return = zp_ADDR_RETURN

@start:
  ; a = address
  ; y = data  
  lda #YM_LFO_WAVEFORM
  jsr sound::fm::write_register
  jmp (return)
.endproc