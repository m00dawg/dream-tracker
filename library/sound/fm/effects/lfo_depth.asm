; Set LFO PM/AM depth
; bit 7: AM (0) or PM (1)
; AM being more of a tremolo, PM being vibrato
;
; y = depth

.proc lfo_depth
return = zp_ADDR_RETURN

@start:
  ; a = address
  ; y = data  
  lda #YM_LFO_AMPLITUDE
  jsr sound::fm::write_register
  jmp (return)
.endproc