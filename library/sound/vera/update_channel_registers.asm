; Update all audio registers of VERA channel from 
; tracker channel
; Assumes VERA stride already set
; x: channel-number
.proc update_channel_registers
  attenuation = zp_MATH0
  pitch = zp_MATH1
  finetune = zp_MATH2

  lda sound::vera::channel_address_lut,x
  sta VERA_addr_low

  lda sound::vera::channel_note,x
  sta pitch + 1
  lda sound::vera::channel_semitone,x
  sta pitch

@check_add_fine_pitch:
  ; Add fine pitch
  ; If fine_pitch is $80 to $FF, it's negative
  ; so add $FF in front.
  ; Otherwise it's positive so add $00
  lda sound::vera::channel_fine_pitch,x
  sta finetune
  ;cmp #$80
  ;blt @fine_pitch_positive
  bpl @fine_pitch_positive
@fine_pitch_negative:
  lda #$FF
  sta finetune + 1
  bra @shift_fine_pitch

@fine_pitch_positive:
  stz finetune + 1

@shift_fine_pitch:
  ; Removed for now as it breaks compat and the better solve
  ; feels like increasing teh semitone precision first?
  ;asl16 FINE_PITCH_16
  ;asl16 FINE_PITCH_16
  ;asl16 FINE_PITCH_16
  ;asl16 FINE_PITCH_16
@add_fine_pitch:
  add16 pitch, finetune, pitch

@convert_midi_to_psg:
  phx
  lda pitch + 1
  pha
  lda pitch
  pha
  rombank #AUDIO_ROM_BANK
  ply
  plx
  jsr notecon_midi2psg
  stz ROM_BANK
;  stx VERA_data0
;  sty VERA_data0


@apply_linear_finetune:
  stx pitch            ; semitone
  sty pitch  + 1       ; note
  plx
  lda sound::vera::channel_linear_fine_pitch,x
  sta finetune
  ;cmp #$80
  ;blt @positive
  bpl @positive
@negative:
  lda #$FF
  sta finetune + 1
  bra @math
@positive:
  stz finetune + 1
@math:
  add16 pitch, finetune, pitch
  lda pitch
  sta VERA_data0
  lda pitch  + 1
  sta VERA_data0

@set_attenuation:
  lda sound::vera::channel_attenuation,x
  sta attenuation
  lda sound::vera::channel_vol,x
  and #%00111111
  suba attenuation
  sta attenuation
; If negative, set to 0
  bbr 6, attenuation, @apply_attenuation
  lda #$00
@apply_attenuation:
  and #%00111111
  sta attenuation
  lda sound::vera::channel_vol,x
  and #%11000000
  ora attenuation 
  sta VERA_data0
  lda sound::vera::channel_wave,x
  sta VERA_data0
@end:
  rts

.endproc