; PWM Slide/Sweep
;
; Slide or Sweep PWM Up or Down
; Bits 7 and 6 control slide/sweep and up/down

; a = channel
; y = pwm slide value
.proc pwm_slide
pwm_slide_value = zp_MATH0
pwm_value = zp_MATH1
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta pwm_slide_value
    and #%00111111
    sta pwm_value

; For slide, we set the pitch directly and don't
; mess with channel state since it's a one-shot
@slide:
    ; Check direction
    bbs 6, pwm_slide_value, @slide_up
@slide_down:
    lda sound::vera::channel_wave,x
    and #%00111111
    sec
    sbc pwm_value
    cmp #%01000000
    bge @slide_down_rollover
    bra @slide_end
@slide_down_rollover:
    lda #$00
    bra @slide_end
@slide_up:
    lda sound::vera::channel_wave,x
    and #%00111111
    clc
    adc pwm_value
    cmp #%01000000
    blt @slide_end
@slide_up_rollover:
    lda #%00111111
@slide_end:
    and #%00111111
    sta pwm_value

    lda sound::vera::channel_wave,x
    and #%11000000
    ora pwm_value
    sta sound::vera::channel_wave,x
    jmp (return)

.endproc