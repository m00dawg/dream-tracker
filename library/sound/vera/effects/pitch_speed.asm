; Set channel pitch speed
;
; This is pretty basic since the synth engine
; will handle the heavy lifting.
; This just sets the channel register

; a = channel
; y = speed 

.proc pitch_speed
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta sound::vera::channel_pitch_speed,x
    jmp (return)
.endproc