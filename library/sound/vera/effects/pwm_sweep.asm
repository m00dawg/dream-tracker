; PWM Sweep
;
; Slide or Sweep PWM Up or Down

; a = channel
; y = pwm slide value
; bit 6 = direction
.proc pwm_sweep
pwm_slide_value = zp_MATH0
pwm_value = zp_MATH1
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta pwm_slide_value
    and #%00111111
    sta pwm_value
    bne @sweep
; Value is 0 so we make sure we turn off sweep
@zero_reset:
    sta sound::vera::channel_pwm_sweep_value,x
    jmp (return)

; For sweep, we set the channel registers 
; and let the engine do the rest.
@sweep:
    ; Get the direction value
    bbs 6, pwm_slide_value, @sweep_up
@sweep_down:
    ; Bit 3 of channel flags is PWM sweep direction
    lda sound::vera::channel_flags,x
    and #%11110111
    jmp @set_sweep
@sweep_up:
    lda sound::vera::channel_flags,x
    ora #%00001000
@set_sweep:    
    sta sound::vera::channel_flags,x
    lda pwm_value
    sta sound::vera::channel_pwm_sweep_value,x
    jmp (return)
.endproc