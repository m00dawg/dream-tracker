; Set Pitch Envelope Speed
;
; This is pretty basic since the synth engine
; will handle the heavy lifting.
; This sets the speed counter of the pitch env
; Higher speeds mean slower (just like song speed)

; a = channel
; y = pitch env speed

.proc pitch_env_speed
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta sound::vera::channel_pitch_envelope_speed,x
    stz sound::vera::channel_pitch_envelope_speed_counter,x
    jmp (return)
.endproc