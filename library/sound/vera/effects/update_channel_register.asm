; Direct update a channel register for some reason
; Parameters:
; zp_ROW_EFFECT_PARAMETER
; zp_ROW_EFFECT_VALUE
; a = channel
;
; Uses:
; zp_TMP0
.proc update_channel_register
	tax	; Channel

	lda #%00010001
	sta VERA_addr_high
	lda #sound::vera::HIGH_BYTE
	sta VERA_addr_med

	; Get the address of the register of the channel
  lda sound::vera::channel_address_lut,x
  sta zp_TMP0	
	lda zp_ROW_EFFECT_PARAMETER
	suba #sound::vera::REGISTER_EFFECTS_START
	adda zp_TMP0
	sta VERA_addr_low

	lda zp_ROW_EFFECT_VALUE
	sta VERA_data0
	rts
.endproc