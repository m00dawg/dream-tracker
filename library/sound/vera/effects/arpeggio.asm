; Arpeggiator Set
;
; This is pretty basic since the synth engine
; will handle the heavy lifting.
; This just sets the channel register

; a = channel
; y = arp semitones (4-bit/4-bit)

.proc arpeggio
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta sound::vera::channel_arp_value,x
    stz sound::vera::channel_arp_position,x

; If glide is enabled, store the glide note instead
    lda sound::vera::channel_glide_speed,x
    bne @glide
    lda sound::vera::channel_note,x
    sta sound::vera::channel_arp_base_note,x
    jmp (return)
@glide:
    lda sound::vera::channel_glide_note,x
    sta sound::vera::channel_arp_base_note,x
    jmp (return)
.endproc