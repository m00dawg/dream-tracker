; Vibrato Set
;
; This is pretty basic since the synth engine
; will handle the heavy lifting.
; This just sets the channel registers based
; on the LFO luts

; a = channel
; y = loop-length / depth (4-bit/4-bit)

.proc vibrato
return = zp_ADDR_RETURN
value = zp_MATH0

@start:
    tax ; Channel #
    ;sty value

    ; If speed is zero, reset
    cpy #$00
    bne @continue
    stz sound::vera::channel_vibrato_depth,x
    stz sound::vera::channel_vibrato_loop_length,x
    stz sound::vera::channel_vibrato_loop_position,x
    stz sound::vera::channel_vibrato_direction,x
    jmp (return)

@continue:
    ; Check if vibrato was previously set 
    ; and bail if so
    ; Why would we bail here? Commented out
    ; as I dunno WTF I was thinking here as it prevents
    ; from being able to change the vibrato on the fly
    ;lda sound::vera::channel_vibrato_depth,x
    ;bne @end
    ; Loop Length (Speed-ish?)
    tya
    lsr4
    sta sound::vera::channel_vibrato_loop_length,x

    ; Jump halfway into ramp up
    ;lda sound::vera::channel_vibrato_loop_length,x
    ; Divide by 2
    lsr
    sta sound::vera::channel_vibrato_loop_position,x
    ;stz sound::vera::channel_vibrato_loop_position,x

    ; Depth
    tya
    ;lda value
    and #%00001111  
    sta sound::vera::channel_vibrato_depth,x

@end:
    jmp (return)
.endproc