; Set Volume Envelope Speed
;
; This is pretty basic since the synth engine
; will handle the heavy lifting.
; This sets the speed counter of the volume env
; Higher speeds mean slower (just like song speed)

; a = channel
; y = vol env speed

.proc vol_env_speed
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta sound::vera::channel_vol_envelope_speed,x
    stz sound::vera::channel_vol_envelope_speed_counter,x
    jmp (return)
.endproc