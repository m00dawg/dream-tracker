; Update note on a channel
; Take originally tracker::play_row, but in order
; for note delay to work, we need to put it here instead

; Things we need to know:
; x = channel
; y = note
; 

; variables:
; LEGATO_FLAG = zp_TMP0
; NOTE_SEMITONE = zp_MATH0

.proc update_note
LEGATO_FLAG = zp_TMP0
NOTE_SEMITONE = zp_MATH0
FINE_PITCH_16 = zp_MATH1

@start:
  ; Check for null note
  cpy #NOTENULL
  bne @not_null
  rts
@not_null:
  ; Check and store legato
  lda sound::vera::channel_flags,x
  and #%00000100  ; Bit 2 is legato
  sta LEGATO_FLAG

  tya ; y is note
  ; Note (high byte)
  sta NOTE_SEMITONE + 1
  ; Since we played a new note, reset the arp base
  sta sound::vera::channel_arp_base_note,x
  ; Semitone (low byte)
  stz NOTE_SEMITONE
@note_play:
  lda LEGATO_FLAG
  bne @continue
 
  lda sound::vera::channel_flags,x
  ora #%10000000
  ; Turn off a previous note rel
  and #%10111111
  sta sound::vera::channel_flags,x

@continue:


@check_glide:
; Check for glide and if found update the glide pitch
; instead
  lda zp_ROW_EFFECT_PARAMETER
  cmp #sound::vera::GLIDE_EFFECT_NUMBER
  beq @update_glide_pitch
; If glide speed is set on the channel, also set the glide
; pitch instead
  lda sound::vera::channel_glide_speed,x
  bne @update_glide_pitch

@update_channel_pitch:
  lda NOTE_SEMITONE
  sta sound::vera::channel_semitone,x
  lda NOTE_SEMITONE + 1
  sta sound::vera::channel_note,x
  jmp @reset

@update_glide_pitch:
@check_for_glide_disable:
  ; If the effect is 00 it means disable glide so skip
  lda zp_ROW_EFFECT_VALUE
  beq @update_channel_pitch
@check_for_empty_glide:
  ; Check to see if the glide register is NULL
  ; indicating we may be playing a note at the same time as enabling glide
  lda sound::vera::channel_glide_note,x
  cmp #NOTENULL
  bne @update_glide
  lda NOTE_SEMITONE
  sta sound::vera::channel_semitone,x
  lda NOTE_SEMITONE + 1
  sta sound::vera::channel_note,x
@update_glide:
  lda NOTE_SEMITONE
  sta sound::vera::channel_glide_semitone,x
  lda NOTE_SEMITONE + 1
  sta sound::vera::channel_glide_note,x

@reset:
  stz sound::vera::channel_note_delay,x

  ; Check for legato
  lda LEGATO_FLAG
  bne @end

  ; Reset envs on new note
  stz sound::vera::channel_vol_envelope_position,x
  stz sound::vera::channel_wave_envelope_position,x
  stz sound::vera::channel_pitch_envelope_position,x
  ; We set speed counters to zero because we always want
  ; the first value in the env to fire
  stz sound::vera::channel_vol_envelope_speed_counter,x
  stz sound::vera::channel_wave_envelope_speed_counter,x
  stz sound::vera::channel_pitch_envelope_speed_counter,x
@end:
  rts

.endproc