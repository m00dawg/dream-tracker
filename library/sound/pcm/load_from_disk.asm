; Load a sample from disk

.proc load_from_disk
FILE_NUMBER = $01  ; set to 1 for now as we will only be opening 1 file at a time
DEVICE = $08  ; set to 8 for host system (emulator)
SECONDARY_ADDR_WRITE = $01 ; Ignore file header, overwrite
SECONDARY_ADDR_READ = $02 ; Ignore file header

@start:
  ldy #$00
; Get the length by finding the first space
@find_filename_length:
  lda sound::pcm::sample_full_filename,y
  cmp #SCREENCODE_BLANK
  beq @set_filename
  iny
  jmp @find_filename_length
@set_filename:
  tya
  ldx #<sound::pcm::sample_full_filename
  ldy #>sound::pcm::sample_full_filename
  jsr SETNAM
  lda #FILE_NUMBER
  ldx #DEVICE
  ldy #SECONDARY_ADDR_READ
  jsr SETLFS
  jsr OPEN
  sta error_code
  ldx #FILE_NUMBER
  jsr CHKIN
  
@find_rambank_location:
  lda zp_INSTRUMENT
  adda sound::pcm::bank_start
  sta RAM_BANK
  lda #$00
  ldx #<sound::pcm::PCM_SAMPLE_ADDRESS
  ldy #>sound::pcm::PCM_SAMPLE_ADDRESS
  jsr LOAD
  stz RAM_BANK
  lda #FILE_NUMBER
  jsr CLOSE
  jsr CLRCHN

  rts

.endproc