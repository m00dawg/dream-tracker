 .scope pcm
    ; Constants
    NUM_CHANNELS = $01
    START_CHANNEL = $18
    ;NUM_SAMPLES = $80
    NUM_SAMPLES = $10
    ; Moved to sound::pcm::bank_start
    ; PAGE_START = $D0
    SAMPLE_ADDRESS = $A000
    ; From bank.inc
    BASE_NAMES_ADDRESS = $A800
    NAME_LENGTH = $10
    ; 256 * 32 = 8192 (BRAM page size)
    SAMPLE_CHONK_SIZE = $20
    
    VERA_FIFO_FULL_MASK = %10000000
    VERA_AFLOW_MASK = %00001000
    CHANNEL_FLAG_ACTIVE_MASK = %10000000
    CHANNEL_FLAG_WAIT_BUFFER_MASK = %00100000

    ; Number of 256 byte chonks to read at once
    ; This should be modified depending on the sample 
    ; rate (channel notes) so as not to overrun the buffer
    ; For supporting up to 48.8kHz
    ; NUM_CHONKS_PER_TICK = $04
    ; Supports up to 30.5kHz
    ; NUM_CHONKS_PER_TICK = $02
    ; Supports up ot 15.2khz
    ; NUM_CHONKS_PER_TICK = $01
    ; If less than or egual
    ; Based on https://docs.google.com/spreadsheets/d/1GdV2P1DjWWLfRROK9WA_AK4HYTbeWE_9FTiT7bq8dxo/edit#gid=0
    ; Calculated as (Samples Per Int Rate) * (Int Rate) * Tick Rate
    ; Or (48.8Khz / 128) * (VERA Int Rate) * (1/60 for Vsync)
    SAMPLE_RATE_FOR_2_CHONKS = $29 ; 41 dec
    SAMPLE_RATE_FOR_3_CHONKS = $51  ; 81 dec
    SAMPLE_RATE_FOR_4_CHONKS = $79  ; 121 dec

    ; For finding PCM samples in BRAM (see setup.asm)
    bank_start: .byte 0
    bank_end:  .byte 0

    ; For loading samples from disk to the song
    sample_full_filename: .byte "samples/"
    sample_filename: .byte "                ",0   

    ; Moved to ZeroPage
    ; Raw sample number
    ;channel_sample:       .byte $00
    ; Actual page (which is sample number + base page address)
    ; This is so we don't have to calculate this during the 
    ; PCM playback routine
    ;channel_sample_page:  .byte $00
    ; bit 7: active
    ; bit 6: mute
    ;channel_flags:            .byte $00
    ;channel_note:             .byte $00
    ;channel_volume:           .byte $00
    ; This is the high byte. That is to say
    ; things are done in chonks of 256.
    ;channel_chonk_count:  .byte $00

    .include "library/sound/pcm/init.asm"
    .include "library/sound/pcm/calc_num_chonks.asm"
    .include "library/sound/pcm/update_pcm.asm"
  .endscope