; MIDI routines and data
; Good ref: http://midi.teragonaudio.com/tech/midispec.htm

.segment "CODE"
.scope midi
	; ## Constants 
	; MIDI Channnels
	MIDI_CHANNELS=$10
	; Shared with PSG
	TRACKER_CHANNELS=$10
	MAX_VOLUME=$7F

	LEGATO_ON = $7F
	LEGATO_OFF = $00

	MAX_SHARED_EFFECT = $07  ; Includes global effects, see data.inc
	UNIQUE_EFFECTS_START = $40
  MAX_UNIQUE_EFFECT = $46  ; Includes global effects, see data.inc


	; MIDI Default per Kevin, IO6/Low (2Mhz)
	MIDI_IO_ADDRESS=$9FC0
	WAVETABLE_IO_ADDRESS=$9FC8
	; IO3 (8MHz)
	;IO_ADDRESS=$9F60

	; Offsets for the base I/O address, which lives in the ZP
	; (zp_MIDI_BASE)
	; These are used for indirect-indexed addressing
	RX_BUFFER_OFFSET=$00           ; Read Only
	TX_HOLDING_OFFSET=$00         ; Write Only
	INTERRUPT_ENABLE_OFFSET=$01
	INTERRUPT_IDENT_OFFSET=$02 ; Read only
	FIFO_CONTROL_OFFSET=$02    ; Write only
	LINE_CONTROL_OFFSET=$03
	MODEM_CONTROL_OFFSET=$04
	LINE_STATUS_OFFSET=$05
	MODEM_STATUS_OFFSET=$06
	SCRATCH_OFFSET=$07
	DIVISOR_LATCH_LOW = $00
	DIVISOR_LATCH_HI = $01

	; 32 because
	; 16 Mhz  / 31250 * 16
	MIDI_BAUD_RATE_DIVISOR = $0020

	; Disable Interupts by default
	; (We'll enable in ISRs)
	INTR_SETUP  = %00000000
	INTR_ENABLE = %00000001
	;INTR_ENABLE = %00000101

	LCR_SETUP  = %00000011
	FIFO_CLEAR = %00000110
	FIFO_SETUP = %00000001
	MODEM_SETUP = %00000000
	MODEM_INT_ENABLE = %00001000



	; ## Memory
	; There are 16 channels as these are shared between the VERA PSG channels
	; (vera::channel_mode determines if the channel is to be evaluated as PSG or MIDI at any given time)
  channel_midi_channel:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
	channel_instrument:           .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
	channel_cc_parameter:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  channel_note:                 .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
	channel_vol:                  .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
	channel_attenuation:          .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

	; Channel Flags
	; bit 7: active, rest are NOPs ATM
	; bit 6: release
	; bits 5-2: unused
	; Legato, bits 0-1
	; 00 = Mono (off before on)
	; 01 = Poly (no off before on, allowing multiple notes to be played)
	; 10 = MIDI Legato Mode (usually skip env attack of next note)
	; 11 = Unused
	channel_flags:               .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

	; These are values specific to the *MIDI* channels, not tracker channels
	; Defaults to SAM ressonance
	midi_channel_nrpn_coarse:    .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
	midi_channel_nrpn_fine:      .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF

.scope message
  NOTE_OFF   		 = $80
	NOTE_ON   		 = $90
	CC_CHANGE 		 = $B0			; Lower nibble = channel
	PROGRAM_CHANGE = $C0
	CLOCK 				 = $F8
	START 				 = $FA
	STOP 					 = $FC
	RESET					 = $FF

	SYSEX_START    = $F0
	SYSEX_END      = $F7

	.scope cc
		DATA_ENTRY		= $06		; Typically for RPN/NRPN
		VOLUME_COARSE = $07
		LEGATO				= $44
		SAM_CUTOFF    = $4A
		NRPN_FINE			= $62
		NRPN_COARSE		= $63
		ALL_SOUND_OFF = $78
		ALL_NOTES_OFF = $7B
	
	.endscope

	.scope nrpn
		SAM_RES_COARSE = $01
		SAM_RES_FINE   = $21
	.endscope

	
.endscope

	.scope commands
		.include "library/sound/midi/commands/send_cc.asm"
		.include "library/sound/midi/commands/note_on.asm"
		.include "library/sound/midi/commands/note_off.asm"
		.include "library/sound/midi/commands/program_change.asm"
		.include "library/sound/midi/commands/reset.asm"
		.include "library/sound/midi/commands/send_multibyte.asm"
	.endscope

	.scope effects
		.include "library/sound/midi/effects/midi_channel.asm"
		.include "library/sound/midi/effects/cc_param.asm"
		.include "library/sound/midi/effects/cc_value.asm"
		.include "library/sound/midi/effects/legato.asm"
		.include "library/sound/midi/effects/channel_attenuation.asm"
		.include "library/sound/midi/effects/sam_cutoff.asm"
		.include "library/sound/midi/effects/sam_ressonance.asm"
	.endscope

.include "library/sound/midi/send.asm"
.include "library/sound/midi/set_address.asm"
.include "library/sound/midi/init.asm"


.endscope