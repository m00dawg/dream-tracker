; Set MIDI address based on if the channel is wavetable or MIDI
; a = PSG channel type
.proc set_address
	cmp #sound::vera::WAVETABLE
	bne @midi
	lda #<WAVETABLE_IO_ADDRESS
	sta zp_MIDI_BASE
	lda #>WAVETABLE_IO_ADDRESS
	sta zp_MIDI_BASE + 1
	rts
@midi:
	lda #<MIDI_IO_ADDRESS
	sta zp_MIDI_BASE
	lda #>MIDI_IO_ADDRESS
	sta zp_MIDI_BASE + 1
	rts
.endproc