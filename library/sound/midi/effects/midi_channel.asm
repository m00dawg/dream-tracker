; Set MIDI Channel for VERA Channel
; The MIDI channel need not match the VERA channel
; and multiple VERA channels can use the same MIDI channel.

; a = channel
; y = MIDI channel

.proc midi_channel
return = zp_ADDR_RETURN

@start:
    tax ; Channel
    tya ; MIDI Channel
    sta sound::midi::channel_midi_channel,x
    jmp (return)
.endproc