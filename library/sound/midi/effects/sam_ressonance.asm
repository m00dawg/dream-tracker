; Set Ressonance for the SAM chip

; a = channel
; y = ressonance value

.proc sam_ressonance
return = zp_ADDR_RETURN

@start:
	phy
	tax
  lda sound::midi::channel_midi_channel,x
  sta zp_MIDI_CHANNEL

	; Check if we have already set NRPN values
	; This avoids having to unncessary send
	; bytes over the MIDI interface, which is 
	; comparatively slow.
@check_coarse:
  ldx zp_MIDI_CHANNEL
	lda sound::midi::midi_channel_nrpn_coarse,x
	cmp #sound::midi::message::nrpn::SAM_RES_COARSE
	beq @check_fine
	lda #sound::midi::message::nrpn::SAM_RES_COARSE
	sta sound::midi::midi_channel_nrpn_coarse,x
	; channel = zp_MIDI_CHANNEL
	; a = value
	; x = cc number
	ldx #sound::midi::message::cc::NRPN_COARSE
	jsr sound::midi::commands::send_cc

@check_fine:
  ldx zp_MIDI_CHANNEL
	lda sound::midi::midi_channel_nrpn_fine,x
	cmp #sound::midi::message::nrpn::SAM_RES_FINE
	beq @continue
	lda #sound::midi::message::nrpn::SAM_RES_FINE
	sta sound::midi::midi_channel_nrpn_fine,x
	; channel = zp_MIDI_CHANNEL
	; a = value
	; x = cc number
	ldx #sound::midi::message::cc::NRPN_FINE
	jsr sound::midi::commands::send_cc

@continue:
	; channel = zp_MIDI_CHANNEL
	; a = CC value
	; x = CC parameter	
	; from the ply
	pla
	ldx #midi::message::cc::DATA_ENTRY
	jsr sound::midi::commands::send_cc

@return:
	jmp (return)
.endproc