; Send a multiebyte string
; zp_ADDR0 = pointer to byte string
; zp_ARG0 = length
; Uses a,y

.proc send_multibyte
	STRING = zp_ADDR0
	LENGTH = zp_ARG0
	ldy #$00
@loop:
	lda (STRING),y
	phy
	jsr send
	ply
	iny
	cpy LENGTH
	bne @loop
@end:
	rts
.endproc