; Sound engine routines

.scope sound

  ; Constants
  INSTRUMENT_NAME_LENGTH = $10
  INSTRUMENT_NAMES_SIZE = $0800
  

  SHARED_EFFECTS_START = $00
  ; This is also where the direct register writes start for each sound chip
  UNIQUE_EFFECTS_START = $40
  GLOBAL_EFFECTS_START = $C0
  ; Highest Implemented Effect
  MAX_GLOBAL_EFFECT = $C9

  MACRO = $3F
  ; Identifiers (mostly for macros)
  VERA = $00
  FM = $01
  PCM = $02

  GLIDE_EFFECT_NUMBER = $06
  NOTE_DELAY_EFFECT_NUMBER = $10

  .include "library/sound/pcm/main.asm"
  .include "library/sound/fm/main.asm"
  .include "library/sound/vera/main.asm"
  .include "library/sound/midi/main.asm"

  ; ZSM 
  .include "library/sound/zsm/main.asm"
  .include "library/sound/tick/update.asm"
.endscope


