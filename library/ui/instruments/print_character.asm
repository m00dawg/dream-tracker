; Print a character in the instrument names column
.proc print_character
  pha
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  ldx text_color
  stx r0
  pla
  jsr graphics::printing::print_alpha_char
  lda cursor_x
  cmp #INSTRUMENT_NAMES_CURSOR_STOP_X
  beq @end
  jsr ui::cursor_right
@end:
  rts
.endproc