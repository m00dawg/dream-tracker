; Since VERA and YM2151 use the same bits
; for pan, we can reuse this routine for both.
; (see tracker::modules::vera_instruments and fm_instruments)

.proc set_pan
@set_pan:
	lda left_x
	ldy left_y
  	jsr graphics::drawing::goto_xy
	bbr 6, pan,@set_left_inactive
@set_left_active:
	lda #LEFT_PAN_ACTIVE_COLOR
	sta zp_TEXT_COLOR
	lda #SCREENCODE_L
	jsr graphics::printing::print_alpha_char
	jmp @check_right
@set_left_inactive:
	lda #PAN_INACTIVE_COLOR
	sta zp_TEXT_COLOR
	lda #SCREENCODE_L
	jsr graphics::printing::print_alpha_char
@check_right:
	lda left_x
	inc
	ldy left_y
  	jsr graphics::drawing::goto_xy	
	bbr 7, pan,@set_right_inactive
@set_right_active:
	lda #RIGHT_PAN_ACTIVE_COLOR
	sta zp_TEXT_COLOR
	lda #SCREENCODE_R
	jsr graphics::printing::print_alpha_char
	bra @end
@set_right_inactive:
	lda #PAN_INACTIVE_COLOR
	sta zp_TEXT_COLOR
	lda #SCREENCODE_R
	jsr graphics::printing::print_alpha_char
@end:
    rts
.endproc