.proc reset_on_edit
  jsr graphics::drawing::cursor_unplot
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high
  stz cursor_layer
  lda edit_start_x
  sta cursor_x
  ldy edit_start_y
  sty cursor_y
  jsr graphics::drawing::goto_xy
  jsr graphics::drawing::cursor_plot
  rts
.endproc