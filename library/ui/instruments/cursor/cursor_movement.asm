; Instrument Name Movements
.proc name_cursor_up
    lda zp_INSTRUMENT
    ; If we're at min instrument, do not scroll
    bne @check_scroll
    rts
@check_scroll:
    ; If we're at the top, scroll
    lda screen_y
    cmp #INSTRUMENT_NAMES_CURSOR_START_Y
    bne @dec_screen_position
@scroll:
    jsr ui::scroll_down
    jmp @move_up
; We need to keep track of the position of the screen
; versus the actual cursor position to make scrolling
; work fluidly
@dec_screen_position:
    dec screen_y
@move_up:
    jsr ui::cursor_up
    jmp @continue
@continue:
    ldx number_color
    jsr change_instrument_color
    dec zp_INSTRUMENT
    ldx #ACTIVE_COLOR
    jsr change_instrument_color
    rts
.endproc

.proc name_cursor_down
    ; If we're at max instrument, do not scroll
    lda num_instruments
    suba #$01
    cmp zp_INSTRUMENT
    beq @end
    ; If we're at the bottom, scroll
    lda screen_y
    cmp #INSTRUMENT_NAMES_CURSOR_STOP_Y
    blt @inc_screen_position
@scroll:
    jsr ui::scroll_up
    jmp @move_down
@inc_screen_position:
    inc screen_y
@move_down:
    jsr ui::cursor_down
@continue:
    ldx number_color
    jsr change_instrument_color
    inc zp_INSTRUMENT
    ldx #ACTIVE_COLOR
    jsr change_instrument_color
@end:
    rts
.endproc

.proc name_cursor_left
    lda cursor_x
    cmp #INSTRUMENT_NAMES_CURSOR_START_X
    beq @cursor_left_end
    jsr ui::cursor_left
@cursor_left_end:
    rts
.endproc

.proc name_cursor_right
    lda cursor_x
    cmp #INSTRUMENT_NAMES_CURSOR_STOP_X
    beq @cursor_right_end
    jsr ui::cursor_right
@cursor_right_end:    
    rts
.endproc

; x = color
; Clobbers: a,y
.proc change_instrument_color
    lda #$00
    ldy zp_INSTRUMENT
    jsr graphics::drawing::goto_xy

    ; Skip over char and only change color
    lda #%00010001	; Inc set to 1, high ram
    sta VERA_addr_high
    lda VERA_data0
    stx VERA_data0
    lda VERA_data0
    stx VERA_data0
    rts
.endproc