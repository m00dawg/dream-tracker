.proc reset_on_names
  jsr graphics::drawing::cursor_unplot
  lda #%00010001	; Inc set to 1, hi ram
  sta VERA_addr_high
  lda #$01
  sta cursor_layer
  lda #INSTRUMENT_NAMES_CURSOR_START_X
  sta cursor_x
  sta screen_x
  ldy zp_INSTRUMENT
  sty cursor_y
  sty screen_y
  jsr graphics::drawing::goto_xy
  jsr graphics::drawing::cursor_plot
  rts
.endproc