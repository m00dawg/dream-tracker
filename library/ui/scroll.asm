; Scroll layer up and down by the character size

.proc scroll_up
  clc
  lda VERA_L0_vscroll_l
  adc #CHARACTER_SCROLL
  sta VERA_L0_vscroll_l
  lda VERA_L0_vscroll_h
  adc #$00
  sta VERA_L0_vscroll_h
  rts
.endproc

.proc scroll_down
  sec
  lda VERA_L0_vscroll_l
  sbc #CHARACTER_SCROLL
  sta VERA_L0_vscroll_l
  lda VERA_L0_vscroll_h
  sbc #$00
  sta VERA_L0_vscroll_h
  rts
.endproc
