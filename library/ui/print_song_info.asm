.proc print_song_info
@start:
  push_state_disable_interrupts
  rambank zp_BANK_MISC
  stz VERA_addr_high
  print_string_with_length_macro song_title, #SONG_TITLE_MAX_LENGTH, #SONG_TITLE_X, #SONG_TITLE_Y, #TEXT_COLORS
  print_string_with_length_macro composer, #COMPOSER_MAX_LENGTH, #COMPOSER_X, #COMPOSER_Y, #TEXT_COLORS
  plp
  rts
.endproc
