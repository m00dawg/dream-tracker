; Update all the dynamic bits at the top of the frame

.proc update_upper_info
@start:
  jsr ui::print_speed
  jsr ui::print_current_pattern_number
  jsr ui::print_row_number
  ;jsr ui::print_current_instrument
  jsr ui::print_current_octave
  jsr ui::print_current_order
  ;jsr ui::update_voices

  ; End
  rts
.endproc
