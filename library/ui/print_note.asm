; Print the tracker representation of a note
; (e.g. C-4) from the numeric counterpart.
;
; Note is stored in the pattern as a numerical value
; and needs to be split between note name (A...G)
; and octave (1,2,3, etc.).
; 
; Finding the octave is integer divison of 
; the note value divided by 12
; The note value is the original numeric value
; minus the octave value.

; ZPs used:
; NOTE_COLOR = zp_ARG0
; NOTEREL_COLOR = zp_ARG1
; NOTEOFF_COLOR = zp_ARG2
; NOTENULL_COLOR = zp_ARG3
; TMP = zp_TMP3
; NOTE_NOTE = zp_TMP1
; NOTE_OCTAVE = zp_TMP2

.proc print_note
  NOTE_COLOR = zp_ARG0
  NOTEREL_COLOR = zp_ARG1
  NOTEOFF_COLOR = zp_ARG2
  NOTENULL_COLOR = zp_ARG3
  TMP = zp_TMP3
  NOTE_NOTE = zp_TMP1
  NOTE_OCTAVE = zp_TMP2
@setup:
  lda NOTE_COLOR
  sta zp_TEXT_COLOR

  lda zp_NOTE_NUMERIC
@check_for_special:
  cmp #NOTEREL
  beq @print_rel
  cmp #NOTEOFF
  beq @print_off
  cmp #NOTENULL
  beq @print_null
@ocatve:
  jsr math::divide_by_12
  ; minus 1 because notes lower than 21 are below 0 octave
  ; in the MIDI note chart.
  ; dec
  sta NOTE_OCTAVE  
@note_value:
  ; Yes we just divided the octave. We need the
  ; integer multiplier of that so we can subtract
  ; that from the original numerical note to find
  ; the note letter.
  jsr math::multiply_by_12
  sta TMP
  lda zp_NOTE_NUMERIC
  sec
  sbc TMP
  sta NOTE_NOTE
@print_note_screen:
  tax
  lda note_names,x
  jsr graphics::printing::print_alpha_char
  lda note_sharps,x
  jsr graphics::printing::print_alpha_char
  lda NOTE_OCTAVE
  adc #$30  ; to PETSCII start of numbers
  jmp @print_note_end
@print_rel:
  lda NOTEREL_COLOR
  sta zp_TEXT_COLOR
  lda #PETSCII_UP_ARROW
  jmp @print_special
@print_off:
  lda NOTEOFF_COLOR
  sta zp_TEXT_COLOR
  lda #PETSCII_DASH
  jmp @print_special
@print_null:
  lda NOTENULL_COLOR
  sta zp_TEXT_COLOR
  lda #PETSCII_PERIOD
; If we're printing ---, ^^^, ...
@print_special:
  jsr graphics::printing::print_alpha_char
  jsr graphics::printing::print_alpha_char
@print_note_end:
  jsr graphics::printing::print_alpha_char
  rts
.endproc
