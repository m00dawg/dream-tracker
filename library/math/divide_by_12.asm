; Divide a by 12, return result as a
.proc divide_by_12
divide_by_12:
  lsr
  lsr
  sta  zp_TMP0
  lsr
  adc  zp_TMP0
  ror
  lsr
  adc  zp_TMP0
  ror
  lsr
  adc  zp_TMP0
  ror
  lsr
  rts
.endproc
