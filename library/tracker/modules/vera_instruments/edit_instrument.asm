; Procedures for editing instrument

.proc toggle_instrument_flag
; Toggle flags based on edit position  
  ldx zp_INSTRUMENT
  lda instrument_edit_pos
; Toggle Left Channel
@instrument_edit_left_channel:
  cmp #INSTRUMENT_EDIT_LEFT
  bne @instrument_edit_right_channel
  lda vera_instruments_vol,x
  jsr ui::instruments::toggle_second_flag
  sta vera_instruments_vol,x
  jmp @end

; Toggle Right Channel
@instrument_edit_right_channel:
  cmp #INSTRUMENT_EDIT_RIGHT
  bne @instrument_edit_volume_env_toggle
  lda vera_instruments_vol,x
  jsr ui::instruments::toggle_first_flag
  sta vera_instruments_vol,x
  jmp @end

; Toggle Volume Envelope
@instrument_edit_volume_env_toggle:
  cmp #INSTRUMENT_EDIT_VOL_ENV_ENABLE
  bne @instrument_edit_volume_env_loop_enable
  lda vera_instruments_vol_envelope_flags,x  
  jsr ui::instruments::toggle_first_flag
  sta vera_instruments_vol_envelope_flags,x
  jmp @end

; Toggle Volume Loop Envelope
@instrument_edit_volume_env_loop_enable:
  cmp #INSTRUMENT_EDIT_VOL_ENV_LOOP_ENABLE
  bne @instrument_edit_wave_env_toggle
  lda vera_instruments_vol_envelope_flags,x  
  jsr ui::instruments::toggle_second_flag
  sta vera_instruments_vol_envelope_flags,x
  jmp @end

; Toggle Wave Envelope
@instrument_edit_wave_env_toggle:
  cmp #INSTRUMENT_EDIT_WAVE_ENV_ENABLE
  bne @instrument_edit_wave_env_loop_enable
  lda vera_instruments_wave_envelope_flags,x  
  jsr ui::instruments::toggle_first_flag
  sta vera_instruments_wave_envelope_flags,x
  jmp @end

; Toggle Wave Loop Envelope
@instrument_edit_wave_env_loop_enable:
  cmp #INSTRUMENT_EDIT_WAVE_ENV_LOOP_ENABLE
  bne @instrument_edit_pitch_env_toggle
  lda vera_instruments_wave_envelope_flags,x  
  jsr ui::instruments::toggle_second_flag
  sta vera_instruments_wave_envelope_flags,x
  jmp @end

; Toggle Pitch Envelope
@instrument_edit_pitch_env_toggle:
  cmp #INSTRUMENT_EDIT_PITCH_ENV_ENABLE
  bne @instrument_edit_pitch_env_loop_enable
  lda vera_instruments_pitch_envelope_flags,x  
  jsr ui::instruments::toggle_first_flag
  sta vera_instruments_pitch_envelope_flags,x
  jmp @end

; Toggle Pitch Loop Envelope
@instrument_edit_pitch_env_loop_enable:
  cmp #INSTRUMENT_EDIT_PITCH_ENV_LOOP_ENABLE
  bne @end
  lda vera_instruments_pitch_envelope_flags,x  
  jsr ui::instruments::toggle_second_flag
  sta vera_instruments_pitch_envelope_flags,x
@end:
; Refresh change made above
  jsr load_instrument
  jsr graphics::drawing::cursor_plot
  rts
.endproc

; Edit various values depending on what part of the
; instrument we are editing
.proc edit_instrument_value
  lda instrument_edit_pos
  ldx zp_INSTRUMENT
  ldy zp_KEY_PRESSED
  jsr update_instrument_wave
  jsr update_instrument_env_loops
  jsr update_instrument_hex_values
@end:
  jsr load_instrument
  jsr graphics::drawing::cursor_plot
  rts
.endproc

; Check to see if we're on the wave selection for the 
; instrument and update the wave if the user pressed
; P, S, T, or N
.proc update_instrument_wave
  cmp #INSTRUMENT_EDIT_WAVE
  bne @end
  lda vera_instruments_wave,x
@key_p:
  cpy #KEY_P
  bne @key_s
  and #%00111111
  sta vera_instruments_wave,x
@key_s:
  cpy #KEY_S
  bne @key_t
  and #%01111111
  ora #%01000000
  sta vera_instruments_wave,x
@key_t:
  cpy #KEY_T
  bne @key_n
  and #%10111111
  ora #%10000000
  sta vera_instruments_wave,x
@key_n:
  cpy #KEY_N
  bne @end
  and #%11111111
  ora #%11000000
  sta vera_instruments_wave,x
@end:
  rts
.endproc

; If we're at a loop position, allow setting the loop to 
; F (forward) or P (ping-pong)
.proc update_instrument_env_loops
  cmp #INSTRUMENT_EDIT_VOL_ENV_LOOP_MODE
  beq @vol_env
  cmp #INSTRUMENT_EDIT_WAVE_ENV_LOOP_MODE
  beq @wave_env
  cmp #INSTRUMENT_EDIT_PITCH_ENV_LOOP_MODE
  bne @end
@pitch_env:
  lda vera_instruments_pitch_envelope_flags,x
  jsr @check_keyboard
  sta vera_instruments_pitch_envelope_flags,x
  jmp @end
@vol_env:
  lda vera_instruments_vol_envelope_flags,x
  jsr @check_keyboard
  sta vera_instruments_vol_envelope_flags,x
  jmp @end  
@wave_env:
  lda vera_instruments_wave_envelope_flags,x
  jsr @check_keyboard
  sta vera_instruments_wave_envelope_flags,x
  jmp @end  
@end:
  rts
; Subroutine to check for P and F and do the logic
@check_keyboard:
@key_p:
  cpy #KEY_P
  bne @key_f
  ora #%00100000
@key_f:
  cpy #KEY_F
  bne @check_keyboard_end
  and #%11011111
@check_keyboard_end:
  rts
.endproc

; For parts of the instrument we can type in numbers,
; read value from keyboard and input into values
; We'll have to track left/right values

; If key is >= 0 ($30) and <= F ($46)
.proc update_instrument_hex_values
  ldy zp_KEY_PRESSED
  cpy #KEY_G
  bcc @less_than
  rts
@less_than:
  cpy #KEY_0
  bcs @greater_than_or_equal_to
  rts
@greater_than_or_equal_to:
  phy
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  pla
  jsr graphics::printing::print_alpha_char
  jsr save_instrument_numericals
  
  lda numerical_cursor_position
  bne @end
  jsr ui::cursor_right
@end:
  inc numerical_cursor_position
  rts
.endproc

; Save all numerical values in instrument from VRAM
; Surely there must be a better way.
.proc save_instrument_numericals
  ldx zp_INSTRUMENT
@instrument_edit_volume:
  lda #VOLUME_X
  ldy #VOLUME_Y
  jsr read_instrument_numerical
  and #%00111111
  sta numerical_mask
  lda vera_instruments_vol,x
  and #%11000000
  ora numerical_mask
  sta vera_instruments_vol,x
@instrument_edit_fine_pitch:
  lda #FINE_PITCH_X
  ldy #FINE_PITCH_Y
  jsr read_instrument_numerical
  sta vera_instruments_fine_pitch,x
@instrument_edit_pitch_speed:
  lda #PITCH_SPEED_X
  ldy #PITCH_SPEED_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  jsr graphics::printing::convert_to_number
  sta vera_instruments_pitch_speed,x
@instrument_edit_pwm:
  lda #PWM_X
  ldy #PWM_Y
  jsr read_instrument_numerical
  and #%00111111
  sta numerical_mask
  lda vera_instruments_wave,x
  and #%11000000
  ora numerical_mask
  sta vera_instruments_wave,x
@volume_envelope:
@instrument_edit_vol_env_number:
  lda #VOLUME_ENV_X
  ldy #ENV_NUMBER_Y
  jsr read_instrument_numerical
  sta vera_instruments_vol_envelope_num,x
@instrument_edit_vol_env_speed:
  lda #VOLUME_ENV_X
  ldy #ENV_SPEED_Y
  jsr read_instrument_numerical
  sta vera_instruments_vol_envelope_speed,x
@instrument_edit_vol_env_length:
  lda #VOLUME_ENV_X
  ldy #ENV_LENGTH_Y
  jsr read_instrument_numerical
  sta vera_instruments_vol_envelope_length,x
@instrument_edit_vol_env_loop_start:
  lda #VOLUME_ENV_X
  ldy #ENV_LOOP_START_Y
  jsr read_instrument_numerical
  sta vera_instruments_vol_envelope_loop_start,x
@instrument_edit_vol_env_loop_end:
  lda #VOLUME_ENV_X
  ldy #ENV_LOOP_END_Y
  jsr read_instrument_numerical
  sta vera_instruments_vol_envelope_loop_end,x
@wave_envelope:
@instrument_edit_wave_env_number:
  lda #WAVE_ENV_X
  ldy #ENV_NUMBER_Y
  jsr read_instrument_numerical
  sta vera_instruments_wave_envelope_num,x
@instrument_edit_wave_env_speed:
  lda #WAVE_ENV_X
  ldy #ENV_SPEED_Y
  jsr read_instrument_numerical
  sta vera_instruments_wave_envelope_speed,x
@instrument_edit_wave_env_loop_length:
  lda #WAVE_ENV_X
  ldy #ENV_LENGTH_Y
  jsr read_instrument_numerical
  sta vera_instruments_wave_envelope_length,x
@instrument_edit_wave_env_loop_start:
  lda #WAVE_ENV_X
  ldy #ENV_LOOP_START_Y
  jsr read_instrument_numerical
  sta vera_instruments_wave_envelope_loop_start,x
@instrument_edit_wave_env_loop_end:
  lda #WAVE_ENV_X
  ldy #ENV_LOOP_END_Y
  jsr read_instrument_numerical
  sta vera_instruments_wave_envelope_loop_end,x
@pitch_envelope:
@instrument_edit_pitch_env_number:
  lda #PITCH_ENV_X
  ldy #ENV_NUMBER_Y
  jsr read_instrument_numerical
  sta vera_instruments_pitch_envelope_num,x
@instrument_edit_pitch_env_speed:
  lda #PITCH_ENV_X
  ldy #ENV_SPEED_Y
  jsr read_instrument_numerical
  sta vera_instruments_pitch_envelope_speed,x
@instrument_edit_pitch_env_loop_length:
  lda #PITCH_ENV_X
  ldy #ENV_LENGTH_Y
  jsr read_instrument_numerical
  sta vera_instruments_pitch_envelope_length,x
@instrument_edit_pitch_env_loop_start:
  lda #PITCH_ENV_X
  ldy #ENV_LOOP_START_Y
  jsr read_instrument_numerical
  sta vera_instruments_pitch_envelope_loop_start,x
@instrument_edit_pitch_env_loop_end:
  lda #PITCH_ENV_X
  ldy #ENV_LOOP_END_Y
  jsr read_instrument_numerical
  sta vera_instruments_pitch_envelope_loop_end,x
@end:
  rts
.endproc

; Read the value at current instrument position
; helper for save_instrument_numericals
.proc read_instrument_numerical
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  sta r0
  ; Skip color
  lda VERA_data0
  lda VERA_data0
  sta r1
  ; Skip color
  lda VERA_data0
  jsr graphics::printing::chars_to_number
  rts
.endproc