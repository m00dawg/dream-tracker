.proc draw_ui
	vera_layer0_16_color

	; Change $0B pallete color to a blueish
	ldx #$16
	lda #$04
	sta palette,x
	inx
	lda #$01
	sta palette,x
	jsr graphics::vera::load_palette_16

  ; Set VERA Stride and bank
  lda #$10          ; Set primary address bank to 0, stride to 1
  sta VERA_addr_high 

	; Draw Frame
	stz zp_ARG0	;X1
	stz zp_ARG1	;Y1
	lda #$4F		;X2
	sta zp_ARG2
	lda #$3B		;Y2
	sta zp_ARG3
	lda #FRAME_COLOR
	sta zp_ARG4
	jsr graphics::drawing::draw_rounded_box

	; Clear inner frame
	lda #$01
	sta zp_ARG0
	sta zp_ARG1
	lda #$4E
	sta zp_ARG2
	lda #$3A
	sta zp_ARG3
	lda #$B1
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box
	
	jsr ui::print_header
  jsr ui::print_song_info
  jsr ui::print_speed
  jsr ui::print_current_order
  jsr ui::print_current_pattern_number
	
	print_string_macro display_elements

	lda #$1B
	sta zp_ARG1
	lda #$23
	sta zp_ARG2
	print_string_macro enveloope_text

	lda #$28
	sta zp_ARG1
	lda #$23
	sta zp_ARG2	
	print_string_macro enveloope_text

	lda #$35
	sta zp_ARG1
	lda #$23
	sta zp_ARG2
	print_string_macro enveloope_text

	jsr draw_instrument_names_box

	rts
.endproc

.proc draw_instrument_names_box
	;lda #INSTRUMENT_NAMES_START_X
	ldy #INSTRUMENT_NAMES_START_Y
@loop:
	lda #INSTRUMENT_NAMES_START_X
	sta zp_ARG1
	sty zp_ARG2
	phy
	print_string_macro instrument_name_text
	ply
	iny
	cpy #INSTRUMENT_NAMES_START_Y + $20
	bne @loop
	rts
.endproc


display_elements:
	.byte SCREENCODE_COLOR,INSTRUMENT_TYPE_ACTIVE_COLOR
	.byte SCREENCODE_XY,$03,$08
	.byte "vera instruments         "
	.byte SCREENCODE_COLOR,INSTRUMENT_TYPE_INACTIVE_COLOR
	.byte "fm instruments"
	.byte SCREENCODE_COLOR,HEADER_COLOR
	
	.byte SCREENCODE_XY,$1B,$0D
	.byte "pan         : ",SCREENCODE_COLOR,$00,SCREENCODE_BLANK,SCREENCODE_BLANK
	
	.byte SCREENCODE_RETURN,SCREENCODE_COLOR,HEADER_COLOR
	.byte "volume      : ",SCREENCODE_COLOR,$00,SCREENCODE_BLANK,SCREENCODE_BLANK
	.byte SCREENCODE_COLOR,HELPER_TEXT," 00-3f"
	
	.byte SCREENCODE_RETURN,SCREENCODE_COLOR,HEADER_COLOR
	.byte "wave        : ",SCREENCODE_COLOR,$00,SCREENCODE_BLANK
	.byte SCREENCODE_COLOR,HELPER_TEXT,"  pulse/saw/tri/noise"
	
	.byte SCREENCODE_RETURN,SCREENCODE_COLOR,HEADER_COLOR
  .byte "width       : ",SCREENCODE_COLOR,$00,SCREENCODE_BLANK
	.byte SCREENCODE_COLOR,HELPER_TEXT,"  (pwm)"

	.byte SCREENCODE_RETURN,SCREENCODE_COLOR,HEADER_COLOR
	.byte "fine tune   : ",SCREENCODE_COLOR,$00,SCREENCODE_BLANK,SCREENCODE_BLANK
	.byte SCREENCODE_COLOR,HELPER_TEXT," signed fx=-1,ex=-2,1x=+1,etc"
	
	.byte SCREENCODE_RETURN,SCREENCODE_COLOR,HEADER_COLOR
	.byte "pitch speed : "

	.byte SCREENCODE_XY,$1B,$1E,SCREENCODE_COLOR,HEADER_COLOR
	.byte "envelopes"
	
	.byte SCREENCODE_XY,$1B,$21
	.byte "volume"
	.byte SCREENCODE_XY,$28,$21
	.byte "pwm"
	.byte SCREENCODE_XY,$35,$21
	.byte "pitch"

	.byte SCREENCODE_XY,$03,$2F
	.byte SCREENCODE_COLOR,HELPER_TEXT
	.byte "scroll for more"

.byte SCREENCODE_XY,$03,$31
	.byte SCREENCODE_COLOR,HELPER_TEXT
	.byte "press f4 to switch to fm"

	.byte 0

enveloope_text:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte "enable:"
	.byte SCREENCODE_RETURN
	.byte "number:"
	.byte SCREENCODE_RETURN
	.byte "speed:"
	.byte SCREENCODE_RETURN
	.byte "length:"
	.byte SCREENCODE_RETURN
	.byte SCREENCODE_RETURN
	.byte "loop:"
	.byte SCREENCODE_RETURN
	.byte "mode:"
	.byte SCREENCODE_RETURN
	.byte "start:"
	.byte SCREENCODE_RETURN
	.byte "end:"
	.byte 0

instrument_name_text:
	.byte SCREENCODE_PRINT_MODE,$01
	.byte SCREENCODE_COLOR,$00
	.byte SCREENCODE_BLANK,SCREENCODE_BLANK
	.byte SCREENCODE_COLOR,HEADER_COLOR,$3A
	.byte SCREENCODE_COLOR,$00
	.byte SCREENCODE_BLANK,SCREENCODE_BLANK,SCREENCODE_BLANK,SCREENCODE_BLANK
	.byte SCREENCODE_BLANK,SCREENCODE_BLANK,SCREENCODE_BLANK,SCREENCODE_BLANK
	.byte SCREENCODE_BLANK,SCREENCODE_BLANK,SCREENCODE_BLANK,SCREENCODE_BLANK
	.byte SCREENCODE_BLANK,SCREENCODE_BLANK,SCREENCODE_BLANK,SCREENCODE_BLANK
	.byte 0