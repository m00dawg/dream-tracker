.proc draw_ui

	MACRO_START_X = $05
	MACRO_START_Y = $0D
	NUMBER_OF_MACROS_TO_DISPLAY = $20
	MACRO_STOP_Y = MACRO_START_Y + NUMBER_OF_MACROS_TO_DISPLAY

	turn_on_layer_0
	vera_layer0_16_color

  ; Set VERA Stride and bank
  lda #$10          ; Set primary address bank to 0, stride to 1
  sta VERA_addr_high 

	; Draw Frame
	stz zp_ARG0	;X1
	stz zp_ARG1	;Y1
	lda #$4F		;X2
	sta zp_ARG2
	lda #$3B		;Y2
	sta zp_ARG3
	lda #FRAME_COLOR
	sta zp_ARG4
	jsr graphics::drawing::draw_rounded_box

	; Clear inner frame
	lda #$01
	sta zp_ARG0
	sta zp_ARG1
	lda #$4E
	sta zp_ARG2
	lda #$3A
	sta zp_ARG3
	lda #$B1
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box

	jsr ui::print_header
  jsr ui::print_song_info
  jsr ui::print_speed
  jsr ui::print_current_order
  jsr ui::print_current_pattern_number
	
	print_string_macro display_elements

; Draw macro row gap
; The bottm layer will be used to pull macro data
	ldx #MACRO_START_X
	ldy #MACRO_START_Y
@draw_colons_loop:
	phy
	print_string_macro_xy display_macro_row 
	ply
	iny
	cpy #MACRO_STOP_Y
	bne @draw_colons_loop

@end:
	rts

display_elements:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte SCREENCODE_XY,$21,$08
	.byte "macros"
	.byte SCREENCODE_XY,MACRO_START_X,MACRO_START_Y - 1
	.byte "## eff0 eff1 eff2 eff3 eff4 eff5 eff6 eff7"
	.byte 0

display_macro_row:
	.byte SCREENCODE_COLOR,$00
	.byte "  "
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte ":"
	.byte SCREENCODE_COLOR,$00
	; 0
	.byte "    "
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte " "
	; 1
	.byte SCREENCODE_COLOR,$00
	.byte "    "
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte " "
	; 2
	.byte SCREENCODE_COLOR,$00
	.byte "    "
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte " "
	; 3
	.byte SCREENCODE_COLOR,$00
	.byte "    "
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte " "
	; 4
	.byte SCREENCODE_COLOR,$00
	.byte "    "
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte " "
	; 5
	.byte SCREENCODE_COLOR,$00
	.byte "    "
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte " "
	; 6
	.byte SCREENCODE_COLOR,$00
	.byte "    "
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte " "
	; 7
	.byte SCREENCODE_COLOR,$00
	.byte "    "
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte " "
	.byte 0


.endproc