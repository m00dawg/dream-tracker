; Load all macros from memory and place in VRAM
;
; zp_TMP0 = $10 (16)

.proc load_macros
; Move screen left 13 characters (8*13)
; And down 13 characters (8*13)
; Numbers are negative since down/right in VERA 
; are negative movements
; We draw the entire env to VRAM so we can scroll
; Same for both short and full envs since we're scrolling the _top_ 
; of the screen downwards
MACRO_SCROLL_X_HIGH = $FF
MACRO_SCROLL_X_LOW = $D7
MACRO_SCROLL_Y_HIGH = $FF
MACRO_SCROLL_Y_LOW = $98


	; Set XY
	lda #$11          ; Set primary address bank to 1, stride to 1
  sta VERA_addr_high 

  lda #MACRO_SCROLL_X_HIGH
  sta VERA_L0_hscroll_h
  lda #MACRO_SCROLL_X_LOW
  sta VERA_L0_hscroll_l
  lda #MACRO_SCROLL_Y_HIGH
  sta VERA_L0_vscroll_h
  lda #MACRO_SCROLL_Y_LOW
  sta VERA_L0_vscroll_l

	rambank zp_BANK_MISC

	lda #<MACROS_START_ADDRESS
	sta zp_ADDR0
	lda #>MACROS_START_ADDRESS
	sta zp_ADDR0 + 1

	ldy #$00
@row_loop:
	lda #$00
	; y is row
	jsr graphics::drawing::goto_xy
@macro_loop:
	; Print Macro Number
	; Set macro color
	lda #HEADER_COLOR
	sta zp_TEXT_COLOR
	; Print macro number
	tya
	jsr graphics::printing::print_hex

	lda #SCREENCODE_BLANK
	jsr graphics::printing::print_alpha_char

	lda #PATTERN_EFX_COLOR
	sta zp_TEXT_COLOR

	; Print next 2 bytes, then skip and do that 8 times, 1 for each effect
	phy
	ldx #EFFECTS_PER_MACRO
	ldy #$00
@line_loop:
	lda (zp_ADDR0),y
	jsr graphics::printing::print_hex
	iny 
	lda (zp_ADDR0),y
	jsr graphics::printing::print_hex
	iny 

	lda #SCREENCODE_BLANK
	jsr graphics::printing::print_alpha_char
	dex
	bne @line_loop
@line_loop_bottom:
	; Add 16 to address (as that is how many times we looped up above)
	clc
	lda zp_ADDR0
	adc #$10
	sta zp_ADDR0
	lda zp_ADDR0+1
	adc #$00
	sta zp_ADDR0+1
	ply
@row_loop_bottom:
	iny
	cpy #NUM_MACROS
	bne @row_loop

@end:
	; Load 8 bytes
	rts
.endproc