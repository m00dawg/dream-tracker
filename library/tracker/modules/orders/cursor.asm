; Cusor procs

; On up/down check if we are at the end of the entire list
; and if we are at the end of the view.
; If at the end of the view, will need to redraw the list
; Also update where in the order list we are.
.proc cursor_up
@start:
  lda order_list_position
  beq @cursor_scroll_up_jump
  jsr ui::cursor_up
  dec order_list_position
  rts
@cursor_scroll_up_jump:
  jsr scroll_orders_up
  rts
.endproc

.proc cursor_down
@start:
  lda order_list_position
  cmp #CURSOR_STOP_Y
  beq @cursor_scroll_down_jump
  jsr ui::cursor_down
  inc order_list_position
  rts
  ; If we're at the bottom of the screen, scroll the orders
@cursor_scroll_down_jump:
  jsr scroll_orders_down
  rts
.endproc

; Move by $08 up in list
.proc page_up
  ldx #$00
@loop:
  phx
  jsr cursor_up
  plx
  inx
  cpx #$08
  bne @loop
  rts
.endproc
  
  
; Move by $08 down in list
.proc page_down
  ldx #$00
@loop:
  phx
  jsr cursor_down
  plx
  inx
  cpx #$08
  bne @loop
  rts
.endproc

.proc cursor_left
; Track the column position of the cursor here as well.
; Needed for further down.
@cursor_left:
  lda order_list_column
  beq @cursor_left_end
  jsr ui::cursor_left
  dec order_list_column
@cursor_left_end:
  jmp main_application_loop
.endproc

.proc cursor_right
@cursor_right:
  lda order_list_column
  cmp #ORDERS_MAX_COLUMN
  beq @cursor_right_end
  jsr ui::cursor_right
  inc order_list_column
@cursor_right_end:
  jmp main_application_loop
.endproc