.segment "ORDERS_MODULE"

; How to save the order list:
; - Track cursor position in 0,1
; - WHen moving left or right, check 0,1
; - If a char was typed at pos 1, read line. If it's a valid number, store it
; - The cursor should be relative to the order list (I tink?)
;   + That is, drawing the order list should be decoupled. We place the
;     cursor where it needs to go.
;   + This is because arrow up/down vs pg up/dn will be different anyway

; pg up/dn, we redraw the entire list, skipping by the number of rows
; displayed AND update the cursor (most of the time the cursor won't
; move unless we're close to the end)

; For arrow up/dn, if we're at the bottom or top of the current list,
; we redraw the list by one (if there is more orders in the desired direction)
; We don't need to move the cursor, but do need to update its order list
; position

; SO in other words, the cursor is always matched to the order number
; and not the position on the screen.

.scope orders

.word init
.word keyboard_loop

; Variables

; Where to start the order list display from
; (such as if we're scrolled further down)
order_list_start:       .byte $00

; Where the cursor is in the order list
order_list_position:    .byte $00

; Which column the cursor is in (there are only 2 so 0/1)
order_list_column:      .byte $00

; Temp Vars
ORDER_NUMBER_SAVE   =      zp_TMP0
PATTERN_NUMBER_SAVE =      zp_TMP1

; Constants

; Constrain movement of the cursor to only the order list
CURSOR_START_X = $24
CURSOR_START_Y = $0A
CURSOR_STOP_Y = $2F

; How many columns we have (2)
ORDERS_MIN_COLUMN = $00
ORDERS_MAX_COLUMN = $01

ORDERS_HEADER_X = $20
ORDERS_HEADER_Y = $08
ORDERS_STOP_Y = $3A
MAX_PATTERN_X = $15
MAX_PATTERN_Y = $0A

FRAME_COLOR = $B1
HEADER_COLOR = $B1
LABEL_COLOR = $BC


.include "library/tracker/modules/orders/draw_ui.asm"
.include "library/tracker/modules/orders/draw_orders_frame.asm"
.include "library/tracker/modules/orders/cursor.asm"

.proc init
init:
	;lda #ORDERS_FILENAME_LENGTH
	;ldx #<ORDERS_FILENAME
	;ldy #>ORDERS_FILENAME
	;jsr files::load_to_vram
  jsr draw_ui
  jsr ui::update_upper_info
  jsr ui::print_song_info
 
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high
  vera_layer0_16_color
  stz r1
  stz order_list_start
  ;lda zp_ORDER_NUMBER
  ;sta order_list_start
  stz order_list_position
  stz order_list_column

  lda #TITLE_COLORS
  sta zp_TEXT_COLOR
  lda #MAX_PATTERN_X
  ldy #MAX_PATTERN_Y
  jsr graphics::drawing::goto_xy
  lda zp_MAX_PATTERN
  jsr graphics::printing::print_hex
  jsr draw_orders_frame
  stz RAM_BANK

@cursor_start_position:
  stz cursor_layer
  lda #CURSOR_START_X
  sta cursor_x
  lda #CURSOR_START_Y
  sta cursor_y
  jsr graphics::drawing::cursor_plot
  stz zp_KEY_PRESSED
  ; Fall through to loop
.endproc

; Jumped from main application loop
.proc keyboard_loop
@keyboard_loop:
@check_keyboard:
  lda zp_KEY_PRESSED
  cmp #KEY_UP
  beq @cursor_up_jump
  cmp #KEY_DOWN
  beq @cursor_down_jump
  cmp #KEY_PGUP
  beq @page_up_jump
  cmp #KEY_PGDN
  beq @page_down_jump
  cmp #KEY_LEFT
  beq @cursor_left_jump
  cmp #KEY_RIGHT
  beq @cursor_right_jump
  cmp #PETSCII_G
  beq @edit_pattern_jump
  cmp #$30
  bpl @print_alphanumeric
  jmp main_application_loop

; Local jumps
@edit_pattern_jump:
  lda order_list_position
  sta zp_ORDER_NUMBER
  jmp @edit_pattern

@cursor_up_jump:
  jsr cursor_up
  jmp main_application_loop

@cursor_down_jump:
  jsr cursor_down
  jmp main_application_loop

@cursor_left_jump:
  jmp cursor_left
@cursor_right_jump:
  jmp cursor_right


@page_up_jump:
  jsr page_up
  jmp main_application_loop

@page_down_jump:
  jsr page_down
  jmp main_application_loop

; Print the # the user typed. If ordrer_list_column is 1, we are in the
; second column, so we need to see if it's a valid pattern number and, if so,
; we need to call a routine to update the order list array.
@print_alphanumeric:
  pha
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  pla
  cmp #PETSCII_AT
  bmi @print_numeric
  cmp #PETSCII_G
  ;bpl @print_end
  ; Fix for running out of relative branch space on 65C02
  bmi @print_alpha
  jmp main_application_loop
@print_alpha:
  sbc #$3F  ; add 40 to get to the letter screencodes, then fall through
@print_numeric:
  sta VERA_data0
  inc VERA_addr_low
  lda #EDITABLE_TEXT_COLORS
  sta VERA_data0

  ; If we printed a character, and we're not at the last column
  ; move to the right
  lda order_list_column
  cmp #ORDERS_MAX_COLUMN
  ;beq @save_order
  beq @save_order
  jsr ui::cursor_right
  inc order_list_column
  jmp @print_end
; If we're already on the last column,
; check to see if we should save the order

@save_order:
  sei
  stz order_list_column
  lda #$20  ; skip over colors
  sta VERA_addr_high

  ; Go to the order number displayed so we can grab that
  ; and know where to save the pattern number
  lda #ORDERS_HEADER_X
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1
  jsr graphics::printing::chars_to_number
  sta ORDER_NUMBER_SAVE

  ; Skip over : and space
  lda VERA_data0
  lda VERA_data0

  ; Now read the pattern value from screen
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1
  jsr graphics::printing::chars_to_number
  sta PATTERN_NUMBER_SAVE

  ; Reset cursor back to where it was
  lda #CURSOR_START_X 
  sta cursor_x
  jsr graphics::drawing::cursor_plot

  ; Finally save the order
  push_rambank
  rambank zp_BANK_MISC
  ldy ORDER_NUMBER_SAVE
  lda PATTERN_NUMBER_SAVE

  ; If the pattern is 00, clear it
  cmp #$00
  bne @check_max_pattern
  lda #$00
  sta order_list,y
  jsr draw_orders_frame
  jsr graphics::drawing::cursor_plot
  pop_rambank
  cli
  jmp main_application_loop

  ; If the pattern is > max pattern, write a 0
  ; and redraw screen to clear bad pattern out
@check_max_pattern:
  cmp zp_MAX_PATTERN
  ble @save_end
  lda #$00
  sta order_list,y
  jsr draw_orders_frame
  jsr graphics::drawing::cursor_plot
  jmp main_application_loop

@save_end:
  sta order_list,y

@print_end:
@main_orders_loop_jump:
  jmp main_application_loop

; Edit pattern specified at cursor
@edit_pattern:
  ; If we're on the second column, move over one
  lda order_list_column
  beq @get_pattern_at_cursor
  jsr ui::cursor_left
  dec order_list_column
@get_pattern_at_cursor:
  lda #$20  ; skip over colors
  sta VERA_addr_high
  ldy cursor_y
  sty VERA_addr_med
  ; X-pos of first digit
  lda cursor_x
  asl ; because 2 bytes per X pos (second being color)
  sta VERA_addr_low

  ; First character
  lda VERA_data0
  sta r0

  ; Second character
  lda VERA_data0
  sta r1

  jsr graphics::printing::chars_to_number
  sta zp_PATTERN_NUMBER
  ldx #EDIT_PATTERN_MODULE
  stx zp_CURRENT_MODULE  
  jmp init_jump

.endproc


.proc scroll_orders_up
; If we're at the top of the screen, but we have more
; orders, scroll the order list
@start:
  dec order_list_start
  jsr draw_orders_frame
  jsr graphics::drawing::cursor_plot
  rts
.endproc

.proc scroll_orders_down
; If we're at the bottom of the screen, but we have more
; orders, scroll the order list
@start:
  inc order_list_start
  jsr draw_orders_frame
  jsr graphics::drawing::cursor_plot
  rts
.endproc

.endscope
