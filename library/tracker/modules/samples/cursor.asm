.scope cursor

.proc cursor_up
    lda edit_mode
    bne @sample_edit
    jsr sample_name_cursor_up
    rts
@sample_edit:
    jsr sample_edit_cursor_up
    rts
.endproc

.proc cursor_down
    lda edit_mode
    bne @sample_edit
    jsr sample_name_cursor_down
    rts
@sample_edit:
    jsr sample_edit_cursor_down
    rts
.endproc

.proc cursor_left
    lda edit_mode
    bne @sample_edit
    jsr sample_name_cursor_left
    rts
@sample_edit:
    jsr sample_edit_cursor_left
    rts
.endproc

.proc cursor_right
    lda edit_mode
    bne @sample_edit
    jsr sample_name_cursor_right
    rts
@sample_edit:
    jsr sample_edit_cursor_right
    rts
.endproc

; Instrument Name Movements
.proc sample_name_cursor_up
    jsr ui::instruments::cursor::name_cursor_up
    jsr load_sample
    rts
.endproc

.proc sample_name_cursor_down
    jsr ui::instruments::cursor::name_cursor_down
    jsr load_sample
    rts
.endproc

.proc sample_name_cursor_left
    jsr ui::instruments::cursor::name_cursor_left
    rts
.endproc

.proc sample_name_cursor_right
    jsr ui::instruments::cursor::name_cursor_right
    rts
.endproc

; Sample Edit movements
.proc sample_edit_cursor_left
    lda sample_edit_pos
    beq @volume
@filename:
    jsr ui::cursor_left
    rts
; We can't move left or right when in the volume column
@volume:
    rts
.endproc

.proc sample_edit_cursor_right
    lda sample_edit_pos
    beq @volume
@filename:
    jsr ui::cursor_right
    rts
; We can't move left or right when in the volume column
@volume:
    rts
.endproc

.proc sample_edit_cursor_down
    lda sample_edit_pos
    beq @goto_filename
@goto_volume:
    stz sample_edit_pos
    bra @end
@goto_filename:
    inc sample_edit_pos
@end:
    jsr goto_sample_edit_position
    rts
.endproc

.proc sample_edit_cursor_up
    lda sample_edit_pos
    beq @goto_filename
@goto_volume:
    stz sample_edit_pos
    bra @end
@goto_filename:
    inc sample_edit_pos
@end:
    jsr goto_sample_edit_position
    rts
.endproc

.proc goto_sample_edit_position
    jsr graphics::drawing::cursor_unplot
    ldx sample_edit_pos
    lda sample_edit_x_pos_lut,x
    sta cursor_x
    lda sample_edit_y_pos_lut,x
    sta cursor_y
    lda cursor_x
    ldy cursor_y
    jsr graphics::drawing::goto_xy
    jsr graphics::drawing::cursor_plot
    rts
.endproc

.endscope