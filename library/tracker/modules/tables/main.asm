.segment "TABLES_MODULE"

; Table effects menu
.scope tables

.word init
.word keyboard_loop

TABLES_FILENAME:
	.byte "scr/tables.scr"
TABLES_FILENAME_LENGTH = $0E

; Constants
TABLE_BASE_ADDRESS_HIGH = $A0
EFFECTS_PER_ROW = $04
NUM_ROWS = $20
TABLE_START_X = $0E
TABLE_START_Y = $0D

; Scope Variables
table_address = zp_ADDR0
table_number = zp_TMP0
effect_count = zp_TMP1
row_count = zp_TMP2

.include "library/tracker/modules/tables/load.asm"

.proc init
init:
	lda #TABLES_FILENAME_LENGTH
	ldx #<TABLES_FILENAME
	ldy #>TABLES_FILENAME
	jsr files::load_to_vram

  vera_layer0_16_color

	stz zp_KEY_PRESSED

	stz table_number
.endproc

.proc keyboard_loop
keyboard_loop:
  	jmp main_application_loop
.endproc

.endscope