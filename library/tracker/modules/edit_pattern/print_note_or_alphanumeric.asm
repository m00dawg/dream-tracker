.proc print_note_or_alphanumeric
; If we're in the note column, print a note
; Otherwise print the hex-number the user typed
@start:
  ;sei
  pha
  ; Make sure we're on layer 1
  lda #$01
  sta VERA_addr_high
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  lda column_pos
  beq @print_note
; Print the # the user typed.
@print_alphanumeric:
  pla
  cmp #PETSCII_AT
  bmi @print_numeric
  cmp #PETSCII_G  ; Do not print invalid chars
  bpl @print_end
@print_alpha:
  sbc #$3F  ; add 40 to get to the letter screencodes, then fall through
@print_numeric:
  sta VERA_data0
  inc VERA_addr_low
  lda #EDITABLE_TEXT_COLORS
  sta VERA_data0

  ; If we printed a character, and we're not at the last column
  ; move to the right
  lda column_pos
  cmp #LAST_COLUMN_POSITION
  beq @print_end
  jsr ui::cursor_right
  inc column_pos
  jmp @print_end

@print_note:
  lda #PATTERN_NOTE_COLOR
  sta COLOR
  lda #$11
  sta VERA_addr_high
  pla
  jsr key_to_note

  lda #$01
  sta VERA_addr_high

  lda #PATTERN_INST_COLOR
  sta COLOR

  ; If the note was a noteoff, don't add inst #
  lda zp_NOTE_NUMERIC
  cmp #NOTEOFF
  beq @end_print_inst
  ; If the note was a noterel, don't add inst #
  cmp #NOTEREL
  beq @end_print_inst
@print_inst:
  lda zp_INSTRUMENT
  jsr graphics::printing::print_hex
  ;jmp @print_end
@end_print_inst:
  jsr save_row_channel
  lda #$00
  sta column_pos
  jsr tracker::modules::edit_pattern::cursor::cursor_down
@print_end:
  jsr save_row_channel
  ;cli
  jmp cleanup
.endproc