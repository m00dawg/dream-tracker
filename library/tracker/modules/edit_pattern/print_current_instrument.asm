.proc print_current_instrument

instrument_name_address = zp_ADDR1
temp_address = zp_ADDR0

print_current_instrument:
  ; Set stride to 1, high bit to 0
  lda #$10
  sta VERA_addr_high
  lda #INSTRUMENT_NUMBER_DISPLAY_X
  ldy #INSTRUMENT_NUMBER_DISPLAY_Y
  jsr graphics::drawing::goto_xy
  ; Color
  set_text_color #TEXT_COLORS
  lda zp_INSTRUMENT
  jsr graphics::printing::print_hex

@print_instrument_name:
  lda tracker::modules::edit_pattern::channel_number
  cmp #sound::pcm::START_CHANNEL
  beq @start_pcm
  cmp #sound::fm::START_CHANNEL
  bge @start_fm
@start_vera:
  lda #>sound::vera::BASE_NAMES_ADDRESS
  bra @insturment_address
@start_fm:
  lda #>sound::fm::BASE_NAMES_ADDRESS
  bra @insturment_address
@start_pcm:
  lda #>sound::pcm::BASE_NAMES_ADDRESS

@insturment_address:
  stz instrument_name_address
  sta instrument_name_address + 1

  lda #INSTRUMENT_NAME_DISPLAY_X
  ldy #INSTRUMENT_NAME_DISPLAY_Y
  jsr graphics::drawing::goto_xy

  ldx zp_INSTRUMENT
  beq @instrument_name_address_found
@instrument_name_address_loop:
  add16to8 instrument_name_address, #sound::INSTRUMENT_NAME_LENGTH, temp_address
  lda temp_address
  sta instrument_name_address
  lda temp_address + 1
  sta instrument_name_address + 1
  dex
  bne @instrument_name_address_loop

@instrument_name_address_found:
  lda RAM_BANK
  pha
  lda tracker::modules::edit_pattern::channel_number
  cmp #sound::pcm::START_CHANNEL
  beq @pcm_bank
  cmp #sound::fm::START_CHANNEL
  bge @fm_bank
@vera_bank:
  rambank zp_BANK_VINS
  bra @print_instrument_name_loop_start
@fm_bank:
  rambank zp_BANK_FINS
  bra @print_instrument_name_loop_start
@pcm_bank:
  rambank zp_BANK_MISC

@print_instrument_name_loop_start:
  ldy #$00
@print_instrument_name_loop:
  lda (instrument_name_address),y
  jsr graphics::printing::print_alpha_char
  iny
  cpy #sound::INSTRUMENT_NAME_LENGTH
  bne @print_instrument_name_loop
@end:
  pla
  sta RAM_BANK
  rts
.endproc
