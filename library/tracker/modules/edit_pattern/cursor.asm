.scope cursor

.proc cursor_up
@start:
  jsr graphics::drawing::cursor_unplot
  lda cursor_y
  sbc #$00
  sta cursor_y
  jsr graphics::drawing::cursor_plot

  lda row_number
  beq @cursor_up_goto_bottom_of_pattern
  lda screen_row
  beq @cursor_up_scroll_down
  dec row_number
  dec screen_row
  jsr ui::print_row_number
  jmp @cursor_up_end
@cursor_up_goto_bottom_of_pattern:
  jsr goto_bottom
  jmp @cursor_up_end
@cursor_up_scroll_down:
  jsr ui::scroll_down
  dec row_number
@cursor_up_end:
  jsr ui::print_row_number
  rts
.endproc

.proc cursor_down
@cursor_down:
  jsr graphics::drawing::cursor_unplot
  ;lda tracker::modules::edit_pattern::cursor_y
  lda cursor_y
  clc
  adc #$01
  ;sta tracker::modules::edit_pattern::cursor_y
  sta cursor_y
  jsr graphics::drawing::cursor_plot

  inc row_number
  lda row_number
  cmp #ROW_MAX
  beq @cursor_down_goto_top_of_pattern
  lda screen_row
  cmp #SCREEN_ROW_MAX
  beq @cursor_down_scroll_up
  inc screen_row
  jmp @cursor_down_end
@cursor_down_goto_top_of_pattern:
  jsr reset_scroll_position
  jmp @cursor_down_end
@cursor_down_scroll_up:
  jsr ui::scroll_up
@cursor_down_end:
  jsr ui::print_row_number
  rts
.endproc

.proc cursor_left
@cursor_left:
  lda tracker::modules::edit_pattern::column_pos
  beq @cursor_left_channel
  cmp #INSTRUMENT_COLUMN_POSITIION
  beq @cursor_left_note
  jsr ui::cursor_left
  dec tracker::modules::edit_pattern::column_pos
  rts
@cursor_left_note:
  jsr ui::cursor_left
  jsr ui::cursor_left
  jsr ui::cursor_left
  stz tracker::modules::edit_pattern::column_pos
  rts
@cursor_left_channel:
  ; If we're at the 0th screen channel, check to see if we're on the
  ; 0th actual channel and, if so, we're already as far left as we can go.
  ; Otherwise, go left normally
  lda tracker::modules::edit_pattern::screen_channel
  bne @cursor_move_left_channel
  ; If we're already at channel 0, we can't go left anymore
  lda tracker::modules::edit_pattern::start_channel
  beq @cursor_left_channel_end
  ; If we're on the first column but our left-most channel isn't zero,
  ; we need to move the pattern view over
@cursor_left_at_first_channel:
  dec tracker::modules::edit_pattern::start_channel
  jsr draw_pattern_frame
  jsr print_pattern
  ; Move to beginning of shifted channel
  lda #CURSOR_X_START
  sta cursor_x
  jsr graphics::drawing::cursor_plot
  stz tracker::modules::edit_pattern::column_pos
  dec tracker::modules::edit_pattern::channel_number
  jmp @cursor_left_channel_end
@cursor_move_left_channel:
  jsr ui::cursor_left
  jsr ui::cursor_left
  dec tracker::modules::edit_pattern::screen_channel
  dec tracker::modules::edit_pattern::channel_number
  ; We're moving to the last column of the previous channel
  lda #LAST_COLUMN_POSITION
  sta tracker::modules::edit_pattern::column_pos
@cursor_left_channel_end:
  rts
.endproc

.proc cursor_right
@cursor_right:
  lda tracker::modules::edit_pattern::column_pos
  beq @cursor_right_note
  cmp #LAST_COLUMN_POSITION
  beq @cursor_right_channel
  jsr ui::cursor_right
  inc tracker::modules::edit_pattern::column_pos
  rts
@cursor_right_note:
  jsr ui::cursor_right
  jsr ui::cursor_right
  jsr ui::cursor_right
  lda #INSTRUMENT_COLUMN_POSITIION
  sta tracker::modules::edit_pattern::column_pos
  rts
@cursor_right_channel:
  ; If we're on the last column, we need to move the pattern view over
  lda tracker::modules::edit_pattern::screen_channel
  cmp #NUM_CHANNEL_COLUMNS - 1
  bne @cursor_move_right_channel

  ; Check if we're not on the max possible channel
  lda tracker::modules::edit_pattern::start_channel
  clc
  adc #NUM_CHANNEL_COLUMNS - 1
  cmp #NUMBER_OF_CHANNELS
  bne @cursor_right_not_at_last_channel
  rts
@cursor_right_not_at_last_channel:
  inc tracker::modules::edit_pattern::start_channel 
  jsr draw_pattern_frame
  jsr print_pattern

  ; Move to beginning of shifted channel
  lda #CURSOR_X_START_OF_LAST_CHNNAEL
  sta cursor_x
  jsr graphics::drawing::cursor_plot
  jmp @cursor_right_channel_end
@cursor_move_right_channel:
  jsr ui::cursor_right
  jsr ui::cursor_right
  inc tracker::modules::edit_pattern::screen_channel
@cursor_right_channel_end:
  stz tracker::modules::edit_pattern::column_pos
  inc tracker::modules::edit_pattern::channel_number
  rts
.endproc

.proc cursor_tab_left
@cursor_tab_left:
  ; If screen channel is 0, and column pos is 0, just move left once
  lda tracker::modules::edit_pattern::screen_channel
  bne @cursor_tab_left_loop_start
  lda tracker::modules::edit_pattern::column_pos
  bne @cursor_tab_left_loop_start
  jsr tracker::modules::edit_pattern::cursor::cursor_left
  jmp cleanup
@cursor_tab_left_loop_start:
  ldx #PATTERN_TAB_LEFT_STEP
@cursor_tab_left_loop:
  phx
  jsr tracker::modules::edit_pattern::cursor::cursor_left
  plx
  dex
  cpx #$00
  bne @cursor_tab_left_loop
  jmp cleanup
.endproc

.proc cursor_tab_right
@cursor_tab_right:
  ldx #PATTERN_TAB_RIGHT_STEP
@cursor_tab_right_loop:
  phx
  jsr tracker::modules::edit_pattern::cursor::cursor_right
  plx
  dex
  cpx #$00
  bne @cursor_tab_right_loop
  jmp cleanup
.endproc

.proc goto_bottom
  lda #$00
  sta VERA_L0_vscroll_h
  lda #$28    ; Roll pattern up so the bottom of the pattern is at the bottom of screen
  sta VERA_L0_vscroll_l
  lda #ROW_MAX - 1
  sta row_number
  lda #SCREEN_ROW_MAX
  sta screen_row
  lda #$01
  sta cursor_layer
  lda cursor_x
  lda #$3F
  sta cursor_y
  jsr graphics::drawing::cursor_plot
  rts
.endproc

.endscope