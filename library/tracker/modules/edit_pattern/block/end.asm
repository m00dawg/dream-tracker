
; Set end of copy block
.proc end
  @end:
  ; Store the channel at cursor position
  @end_channel:
    lda tracker::modules::edit_pattern::channel_number
  ; If the end channel is greater than the start channel, we need to swap
    cmp block_begin_channel
    bcs @no_channel_swap
  @swap_channels:
    cpm block_begin_channel, block_end_channel
    lda tracker::modules::edit_pattern::channel_number
    sta block_begin_channel
    jmp @end_row
  @no_channel_swap:
    sta block_end_channel

  ; Store the row at current cursor position.
  ; If the end row is greater than the start row, swap them
  @end_row:
    lda row_number
    cmp block_begin_row
    bcs @no_row_swap
  @swap_rows:
    cpm block_begin_row, block_end_row
    lda row_number
    sta block_begin_row
    jmp @finish
  @no_row_swap:
    sta block_end_row

  @block_maths:
    ; Find how many channels the block is
    sub block_end_channel, block_begin_channel
    adc #$00
    sta block_num_channels

    ; Find out how many rows the block is
    sub block_end_row, block_begin_row
    adc #$00
    sta block_num_rows

    ; Figure out the memory size per row
    ldx block_num_channels
    lda #$00
  @count_channel_loop:
    clc
    adc #TOTAL_BYTES_PER_CHANNEL
    dex
    cpx #$00
    bne @count_channel_loop
    sta block_bytes_per_row

  ; Finish end block mark
  @finish:
    ;jsr ui::update_upper_info
    jsr block::print_block
    jmp cleanup
.endproc