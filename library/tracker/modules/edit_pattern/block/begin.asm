
; Set start of copy block
.proc begin
  @begin:
    ; Store the pattern, channel and row number at cursor position
    lda zp_PATTERN_NUMBER
    sta block_source_pattern
    lda tracker::modules::edit_pattern::channel_number
    sta block_begin_channel
    lda row_number
    sta block_begin_row
  @end:
    ;jsr ui::update_upper_info
    jmp cleanup
.endproc
