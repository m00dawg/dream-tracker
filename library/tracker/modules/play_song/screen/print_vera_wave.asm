.proc print_vera_wave
@print_wave:
  jsr skip_two
  lda sound::vera::channel_wave,x
	and #%11000000
	cmp #PULSE_TYPE
	beq @waveform_pulse
	cmp #SAW_TYPE
	beq @waveform_saw
	cmp #TRI_TYPE
	beq @waveform_tri
@set_waveform_noise:
	lda #SCREENCODE_N
	jmp @waveform_print
@waveform_pulse:
	lda #SCREENCODE_P
	jmp @waveform_print
@waveform_saw:
	lda #SCREENCODE_S
	jmp @waveform_print
@waveform_tri:
	lda #SCREENCODE_T
@waveform_print:
  jsr graphics::printing::print_alpha_char
  ; Skip 1
  lda VERA_data0
  lda VERA_data0
  rts
.endproc