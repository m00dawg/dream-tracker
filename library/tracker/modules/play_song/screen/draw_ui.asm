; Draw the elements on the screen
; (previouisly done by loading a buffer)
;
; zp_TMP0 = channel #
; zp_TMP1 = channel screen row
; zp_TMP2 = channel stop

.proc draw_ui
	PSG_HEADER_X = $06
	PSG_HEADER_Y = $08
	PSG_LABEL_X = $02
	PSG_LABEL_Y = $09
	
	CHANNEL_START_X = $06
	;PSG_CHANNEL_START_Y = $08
	PSG_CHANNEL_START_Y = $09
	FM_CHANNEL_START_Y = $1A
	PCM_CHANNEL_START_Y = $23

	PSG_CHANNEL_COLOR = $BE
	FM_CHANNEL_COLOR = $BD
	PCM_CHANNEL_COLOR = $B8

	FM_HEADER_X = $06
	FM_HEADER_Y = $19
	FM_LABEL_X = $02
	FM_LABEL_Y = $1A
	
	PCM_HEADER_X = $06
	PCM_HEADER_Y = $22
	PCM_LABEL_X = $02
	PCM_LABEL_Y = $23

	TRACKER_SCROLL_X1 = $01
	TRACKER_SCROLL_Y1 = $26
	TRACKER_SCROLL_X2 = $4D
	TRACKER_SCROLL_Y2 = $3A

	TRACKER_SCROLL_PSG_HEADER_X = $03
	TRACKER_SCROLL_PSG_HEADER_Y = $25
	TRACKER_SCROLL_PSG_HEADER_COLOR = $BE

	TRACKER_SCROLL_FM_HEADER_X = $33
	TRACKER_SCROLL_FM_HEADER_Y = $25
	TRACKER_SCROLL_FM_HEADER_COLOR = $BD

	TRACKER_SCROLL_PCM_HEADER_X = $4B
	TRACKER_SCROLL_PCM_HEADER_Y = $25
	TRACKER_SCROLL_PCM_HEADER_COLOR = $B8

	FRAME_COLOR = $B1
	HEADER_COLOR = $B1
	LABEL_COLOR = $BC

	CHANNEL_NUMBER = zp_TMP0
	CHANNEL_Y = zp_TMP1
	CHANNEL_STOP = zp_TMP2

@start:
	vera_layer0_256_color
	; Change $0B pallete color to a hint of blue
	ldx #$16
	lda #$12
	sta palette,x
	inx
	lda #$01
	sta palette,x

	;ldx #$02
	;acc16b
	;lda #$FF
	;sta palette,x
	;acc8b

	jsr graphics::vera::load_palette_16



  ; Set VERA Stride and bank
  lda #$10          ; Set primary address bank to 0, stride to 1
  sta VERA_addr_high 

	; Draw Frame
	stz zp_ARG0	;X1
	stz zp_ARG1	;Y1
	lda #$4F		;X2
	sta zp_ARG2
	lda #$3B		;Y2
	sta zp_ARG3
	lda #FRAME_COLOR
	sta zp_ARG4
	jsr graphics::drawing::draw_rounded_box

	; Clear inner frame
	lda #$01
	sta zp_ARG0
	sta zp_ARG1
	lda #$4E
	sta zp_ARG2
	lda #$3A
	sta zp_ARG3
	lda #$B1
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box

	; Make space for pattern scroll
	lda #TRACKER_SCROLL_X1
	sta zp_ARG0
	lda #TRACKER_SCROLL_Y1
	sta zp_ARG1
	lda #TRACKER_SCROLL_X2
	sta zp_ARG2
	lda #TRACKER_SCROLL_Y2
	sta zp_ARG3
	lda #$00
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box

	jsr ui::print_header
  jsr ui::print_song_info
  jsr ui::print_speed
	jsr ui::print_current_octave
  jsr ui::print_current_order
  jsr ui::print_current_pattern_number

@number_psg_channels:
	lda #PSG_CHANNEL_COLOR
	sta zp_TEXT_COLOR
	lda #sound::vera::NUM_CHANNELS
	sta CHANNEL_STOP
	ldy #PSG_CHANNEL_START_Y
	sty CHANNEL_Y
	stz CHANNEL_NUMBER
	jsr @print_channel_loop
@number_fm_channels:
	lda #FM_CHANNEL_COLOR
	sta zp_TEXT_COLOR
	lda #sound::vera::NUM_CHANNELS
	clc
	adc #sound::fm::NUM_CHANNELS
	sta CHANNEL_STOP
	ldy #FM_CHANNEL_START_Y
	sty CHANNEL_Y
	jsr @print_channel_loop
@number_pcm_channels:
	lda #PCM_CHANNEL_COLOR
	sta zp_TEXT_COLOR
	lda #CHANNEL_START_X
	ldy #PCM_CHANNEL_START_Y
	jsr graphics::drawing::goto_xy
	lda #sound::vera::NUM_CHANNELS
	clc
	adc #sound::fm::NUM_CHANNELS
	adc $01
	jsr graphics::printing::print_hex


	print_string_macro text
	rts

@print_channel_loop:
	lda #CHANNEL_START_X
	ldy CHANNEL_Y
	jsr graphics::drawing::goto_xy
	lda CHANNEL_NUMBER
	jsr graphics::printing::print_hex
	inc CHANNEL_NUMBER
	inc CHANNEL_Y
	lda CHANNEL_NUMBER
	cmp CHANNEL_STOP
	bne @print_channel_loop
	rts

text:
psg_header:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte SCREENCODE_XY,PSG_HEADER_X,PSG_HEADER_Y
	.byte "ch note ins vol att pan wav pwm arp gld pth vol pwm vrb tlo plo"
psg_label:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte SCREENCODE_XY,PSG_LABEL_X,PSG_LABEL_Y
	.byte "psg"
fm_header:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte SCREENCODE_XY,FM_HEADER_X,FM_HEADER_Y
	.byte "ch note ins vol att pan alg"
fm_label:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte SCREENCODE_XY,FM_LABEL_X,FM_LABEL_Y
	.byte " fm"
pcm_header:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte SCREENCODE_XY,PCM_HEADER_X,PCM_HEADER_Y
	.byte "ch note ins vol"
pcm_label:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte SCREENCODE_XY,PCM_LABEL_X,PCM_LABEL_Y
	.byte "pcm"
tracker_scroll_psg_header:
	.byte SCREENCODE_COLOR,TRACKER_SCROLL_PSG_HEADER_COLOR
	.byte SCREENCODE_XY,TRACKER_SCROLL_PSG_HEADER_X,TRACKER_SCROLL_PSG_HEADER_Y
	.byte "00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f"
tracker_scroll_fm_header:
	.byte SCREENCODE_COLOR,TRACKER_SCROLL_FM_HEADER_COLOR
	.byte SCREENCODE_XY,TRACKER_SCROLL_FM_HEADER_X,TRACKER_SCROLL_FM_HEADER_Y
	.byte "10 11 12 13 14 15 16 17"
tracker_scroll_pcm_header:
	.byte SCREENCODE_COLOR,TRACKER_SCROLL_PCM_HEADER_COLOR
	.byte SCREENCODE_XY,TRACKER_SCROLL_PCM_HEADER_X,TRACKER_SCROLL_PCM_HEADER_Y
	.byte "18 ",0
.endproc