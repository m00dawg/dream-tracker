; Print scroll row. Similar to read_row in play row
; zp_TMP0 = Volume
; zp_TMP1 = Jump Effect
; zp_TMP2 = Jump Effect Value

; This only accounts for the _last_ jump effect in the row
; such that if multiple effects are specified, only the last
; one gets evaluated.

.proc print_row
  TEMP_VOLUME = zp_TMP0
  TEMP_EFFECT_PARAM = zp_TMP1
  TEMP_EFFECT_VALUE = zp_TMP2

  lda #SCROLL_NOTE_COLOR
	sta zp_ARG0
	lda #SCROLL_NOTEREL_COLOR
	sta zp_ARG1
	lda #SCROLL_NOTEOFF_COLOR
	sta zp_ARG2
	lda #SCROLL_NOTENULL_COLOR
	sta zp_ARG3

  ldy #$00
@loop:
  rambank zp_SCROLL_PATTERN_NUMBER
  lda zp_SCROLL_PATTERN_NUMBER
	lda (zp_SCROLL_ROW_POINTER),y

  ; If less than NOTEREL value, transpose applies
  cmp #NOTEREL
  bge @continue
@transpose:
  pha
  rambank zp_BANK_MISC
  pla
  clc
  adc global_transpose 
  ; If transpose results in a note above NOTEREL, just set to max pitch note
@check_max_note:
  cmp #NOTEREL
  blt @continue
  lda #NOTEREL - 1
@continue:
	sta zp_NOTE_NUMERIC
  rambank zp_SCROLL_PATTERN_NUMBER
  ; instrument
  iny
  ;lda (zp_SCROLL_ROW_POINTER),y
  ;sta zp_ROW_INSTRUMENT
  ; volume
  iny
  lda (zp_SCROLL_ROW_POINTER),y
  sta TEMP_VOLUME
  ; effect param
  iny
  lda (zp_SCROLL_ROW_POINTER),y
  sta TEMP_EFFECT_PARAM
  ; effect value
  iny
  lda (zp_SCROLL_ROW_POINTER),y
  sta TEMP_EFFECT_VALUE
	; Next note
	iny
  phy

  jsr check_for_effects

@check_for_vol_effects:
  lda zp_NOTE_NUMERIC
  cmp #NOTENULL
  beq @check_for_volume
@print_note:
  jsr ui::print_note

@end:
  ply
	cpy #TOTAL_BYTES_PER_ROW	; 25 channels * 5
	blt @loop
  rts

; If no note was played, check if volume was used
@check_for_volume:
  lda TEMP_VOLUME
  cmp #VOLNULL
  beq @check_for_effect
  pha
  lda #SCROLL_VOLUME_COLOR
  sta zp_TEXT_COLOR
  pla
  jsr graphics::printing::print_hex
  ; Print Space
  lda #SPACE
  sta VERA_data0
  lda zp_TEXT_COLOR
  sta VERA_data0
  bra @end
  
; If volume was null, check fro effect
@check_for_effect:
  lda TEMP_EFFECT_PARAM
  cmp #EFFNULL
  beq @print_note
  pha
  lda #SCROLL_EFX_COLOR
  sta zp_TEXT_COLOR
  pla
  jsr graphics::printing::print_hex
  ; Print Space
  lda #SPACE
  sta VERA_data0
  lda zp_TEXT_COLOR
  sta VERA_data0
  bra @end

.endproc

; Check for certain pattern effects which will require us to change things
.proc check_for_effects
  TEMP_EFFECT_PARAM = zp_TMP1
  TEMP_EFFECT_VALUE = zp_TMP2
  lda TEMP_EFFECT_PARAM
  cmp #tracker::effects::PATTERN_BREAK
  beq @store_effect
  cmp #tracker::effects::JUMP_TO_ORDER
  beq @store_effect
  rts

@store_effect:
  sta zp_SCROLL_ROW_EFFECT_PARAM
  lda TEMP_EFFECT_VALUE
  sta zp_SCROLL_ROW_EFFECT_VALUE
  rts
.endproc
