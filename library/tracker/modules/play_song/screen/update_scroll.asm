.proc update_scroll
	sei
	lda #$01          ; Set primary address bank to 1, stride to 0
  sta VERA_addr_high 

	ldy zp_SCROLL_SCREEN_NUMBER
	lda #$00
	jsr graphics::drawing::goto_xy
	lda #PATTERN_ROW_COLOR
	sta zp_TEXT_COLOR
	lda zp_SCROLL_ROW_NUMBER
	jsr graphics::printing::print_hex

	lda zp_SCROLL_ROW_NUMBER
	jsr tracker::get_row
	lda zp_ARG0
	sta zp_SCROLL_ROW_POINTER
	lda zp_ARG0 + 1
	sta zp_SCROLL_ROW_POINTER + 1


	jsr print_row

	; Update debug info
	lda #$00          ; Set primary address bank to 1, stride to 0
  sta VERA_addr_high 


;	lda #$B1
;	sta zp_TEXT_COLOR

;	lda #$10
;	ldy #$3A
;	jsr graphics::drawing::goto_xy
;	lda zp_SCROLL_ORDER_NUMBER
;	jsr graphics::printing::print_hex

;	lda #$13
;	ldy #$3A
;	jsr graphics::drawing::goto_xy
;	lda zp_SCROLL_PATTERN_NUMBER
;	jsr graphics::printing::print_hex

;	lda #$16
;	ldy #$3A
;	jsr graphics::drawing::goto_xy
;	lda zp_SCROLL_ROW_NUMBER
;	jsr graphics::printing::print_hex

;	lda #$01
;	ldy #$25
;	jsr graphics::drawing::goto_xy
;	lda zp_ROW_NUMBER
;	jsr graphics::printing::print_hex

;	lda #$19
;	ldy #$3A
;	jsr graphics::drawing::goto_xy
;	lda zp_SCROLL_SCREEN_NUMBER
;	jsr graphics::printing::print_hex
	clc
	rts
.endproc