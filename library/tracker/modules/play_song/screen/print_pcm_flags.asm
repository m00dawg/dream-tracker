.proc print_pcm_flags
@print_pcm_flags:
  lda #PCM_FLAGS_X
  ldy #PCM_FLAGS_Y
  jsr graphics::drawing::goto_xy
  lda zp_PCM_FLAGS
  sta VALUE
  ldx #$00
@print_flags_loop:
  clc
  rol VALUE
  bcs @print_flags_active
@print_flags_inactive:
  lda #INACTIVE_FLAG_COLOR
  bra @print_flags_color
@print_flags_active:
  lda #ACTIVE_FLAG_COLOR
@print_flags_color:
  pha
  lda VERA_data0  ; skip character
  pla
  sta VERA_data0
@print_flags_loop_bottom:
  inx
  cpx #NUM_VERA_FLAGS
  bne @print_flags_loop
  rts
.endproc