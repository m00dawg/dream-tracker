  ; LUT index
  TITLE = $00
  COMPOSER = $01
  SYNC_METHOD = $02
  INITIAL_SPEED = $03
  INITIAL_BPM = $04
  FM_CH7_NOISE_EN = $05
  FM_CH7_NOISE_FQ = $06
  FM_LFO_FREQUENCY = $07
  FM_LFO_AM_DEPTH = $08
  FM_LFO_PM_DEPTH = $09
  FM_LFO_WAVEFORM = $0A
  GLOBAL_TRANSPOSE = $0B
  DESCRIPTION = $0C

  ; Min/Max
  DESCRIPTION_MAX_LINE = $0F
  DESCRIPTION_MAX_LINE_LENGTH = $40

  ; Positions
  CURSOR_START_X = $18
  CURSOR_START_Y = $0A

  DESCRIPTION_START_X = $05
  DESCRIPTION_STOP_X = $44
  DESCRIPTION_START_Y = $25

  SONG_TITLE_INPUT_X = $18
  SONG_TITLE_INPUT_Y = $0A
  COMPOSER_INPUT_X = $18
  COMPOSER_INPUT_Y = $0B

  SYNC_METHOD_X = $18
  SYNC_METHOD_Y = $0E
  SPEED_INPUT_X = $18
  SPEED_INPUT_Y = $0F
  BPM_INPUT_X = $18
  BPM_INPUT_Y = $10

  NOISE_ENABLE_X = $18
  NOISE_ENABLE_Y = $13
  NOISE_FREQ_X = $18
  NOISE_FREQ_Y = $14

  LFO_FREQ_X = $18
  LFO_FREQ_Y = $15
  LFO_AM_DEPTH_X = $18
  LFO_AM_DEPTH_Y = $16
  LFO_PM_DEPTH_X = $18
  LFO_PM_DEPTH_Y = $17
  LFO_WAVEFORM_X = $18
  LFO_WAVEFORM_Y = $18

  GLOBAL_TRANSPOSE_X = $18
  GLOBAL_TRANSPOSE_Y = $1B


x_pos_lut:
  ; Title
  .byte $18
  ; Composer
  .byte $18
  ; Sync Method
  .byte $18
  ; Initial Speed
  .byte $18
  ; Initial BPM
  .byte $18
  ; Ch7 Noise Enable
  .byte $18
  ; Ch7 Freq
  .byte $18
  ; LFO Frequency
  .byte $18
  ; LFO AM Depth
  .byte $18
  ; LFO PM Depth
  .byte $18
  ; LFO Waveform
  .byte $18
  ; Transpose
  .byte $18
  ; Description
  .byte $05

x_stop_pos_lut:
  ; Title
  .byte $37
  ; Composer
  .byte $37
  ; Sync Method
  .byte $18
  ; Initial Speed
  .byte $19
  ; Initial BPM
  .byte $19
  ; Ch7 Noise Enable
  .byte $18
  ; Ch7 Freq
  .byte $19
  ; LFO Frequency
  .byte $19
  ; LFO AM Depth
  .byte $19
  ; LFO PM Depth
  .byte $19
  ; LFO Waveform
  .byte $18
  ; Transpose
  .byte $19
  ; Description
  .byte $44

y_pos_lut:
  ; Title
  .byte $0A
  ; Composer
  .byte $0B
  ; Sync Method
  .byte $0E
  ; Initial Speed
  .byte $0F
  ; Initial BPM
  .byte $10
  ; Ch7 Noise Enable
  .byte $13
  ; Ch7 Freq
  .byte $14
  ; LFO Freq
  .byte $15
  ; LFO AM Depth
  .byte $16
  ; LFO PM Depth
  .byte $17
  ; LFO Waveform
  .byte $18
  ; Transpose
  .byte $1B
  ; Description
  .byte $25

