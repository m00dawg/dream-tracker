.proc update_info

start:
  print_string_with_length_macro song_title, #SONG_TITLE_MAX_LENGTH, #SONG_TITLE_INPUT_X, #SONG_TITLE_INPUT_Y, #EDITABLE_TEXT_COLORS
  print_string_with_length_macro composer, #COMPOSER_MAX_LENGTH, #COMPOSER_INPUT_X, #COMPOSER_INPUT_Y, #EDITABLE_TEXT_COLORS

  push_rambank
  rambank zp_BANK_MISC

  lda #SYNC_METHOD_X
  ldy #SYNC_METHOD_Y
  jsr graphics::drawing::goto_xy
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  lda sync_method
  jsr graphics::printing::print_single_hex

  lda #SPEED_INPUT_X
  ldy #SPEED_INPUT_Y
  jsr graphics::drawing::goto_xy
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  lda starting_speed
  jsr graphics::printing::print_hex

  lda #BPM_INPUT_X
  ldy #BPM_INPUT_Y
  jsr graphics::drawing::goto_xy
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  lda starting_bpm
  jsr graphics::printing::print_hex

  lda #NOISE_ENABLE_COLOR
  sta enable_color
  lda #NOISE_DISABLE_COLOR
  sta disable_color
  lda fm_global_ch7_noise_control
  and #%10000000
  sta flag
  lda #NOISE_ENABLE_X
	ldy #NOISE_ENABLE_Y
	jsr ui::instruments::print_flag

  lda #NOISE_FREQ_X
  ldy #NOISE_FREQ_Y
  jsr graphics::drawing::goto_xy
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  lda fm_global_ch7_noise_control
  and #%00011111
  jsr graphics::printing::print_hex

  lda #LFO_FREQ_X
  ldy #LFO_FREQ_Y
  jsr graphics::drawing::goto_xy
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  lda fm_global_lfo_frequency
  jsr graphics::printing::print_hex

  lda #LFO_AM_DEPTH_X
  ldy #LFO_AM_DEPTH_Y
  jsr graphics::drawing::goto_xy
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  lda fm_global_lfo_am_depth
  jsr graphics::printing::print_hex

  lda #LFO_PM_DEPTH_X
  ldy #LFO_PM_DEPTH_Y
  jsr graphics::drawing::goto_xy
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  lda fm_global_lfo_pm_depth
  jsr graphics::printing::print_hex

  lda #LFO_WAVEFORM_X
  ldy #LFO_WAVEFORM_Y
  jsr graphics::drawing::goto_xy
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  lda fm_global_lfo_waveform
  jsr graphics::printing::print_single_hex

  lda #GLOBAL_TRANSPOSE_X
  ldy #GLOBAL_TRANSPOSE_Y
  jsr graphics::drawing::goto_xy
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  lda global_transpose
  jsr graphics::printing::print_hex

@description:
  lda #<song_description
  sta description_address
  lda #>song_description
  sta description_address + 1

  ldx #DESCRIPTION_START_X
  ldy #DESCRIPTION_START_Y
  stz temp_line_pos
@line_loop:
  stz temp_char_pos
@character_loop:
  txa
  jsr graphics::drawing::goto_xy
  phy
  ldy #$00
  lda (description_address),y
  ply
  sta VERA_data0
  lda VERA_data0  ; color
@character_loop_end:
  add16to8 description_address, #$01, temp_address
  lda temp_address
  sta description_address
  lda temp_address + 1
  sta description_address + 1
  inx
  inc temp_char_pos
  lda temp_char_pos
  cmp #DESCRIPTION_MAX_LINE_LENGTH
  bne @character_loop
@line_loop_end:
  iny
  ldx #DESCRIPTION_START_X
  inc temp_line_pos
  lda temp_line_pos
  cmp #DESCRIPTION_MAX_LINE
  bne @line_loop

end:
  pop_rambank
  rts

.endproc

