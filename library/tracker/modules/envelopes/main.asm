.segment "ENV_MODULE"

; Envelopes Module

; Envelope Addressing
; Since envelopes are currently 256 in length
; this means the low address is just the position of the envelope.
; The high position is the envelope number plus the base address ($A0)

.scope envelopes

.word init
.word keyboard_loop

.include "library/tracker/modules/envelopes/cursor.asm"
.include "library/tracker/modules/envelopes/load.asm"

ENVELOPES_SHORT_FILENAME:
	.byte "scr/envs.scr"
ENVELOPES_SHORT_FILENAME_LENGTH = $0C

ENVELOPES_FULL_FILENAME:
	.byte "scr/envf.scr"
ENVELOPES_FULL_FILENAME_LENGTH = $0C

; Constants
ENVELOPE_BASE_ADDRESS_HIGH = $A0
MAX_ENVELOPES = $1F
MAX_SPLIT_ENVELOPE_VALUE = $3F
MAX_FULL_POSITIVE_ENVELOPE_VALUE = $7F

MAX_POSITION = $FF
; How many values to inc/dec by chunk
; (e.g. pgup/pgdn
CURSOR_CHUNK = $10

; Views
VIEW_SPLIT = $00
VIEW_PITCH = $01

; Colors
BIT_ACTIVE_COLOR = $0D
BIT_INACTIVE_COLOR = $06
ENVELOPE_NUMBER_ACTIVE_COLOR = $B1
ENVELOPE_NUMBER_INACTIVE_COLOR = $BF
PARAMETERS_COLOR = $BF

; X/Y Screen positions
ENV_NUMBER_X = $03
ENV_NUMBER_START_Y = $0D
ENV_NUMBER_END_Y = $2C
ENV_POSITION_X = $1C
ENV_POSITION_Y = $31
ENV_8BIT_VALUE_X = $1C
ENV_8BIT_VALUE_Y = $32
ENV_6BIT_VALUE_X = $1C
ENV_6BIT_VALUE_Y = $33

ENV_MARKER_START_X = $0D
ENV_MARKER_SHORT_START_Y = $19
ENV_MARKER_FULL_START_Y = $2E


; Envelope Graph Positions
BIT7_Y_POS = $00
BIT6_Y_POS = $01
GRAPH_WIDTH = $40
SHORT_SPLIT_BLOCKS_NUM = $08
; Same for both negative and positive (since full is bipolar/signed)
FULL_SPLIT_BLOCKS_NUM = $10
; Full graph size
FULL_Y_HEIGHT = $20

; Range of values in a character block
BLOCKS_VALUE_RANGE = $07

; Remember this is "0" since the graph moves up
; on positive numbers, but the screen moves down
; on positive numbers
SPLIT_ENV_START_X = $10

; Move screen left 13 characters (8*13)
; And down 13 characters (8*13)
; Numbers are negative since down/right in VERA 
; are negative movements
; We draw the entire env to VRAM so we can scroll
; Same for both short and full envs since we're scrolling the _top_ 
; of the screen downwards
ENV_SCROLL_X_HIGH = $FF
ENV_SCROLL_X_LOW = $98
ENV_SCROLL_Y_HIGH = $FF
ENV_SCROLL_Y_LOW = $98


; Scope Variables
envelope_address = zp_ADDR0
envelope_number = zp_TMP0

mouse_x = zp_ADDR1
mouse_y = zp_ADDR2

mode = zp_TMP1
env_mode = zp_TMP2      ; 0 = short, 1 = full
position = zp_TMP3
color = zp_TMP4
value = zp_TMP5
graph_start = zp_TMP6 
graph_stop = zp_TMP7 
env_marker_start_y = zp_TMP8
max_positive_env_value = zp_TMP9
graph_position = zp_TMPA

loop_index = zp_ARG0
block_number = zp_ARG1

; Main Memory
; Scope Data
; Characters for values 0-7 for graphing envelope
; #SCREENCODE_FULL_BLOCK is then the next 0 for the next
; line up
; See library/screencodes.inc for values
vu_meters:
  .byte SCREENCODE_VU0
  .byte SCREENCODE_VU1
  .byte SCREENCODE_VU2
  .byte SCREENCODE_VU3
  .byte SCREENCODE_VU4
  .byte SCREENCODE_VU5
  .byte SCREENCODE_VU6
  .byte SCREENCODE_VU7  

negative_vu_meters:
  .byte SCREENCODE_INV_VU0
  .byte SCREENCODE_INV_VU1
  .byte SCREENCODE_INV_VU2
  .byte SCREENCODE_INV_VU3
  .byte SCREENCODE_INV_VU4
  .byte SCREENCODE_INV_VU5
  .byte SCREENCODE_INV_VU6
  .byte SCREENCODE_INV_VU7  

; For range offset of each block
positive_block_start_values:
  .byte $00, $08, $10, $18, $20, $28, $30, $38
  .byte $40, $48, $50, $58, $60, $68, $70, $78

negative_block_start_values:
  .byte $FF, $F7, $EF, $E7, $DF, $D7, $CF, $C7
  .byte $BF, $B7, $AF, $A7, $9F, $97, $8F, $87
  ;.byte $F8, $F0, $E8, $E0, $D8, $D0, $C8, $C0
  ;.byte $B8, $B0, $A8, $A0, $98, $90, $88, $80


; Easy way to reverse the direction since
; the screen moves down on positive numbers
; but blocks move up.
short_block_positions:
  .byte $0A,$09,$08,$07,$06,$05,$04,$03
positive_full_block_positions:
  .byte $0F,$0E,$0D,$0C,$0B,$0A,$09,$08
  .byte $07,$06,$05,$04,$03,$02,$01,$00
negative_full_block_positions:
  .byte $10,$11,$12,$13,$14,$15,$16,$17
  .byte $18,$19,$1A,$1B,$1C,$1D,$1E,$1F



; For having a gradient (likely for 256 color mode)
short_block_colors:
  .byte $05,$05,$0D,$0D,$07,$07,$08,$0A
  ;.byte $A9,$AA,$87,$88,$56,$57,$3A,$3B
  ;.byte $A0,$A1,$A2,$A3,$A4,$73,$57,$3B
  ;.byte $82,$83,$84,$85,$86,$87,$88,$72
  ;.byte $68,$69,$6A,$6B,$56,$57,$3B,$34
  
full_block_colors:
  .byte $05,$05,$05,$05,$0D,$0D,$0D,$0D
  .byte $07,$07,$07,$07,$08,$08,$0A,$0A

.proc init
init:
  ;vera_layer0_256_color
  turn_on_layer_0
  vera_layer0_16_color

  ;jsr ui::update_upper_info
  ;jsr ui::print_song_info
  lda zp_BANK_ENVS
  sta RAM_BANK

  ; Set scroll for the env layer (same for both short and full env views)
  lda #ENV_SCROLL_X_HIGH
  sta VERA_L0_hscroll_h
  lda #ENV_SCROLL_X_LOW
  sta VERA_L0_hscroll_l
  lda #ENV_SCROLL_Y_HIGH
  sta VERA_L0_vscroll_h
  lda #ENV_SCROLL_Y_LOW
  sta VERA_L0_vscroll_l

  ; Turn on the mouse
  ; mouse_on

  stz zp_KEY_PRESSED
  stz envelope_number
  stz mode
  stz graph_start
  lda #GRAPH_WIDTH
  sta graph_stop
  lda #$01
  sta env_mode
  jmp switch_env_mode
.endproc

.proc keyboard_loop
keyboard_loop:
  lda zp_KEY_PRESSED
  cmp #KEY_UP
  beq @cursor_up_jump
  cmp #KEY_DOWN
  beq @cursor_down_jump
  cmp #KEY_LEFT
  beq @cursor_left_jump
  cmp #KEY_RIGHT
  beq @cursor_right_jump
  cmp #KEY_HOME
  beq @cursor_max_value_jump
  cmp #KEY_END
  beq @cursor_min_value_jump
  cmp #KEY_PGUP
  beq @cursor_up_by_chunk_jump
  cmp #KEY_PGDN
  beq @cursor_down_by_chunk_jump
  cmp #KEY_TAB
  beq @switch_mode
  cmp #ALT_M
  beq @switch_env_mode_jump
  jmp main_application_loop
  ;stz envelope_number

; Cusor Up
@cursor_up_jump:
  jsr cursor::cursor_up
  jmp main_application_loop

; Cursor Down
@cursor_down_jump:
  jsr cursor::cursor_down
  jmp main_application_loop

; Cursor Left
@cursor_left_jump:
  jsr cursor::cursor_left
  jmp main_application_loop

; Cursor Right
@cursor_right_jump:
  jsr cursor::cursor_right
  jmp main_application_loop

@cursor_max_value_jump:
  jsr cursor::cursor_max
  jmp main_application_loop

@cursor_min_value_jump:
  jsr cursor::cursor_min
  jmp main_application_loop

@cursor_up_by_chunk_jump:
  jsr cursor::cursor_up_by_chunk
  jmp main_application_loop

@cursor_down_by_chunk_jump:
  jsr cursor::cursor_down_by_chunk
  jmp main_application_loop


; Update the UI and things for when the mode
; changes, such as moving the cursor to the right place, etc.
@switch_mode:
  lda mode
  beq @switch_mode_edit
@switch_mode_number:
  jsr cursor::remove_arrow
  stz mode
  jmp main_application_loop
@switch_mode_edit:
  jsr cursor::add_arrow
  inc mode
  jmp main_application_loop

; Switch to/from full/short env view
@switch_env_mode_jump:
  jmp switch_env_mode

.endproc


; Highlights the selected envelope
.proc highlight_envelope_number
  ldy #ENV_NUMBER_START_Y  
  ldx #$00
@highlight_loop:
  cpx envelope_number
  beq @active
@inactive:
  lda #ENVELOPE_NUMBER_INACTIVE_COLOR
  sta color
  jmp @highlight_loop_bottom
@active:
  lda #ENVELOPE_NUMBER_ACTIVE_COLOR
  sta color 
@highlight_loop_bottom:
  lda #ENV_NUMBER_X
  jsr graphics::drawing::goto_xy
  ; First Value
  ; Character
  lda VERA_data0
  ; Color
  lda color
  sta VERA_data0
  ; Second Value
  ; Character
  lda VERA_data0
  ; Color
  lda color
  sta VERA_data0
  inx
  iny 
  cpy #ENV_NUMBER_END_Y + 1
  bne @highlight_loop
  rts
.endproc

; Increments the selected env number (and loops on max)
.proc increment_envelope
  ; Switch to upper layer
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high

  ldx envelope_number
  inx
  cpx #MAX_ENVELOPES + 1
  bne @end
  ldx #$00
@end:
  stx envelope_number
  jsr highlight_envelope_number
  jsr load_envelope
  rts
.endproc

; Decrements the selected env number (and loops on max)
.proc decrement_envelope
  ; Switch to upper layer
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high

  ldx envelope_number
  dex
  cpx #$FF
  bne @end
  ldx #MAX_ENVELOPES
@end:
  stx envelope_number
  jsr highlight_envelope_number
  jsr load_envelope
  rts
.endproc

.proc update_envelope_info
@start:
  ; Print position number
  ; Switch to upper layer
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high  

  lda #ENV_POSITION_X
  ldy #ENV_POSITION_Y
  jsr graphics::drawing::goto_xy
  lda #PARAMETERS_COLOR
  sta zp_TEXT_COLOR
  lda position
  jsr graphics::printing::print_hex

  lda #ENV_8BIT_VALUE_X
  ldy #ENV_8BIT_VALUE_Y
  jsr graphics::drawing::goto_xy
  lda #PARAMETERS_COLOR
  sta zp_TEXT_COLOR
  ldy position
  lda (envelope_address),y
  jsr graphics::printing::print_hex

  lda #ENV_6BIT_VALUE_X
  ldy #ENV_6BIT_VALUE_Y
  jsr graphics::drawing::goto_xy
  lda #PARAMETERS_COLOR
  sta zp_TEXT_COLOR
  ldy position
  lda (envelope_address),y
  and #%00111111
  jsr graphics::printing::print_hex

  rts
.endproc


.proc evaluate_mouse
  ldx #mouse_x
  jsr MOUSE_GET
  lda #ENV_POSITION_X
  ldy #ENV_POSITION_Y - 2
  jsr graphics::drawing::goto_xy
  lda mouse_x + 1 
  jsr graphics::printing::print_hex
  lda mouse_x 
  jsr graphics::printing::print_hex

  lda #ENV_POSITION_X
  ldy #ENV_POSITION_Y - 1
  jsr graphics::drawing::goto_xy
  lda mouse_y + 1 
  jsr graphics::printing::print_hex
  lda mouse_y 
  jsr graphics::printing::print_hex
.endproc


.proc switch_env_mode
@start:
  lda env_mode
  beq @switch_mode_full
@switch_mode_short:
  stz env_mode
  lda #ENVELOPES_SHORT_FILENAME_LENGTH
	ldx #<ENVELOPES_SHORT_FILENAME
	ldy #>ENVELOPES_SHORT_FILENAME
	jsr files::load_to_vram

  lda #MAX_SPLIT_ENVELOPE_VALUE
  sta max_positive_env_value

  lda #ENV_MARKER_SHORT_START_Y
  jmp @end
@switch_mode_full:
  inc env_mode
  lda #ENVELOPES_FULL_FILENAME_LENGTH
	ldx #<ENVELOPES_FULL_FILENAME
	ldy #>ENVELOPES_FULL_FILENAME
	jsr files::load_to_vram

  lda #MAX_FULL_POSITIVE_ENVELOPE_VALUE
  sta max_positive_env_value
  
  lda #ENV_MARKER_FULL_START_Y
@end:
  sta env_marker_start_y
  jsr load_envelope
  jsr update_envelope_info
  jsr highlight_envelope_number
  jmp main_application_loop 

.endproc

.endscope
