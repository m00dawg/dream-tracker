; Procs to load/draw envelopes

; Load the selected envelope from memory
.proc load_envelope
  stz position
  ; Get the full address of the envelope
  ; (A0 + env_num 00)
  ; Remember 6502 is little endian so the +1 
  ; is the env number
  stz envelope_address
  lda #ENVELOPE_BASE_ADDRESS_HIGH
  sta envelope_address + 1
  add envelope_address + 1, envelope_number
  sta envelope_address + 1
  
  ; Switch to lower layer
  jsr clear_blocks
  jsr draw_envelope
  rts
.endproc

; Draw the envelope onto lower layer
.proc draw_envelope
  lda #%00010001	; Inc set to 1, hi ram
  sta VERA_addr_high
  lda env_mode
  bne @draw_full
@draw_short:
  ; Draw hi bit
  ldy #BIT7_Y_POS
  jsr draw_bits
  ldy #BIT6_Y_POS
  jsr draw_bits

  ; Blocks loop
  stz block_number
@block_short_loop:
  lda graph_start
  sta graph_position
@short_graph_loop:
  jsr draw_blocks
  jsr draw_negative_blocks
  inc graph_position
  lda graph_position
  cmp graph_stop
  bne @short_graph_loop
@block_short_loop_bottom:
  inc block_number
  lda block_number
  cmp #SHORT_SPLIT_BLOCKS_NUM
  bne @block_short_loop
  rts

@draw_full:
  ; Blocks loop
  stz block_number
@block_full_loop:
  lda graph_start
  sta graph_position
@full_graph_loop:
  jsr draw_blocks
  jsr draw_negative_blocks
  inc graph_position
  lda graph_position
  cmp graph_stop
  bne @full_graph_loop
@block_full_loop_bottom:
  inc block_number
  lda block_number
  cmp #FULL_SPLIT_BLOCKS_NUM
  bne @block_full_loop
  rts
.endproc

; y = bit position
; we use this to figure out which 
; row to start on as well as which
; bit to look at in value
.proc draw_bits
  phy
  plx
  lda #$00  
  jsr graphics::drawing::goto_xy
  ldy graph_start

@loop:
  lda (envelope_address),y
  sta value
  ; Check our pushed value (the row position)
  ; if 0, bbr 7,, if one bbr6
  cpx #$00
  bne @bbr6
  bbr 7, value,@bit_off
  jmp @bit_on
@bbr6:
  bbr 6,value,@bit_off
@bit_on:
  lda #SCREENCODE_PLUS
  sta VERA_data0
  lda #BIT_ACTIVE_COLOR
  sta VERA_data0
  jmp @loop_bottom
@bit_off:
  lda #SCREENCODE_MINUS
  sta VERA_data0
  lda #BIT_INACTIVE_COLOR
  sta VERA_data0
@loop_bottom:
  iny
  cpy graph_stop
  bne @loop
@end:
  rts
.endproc

; Draw a line of blockos
.proc draw_blocks
  ldx block_number
  lda env_mode
  bne @full_block_positions
  lda short_block_positions,x
  jmp @continue
@full_block_positions:
  lda positive_full_block_positions,x
@continue:
  tay
  lda graph_position
  jsr graphics::drawing::goto_xy

  ldy graph_position
  lda (envelope_address),y
  ; Check for full env view, and if so, do not draw the top
  ; half of the graph if the value is > $7F (if it is negative)
  ldy env_mode
  beq @short_mode
  ; If >=$80 it means number is negative, so don't draw the top
  cmp #MAX_FULL_POSITIVE_ENVELOPE_VALUE + 1
  bge @end
@short_mode:
  sta value
  ; If value is > block stop value, draw full brick and continue
  lda positive_block_start_values,x
  adda #BLOCKS_VALUE_RANGE
  cmp value
  bge @check_min_value
  lda #SCREENCODE_VU_FULL
  jmp @output_character
@check_min_value:
  ; If we're here value was <= block stop value
  ; So if value is >= block start value 
  ; we're in the right range
  ldx block_number
  lda positive_block_start_values,x
  sta zp_MATH0
  lda value
  cmp zp_MATH0
  ; branch if value < block start
  blt @output_empty
  ; Subtract value from the block start value to get the 0-7 range we want
  suba zp_MATH0
  tax
  lda vu_meters,x
  jmp @output_character
@output_empty:
  lda #SCREENCODE_VU0
@output_character:
  sta VERA_data0
  ldx block_number
  lda env_mode
  bne @full_block_colors
@short_block_colors:
  lda short_block_colors,x
  jmp @continue_color
@full_block_colors:
  lda full_block_colors,x
@continue_color:
  sta VERA_data0
@end:
  rts
.endproc

; For the full env, draw negative blocks, which we do in reverse of the positives
; (going down not up)
.proc draw_negative_blocks
  ldx block_number
  lda negative_full_block_positions,x
  tay
  ;lda #$00
  lda graph_position
  ;ldy block_number
  jsr graphics::drawing::goto_xy
  ldy graph_position
  lda (envelope_address),y
  ; If <$80 it means number is positive, so don't draw the top
  cmp #MAX_FULL_POSITIVE_ENVELOPE_VALUE + 1
  blt @end
  sta value
  ; If value is < block stop value, draw full brick and continue
  lda negative_block_start_values,x
  suba #BLOCKS_VALUE_RANGE
  cmp value
  blt @check_min_value
  lda #SCREENCODE_VU_FULL
  jmp @output_character
@check_min_value:
  ; If we're here value was <= block stop value
  ; So if value is >= block start value 
  ; we're in the right range
  ldx block_number
  lda negative_block_start_values,x
  sta zp_MATH0
  lda value
  cmp zp_MATH0
  ; branch if value < block start
  ;bgt @continue, @output_empty
  bgt @output_empty
  
@continue:
  ; Subtract value from the block start value to get the 0-7 range we want
  dec value
  lda zp_MATH0
  suba value
  tax
  lda negative_vu_meters,x
  jmp @output_character
@output_empty:
  lda #SCREENCODE_VU0
@output_character:
  sta VERA_data0
  ldx block_number
  lda full_block_colors,x
  sta VERA_data0
@end:
  rts
.endproc

; Clear out envelope area
.proc clear_blocks
  lda #%00010001	; Inc set to 1, hi ram
  sta VERA_addr_high
  ldy #$00
@loop_y:
  ldx graph_start
@loop_x:
  txa
  jsr graphics::drawing::goto_xy
  lda #SCREENCODE_VU0
  sta VERA_data0
  stz VERA_data0
@loop_x_bottom:
  inx
  cpx graph_stop
  bne @loop_x
@loop_y_bottom:
  iny
  cpy #FULL_Y_HEIGHT
  bne @loop_y

@end:
  rts
.endproc
