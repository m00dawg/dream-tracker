.segment "FINST_MODULE"

.scope fm_instruments

.word init
.word keyboard_loop

FM_INSTRUMENTS_SCREEN_FILENAME:
	.byte "scr/finst.scr"
FM_INSTRUMENTS_SCREEN_FILENAME_LENGTH = $0D

; Positions
MAX_CURSOR_POSITION = $36
ALGO_X = $2A
ALGO_Y = $0D
LEFT_X = $29
LEFT_Y = $0E
RIGHT_X = $2A
RIGHT_Y = $0E
SENSITIVITY_PHASE_X = $39
SENSITIVITY_PHASE_Y = $0E
SENSITIVITY_AMP_X = $4B
SENSITIVITY_AMP_Y = $0E
GLOBAL_VOL_X = $29
GLOBAL_VOL_Y = $10
OP_GLOBAL_VOL_X = $3D
OP_GLOBAL_VOL_Y = $10
M1_FEEDBACK_X = $4B
M1_FEEDBACK_Y = $13

OP_GLOBAL_VOLUME_M1_X = $3D
OP_GLOBAL_VOLUME_M1_Y = $10
OP_GLOBAL_VOLUME_C1_X = $3F
OP_GLOBAL_VOLUME_C1_Y = $10
OP_GLOBAL_VOLUME_M2_X = $41
OP_GLOBAL_VOLUME_M2_Y = $10
OP_GLOBAL_VOLUME_C2_X = $43
OP_GLOBAL_VOLUME_C2_Y = $10

; X positions of operators
; Since all the ops have the same text setup,
; we can just start at a Y position and increment when needed
OP_VOLUME_X = $29
OP_AM_MOD_EN_X = $2A
OP_KEY_SCALE_X = $39
OP_FREQ_MULT_X = $4B 
OP_DETINE_FINE_X = $2A
OP_DETUNE_COARSE_X = $39
OP_ATTACK_X = $29
OP_RELEASE_X = $39 
OP_DECAY_1_RATE_X = $29
OP_DECAY_1_LEVEL_X = $39
OP_DECAY_2_RATE_X = $4A

; Y positions of operators
M1_VOLUME_Y = $13
C1_VOLUME_Y = $1A
M2_VOLUME_Y = $21
C2_VOLUME_Y = $28

; Colors
FM_TEXT_COLOR = $01
FM_ACTIVE_COLOR = $01
FM_INACTIVE_COLOR = $0D
PARAMETER_COLOR = $CD
LEFT_PAN_ACTIVE_COLOR   = $CD
RIGHT_PAN_ACTIVE_COLOR = $CA
PAN_INACTIVE_COLOR = $CB
PARAM_INACTIVE_COLOR = $CB
PARAM_ACTIVE_COLOR = $CD

; Addresses
op_register_address = zp_ADDR0

; Temp Variables
edit_mode = zp_TMP0
instrument_edit_pos = zp_TMP1
operator_offset = zp_TMP2
temp_register0 = zp_TMP3
save_pos       = zp_TMP4
edit_position_temp = zp_TMP5

; Calling Args for ui::instruments::list_names
instrument_base_address = zp_ARG0
text_color              = zp_ARG1
number_color            = zp_ARG2
num_instruments         = zp_ARG3
edit_start_x            = zp_ARG4
edit_start_y            = zp_ARG5

; Calling Args for ui::instruments::set_pan
left_x                  = zp_ARG6
left_y                  = zp_ARG7
pan                     = zp_ARG8

; Calling args for ui::instruments::print_flag
enable_color            = zp_ARG6
disable_color           = zp_ARG7
flag                    = zp_ARG8

; Calling args for sound::fm::update_channel_instrument
channel = zp_ARG0
instrument = zp_ARG1

; Calling args for randomize instrument
random_values = zp_MATH0

; Includes
.include "library/tracker/modules/fm_instruments/luts.inc"
.include "library/tracker/modules/fm_instruments/load_instrument.asm"
.include "library/tracker/modules/fm_instruments/cursor.asm"
.include "library/tracker/modules/fm_instruments/edit_instrument.asm"
.include "library/tracker/modules/fm_instruments/save_instrument.asm"
.include "library/tracker/modules/fm_instruments/randomize_instrument.asm"

.proc init
	lda #FM_INSTRUMENTS_SCREEN_FILENAME_LENGTH
	ldx #<FM_INSTRUMENTS_SCREEN_FILENAME
	ldy #>FM_INSTRUMENTS_SCREEN_FILENAME
	jsr files::load_to_vram

  rambank zp_BANK_FINS

  lda #%00010001	; Inc set to 1, hi ram
  sta VERA_addr_high
  turn_on_layer_0
  vera_layer0_16_color

  lda #<sound::fm::BASE_NAMES_ADDRESS
  sta instrument_base_address
  lda #>sound::fm::BASE_NAMES_ADDRESS
  sta instrument_base_address + 1
  lda #FM_TEXT_COLOR
  sta text_color
  lda #FM_INACTIVE_COLOR
  sta number_color
  lda #sound::fm::NUM_INSTRUMENTS
  sta num_instruments
  lda #ALGO_X
  sta edit_start_x
  lda #ALGO_Y
  sta edit_start_y
  jsr ui::instruments::list_names

  stz zp_INSTRUMENT
  stz instrument_edit_pos
  jsr load_instrument

  jsr ui::instruments::cursor::reset_on_names

  stz zp_KEY_PRESSED

.endproc

.proc keyboard_loop
  lda zp_KEY_PRESSED
  cmp #KEY_UP
  beq @cursor_up_jump
  cmp #KEY_DOWN
  beq @cursor_down_jump
  cmp #KEY_LEFT
  beq @cursor_left_jump
  cmp #KEY_RIGHT
  beq @cursor_right_jump
  cmp #KEY_TAB
  beq @change_edit_mode
  cmp #ALT_R
  beq @randomize_instrument_jump
  cmp #SPACE
  beq @spacebar_jump
  ; Start at period
  cmp #PETSCII_PERIOD
  bpl @eval_characters_jump
  jmp main_application_loop

; Cusor Up
@cursor_up_jump:
  jsr cursor::cursor_up
  jmp main_application_loop

; Cursor Down
@cursor_down_jump:
  jsr cursor::cursor_down
  jmp main_application_loop

; Cursor Left
@cursor_left_jump:
  jsr cursor::cursor_left
  jmp main_application_loop

; Cursor Right
@cursor_right_jump:
  jsr cursor::cursor_right
  jmp main_application_loop

@randomize_instrument_jump:
  jsr randomize_instrument
  jmp main_application_loop

@spacebar_jump:
  jmp spacebar
  ;jmp main_application_loop

@eval_characters_jump:
  jmp eval_characters

@change_edit_mode:
  lda edit_mode
  bne @instrument_names
@instrument_settings:
  inc edit_mode
  jsr ui::instruments::cursor::reset_on_edit
  stz instrument_edit_pos
  jmp @change_edit_mode_end
@instrument_names:
  stz edit_mode
  stz instrument_edit_pos
  jsr ui::instruments::cursor::reset_on_names
@change_edit_mode_end:
  jmp main_application_loop


.endproc


; If we pressed space in instrument name mode, add a space
; otherwise the behavior is dependent on where we are 
; in the instrument edit screen
.proc spacebar
  tax
  lda edit_mode
  bne @instrument_edit
  jmp @spacebar_print_character
@instrument_edit:
  jsr toggle_instrument_flag
  jmp @spacebar_end
@spacebar_print_character:
  txa  
  jsr print_character
@spacebar_end:
  jmp main_application_loop
.endproc

; For now, go wheever the F you want on the screen
.proc print_character
  pha
  lda edit_mode
  beq @names
@edit_instrument:
  pla
  pha
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  ldx text_color
  stx r0
  pla
  jsr graphics::printing::print_alpha_char
  jsr save_instrument
  jsr ui::cursor_right

  ;jsr ui::instruments::print_character
  
  bra @end
@names:
  pla
  jsr ui::instruments::print_character
  lda #<sound::fm::BASE_NAMES_ADDRESS
  sta instrument_base_address
  lda #>sound::fm::BASE_NAMES_ADDRESS
  sta instrument_base_address + 1
  jsr ui::instruments::save_names
@end:
  rts
.endproc

; User typed something, depending on the edit mode
; we'll do thangs.
.proc eval_characters
  tax
  lda edit_mode
  bne @instrument_edit
  txa
  jsr print_character
  jmp main_application_loop
@instrument_edit:
  txa
  cmp #KEY_Q
  beq @play_instrument
  cmp #KEY_W
  beq @stop_instrument
  jsr print_character
  jmp main_application_loop
@play_instrument:
  jsr sound::fm::preview_instrument
  jmp main_application_loop
@stop_instrument:
  jsr sound::fm::init
  jmp main_application_loop
.endproc


.endscope