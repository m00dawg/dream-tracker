.proc instrument_edit_cursor_down
    ldx instrument_edit_pos
    lda instrument_edit_cursor_down_lut,x
    adda instrument_edit_pos
    sta instrument_edit_pos
    jsr jump_to_instrument_edit_position
    rts
.endproc

.proc instrument_edit_cursor_up
    ldx instrument_edit_pos
    lda instrument_edit_cursor_up_lut,x
    sta edit_position_temp
    lda instrument_edit_pos
    suba edit_position_temp
    sta instrument_edit_pos
    jsr jump_to_instrument_edit_position
    rts
.endproc

.proc instrument_edit_cursor_right
  lda instrument_edit_pos
  cmp #MAX_CURSOR_POSITION
  bge @end
  inc instrument_edit_pos
  jsr jump_to_instrument_edit_position
@end:
  rts
.endproc

.proc instrument_edit_cursor_left
  lda instrument_edit_pos
  beq @end
  dec instrument_edit_pos
  jsr jump_to_instrument_edit_position
@end:
  rts
.endproc

.proc jump_to_instrument_edit_position
    jsr graphics::drawing::cursor_unplot
    ldx instrument_edit_pos
    lda instrument_edit_x_pos_lut,x
    ldy instrument_edit_y_pos_lut,x
    sta cursor_x
    sty cursor_y
    jsr graphics::drawing::goto_xy
    jsr graphics::drawing::cursor_plot
    rts
.endproc

.proc toggle_instrument_flag
; Toggle flags based on edit position  
  ldx zp_INSTRUMENT
  lda instrument_edit_pos
; Toggle Left Channel
@left_channel:
  cmp #INSTRUMENT_EDIT_LEFT
  bne @right_channel
  lda fm_instruments_pan_feedback_algo,x
  jsr ui::instruments::toggle_second_flag
  sta fm_instruments_pan_feedback_algo,x
  jmp @end

; Toggle Right Channel
@right_channel:
  cmp #INSTRUMENT_EDIT_RIGHT
  bne @global_vol_m1
  lda fm_instruments_pan_feedback_algo,x
  jsr ui::instruments::toggle_first_flag
  sta fm_instruments_pan_feedback_algo,x
  bra @end
@global_vol_m1:
  cmp #OP_GLOBAL_VOL_M1
  bne @global_vol_m2
  lda fm_instruments_global_to_op_volume,x
  jsr ui::instruments::toggle_first_flag
  sta fm_instruments_global_to_op_volume,x
  bra @end
@global_vol_m2:
  cmp #OP_GLOBAL_VOL_M2
  bne @global_vol_c1
  lda fm_instruments_global_to_op_volume,x
  jsr ui::instruments::toggle_second_flag
  sta fm_instruments_global_to_op_volume,x
  bra @end
@global_vol_c1:
  cmp #OP_GLOBAL_VOL_C1
  bne @global_vol_c2
  lda fm_instruments_global_to_op_volume,x
  jsr ui::instruments::toggle_third_flag
  sta fm_instruments_global_to_op_volume,x
  bra @end
@global_vol_c2:
  cmp #OP_GLOBAL_VOL_C2
  bne @am_mod_en_m1
  lda fm_instruments_global_to_op_volume,x
  jsr ui::instruments::toggle_fourth_flag
  sta fm_instruments_global_to_op_volume,x
@am_mod_en_m1:
  cmp #AM_MOD_EN_M1
  bne @am_mod_en_m2
  lda fm_instruments_am_decay1_m1,x
  jsr ui::instruments::toggle_first_flag
  sta fm_instruments_am_decay1_m1,x
@am_mod_en_m2:
  cmp #AM_MOD_EN_M2
  bne @am_mod_en_c1
  lda fm_instruments_am_decay1_m2,x
  jsr ui::instruments::toggle_first_flag
  sta fm_instruments_am_decay1_m2,x
@am_mod_en_c1:
  cmp #AM_MOD_EN_C1
  bne @am_mod_en_c2
  lda fm_instruments_am_decay1_c1,x
  jsr ui::instruments::toggle_first_flag
  sta fm_instruments_am_decay1_c1,x
@am_mod_en_c2:
  cmp #AM_MOD_EN_C2
  bne @end
  lda fm_instruments_am_decay1_c2,x
  jsr ui::instruments::toggle_first_flag
  sta fm_instruments_am_decay1_c2,x
@end:
  jsr load_instrument
  jsr graphics::drawing::cursor_plot
  rts

.endproc