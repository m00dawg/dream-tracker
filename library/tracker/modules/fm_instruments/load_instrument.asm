.proc load_instrument
@start:
	lda #%00010000	; Inc set to 1, lo ram
	sta VERA_addr_high
	ldx zp_INSTRUMENT
@set_algo:
	lda #ALGO_X
	ldy #ALGO_Y
	jsr graphics::drawing::goto_xy
	lda #PARAMETER_COLOR
	sta zp_TEXT_COLOR
	lda fm_instruments_pan_feedback_algo,x
	and #%00000111
	jsr graphics::printing::print_single_hex

@set_pan:
	lda #LEFT_X
	sta left_x
	lda #LEFT_Y
	sta left_y
	lda fm_instruments_pan_feedback_algo,x
	sta pan
	jsr ui::instruments::set_pan

@set_sensitivity_phase:
	; Reset since the set_pan changed the text color
    lda #PARAMETER_COLOR
	sta zp_TEXT_COLOR

	lda #SENSITIVITY_PHASE_X
	ldy #SENSITIVITY_PHASE_Y
	jsr graphics::drawing::goto_xy
	lda fm_instruments_modulation_sensitivity,x
	and #%01110000
	ror4
	jsr graphics::printing::print_single_hex

@set_sensitivity_amplitude:
	lda #SENSITIVITY_AMP_X
	ldy #SENSITIVITY_AMP_Y
	jsr graphics::drawing::goto_xy
	lda fm_instruments_modulation_sensitivity,x
	and #%00000011
	jsr graphics::printing::print_single_hex

@global_vol:
	lda #GLOBAL_VOL_X
	ldy #GLOBAL_VOL_Y
	jsr graphics::drawing::goto_xy
	lda fm_instruments_global_volume,x
	jsr graphics::printing::print_hex

@op_global_vol:
	lda fm_instruments_global_to_op_volume,x
	sta temp_register0
@m1_vol:
	bbs 7, temp_register0,@m1_vol_set
@m1_vol_not_set:
	lda #OP_GLOBAL_VOLUME_M1_X
	ldy #OP_GLOBAL_VOLUME_M1_Y
	jsr global_op_volume_inactive
	bra @m2_vol
@m1_vol_set:
	lda #OP_GLOBAL_VOLUME_M1_X
	ldy #OP_GLOBAL_VOLUME_M1_Y
	jsr global_op_volume_active
@m2_vol:
	bbs 6, temp_register0,@m2_vol_set
@m2_vol_not_set:
	lda #OP_GLOBAL_VOLUME_M2_X
	ldy #OP_GLOBAL_VOLUME_M2_Y
	jsr global_op_volume_inactive
	bra @c1_vol
@m2_vol_set:
	lda #OP_GLOBAL_VOLUME_M2_X
	ldy #OP_GLOBAL_VOLUME_M2_Y
	jsr global_op_volume_active
@c1_vol:
	bbs 5, temp_register0,@c1_vol_set
@c1_vol_not_set:
	lda #OP_GLOBAL_VOLUME_C1_X
	ldy #OP_GLOBAL_VOLUME_C1_Y
	jsr global_op_volume_inactive
	bra @c2_vol
@c1_vol_set:
	lda #OP_GLOBAL_VOLUME_C1_X
	ldy #OP_GLOBAL_VOLUME_C1_Y
	jsr global_op_volume_active
@c2_vol:
	bbs 4, temp_register0,@c2_vol_set
@c2_vol_not_set:
	lda #OP_GLOBAL_VOLUME_C2_X
	ldy #OP_GLOBAL_VOLUME_C2_Y
	jsr global_op_volume_inactive
	bra @op_global_vol_end
@c2_vol_set:
	lda #OP_GLOBAL_VOLUME_C2_X
	ldy #OP_GLOBAL_VOLUME_C2_Y
	jsr global_op_volume_active
@op_global_vol_end:
@m1_feedback:
	lda #M1_FEEDBACK_X
	ldy #M1_FEEDBACK_Y
	jsr graphics::drawing::goto_xy
	lda fm_instruments_pan_feedback_algo,x
	and #%00111000
	lsr3
	jsr graphics::printing::print_single_hex

@m1:
	stz operator_offset
	ldy #M1_VOLUME_Y
	jsr display_operator

@m2:
	inc operator_offset
	ldy #M2_VOLUME_Y
	jsr display_operator

@c1:
	inc operator_offset
	ldy #C1_VOLUME_Y
	jsr display_operator

@c2:
	inc operator_offset
	ldy #C2_VOLUME_Y
	jsr display_operator

@end:
	lda #%00010001	; Inc set to 1, hi ram
  	sta VERA_addr_high
	rts
.endproc

.proc display_operator
; We use volume, *not* attenuation which the actual YM2151
; uses. Volume is converted to attenuation when writing
; to the chip registers (e.g. in read_row)
@volume:
	lda #OP_VOLUME_X
	jsr graphics::drawing::goto_xy
	fm_get_op operator_offset, volume_lo, volume_hi
	jsr graphics::printing::print_hex
@am_mode_and_decay1:
	fm_get_op operator_offset, am_decay1_lo, am_decay1_hi
	sta temp_register0
@am_mode:
	iny	; From here on out every other item is under volume_y
	lda temp_register0
	and #%10000000
	sta flag
	lda #OP_AM_MOD_EN_X
	jsr print_flag
@decay1:
	; reset since am_mode changed the color
    lda #PARAMETER_COLOR
	sta zp_TEXT_COLOR
	phy
	iny
	iny
	iny
	lda #OP_DECAY_1_RATE_X
	jsr graphics::drawing::goto_xy
	lda temp_register0
	and #%00011111		; Decay 1
	jsr graphics::printing::print_hex
@keyscale_attack:
	fm_get_op operator_offset, ks_attack_lo, ks_attack_hi
	sta temp_register0
@keyscale:
	ply	; Go back to the 2nd-row Y-position offset
	lda #OP_KEY_SCALE_X
	jsr graphics::drawing::goto_xy
	lda temp_register0
	lsr6
	jsr graphics::printing::print_single_hex
@attack:
	phy
	iny
	iny
	lda #OP_ATTACK_X
	jsr graphics::drawing::goto_xy
	lda temp_register0
	and #%00111111
	jsr graphics::printing::print_hex
@detune_fine_freq_mul:
	ply	; Go back to the 2nd-row Y-position offset
	fm_get_op operator_offset, detune_fine_frequency_mul_lo, detune_fine_frequency_mul_hi
	sta temp_register0
@freq_mul:
	lda #OP_FREQ_MULT_X
	jsr graphics::drawing::goto_xy
	lda temp_register0
	and #%00001111
	jsr graphics::printing::print_single_hex
@detune_fine:
	iny	; No need to push as everything is on this row or lower
	lda #OP_DETINE_FINE_X
	jsr graphics::drawing::goto_xy
	lda temp_register0
	and #%01110000
	lsr4
	jsr graphics::printing::print_single_hex
@detune_coarse_decay2:
	fm_get_op operator_offset, detune_coarse_decay2_lo, detune_coarse_decay2_hi
	sta temp_register0
@detune_coarse:
	lda #OP_DETUNE_COARSE_X
	jsr graphics::drawing::goto_xy
	lda temp_register0
	and #%11000000
	lsr6
	jsr graphics::printing::print_single_hex
@decay2:
	lda #OP_DECAY_2_RATE_X
	iny
	phy	; From here on out this is where y-positions now start
	iny
	jsr graphics::drawing::goto_xy
	lda temp_register0
	and #%00011111
	jsr graphics::printing::print_hex
@decay1_release:
	ply
	fm_get_op operator_offset, decay1_release_lo, decay1_release_hi
	sta temp_register0
@release:
	lda #OP_RELEASE_X
	jsr graphics::drawing::goto_xy
	lda temp_register0
	and #%00001111
	jsr graphics::printing::print_single_hex
@decay1_level:
	lda #OP_DECAY_1_LEVEL_X
	iny
	jsr graphics::drawing::goto_xy
	lda temp_register0
	lsr4
	jsr graphics::printing::print_single_hex
@end:
	rts
.endproc

; a = x-position
; y = y-position
; env_flags_temp = flag (zero vs non-zero)
.proc print_flag
@start:
	pha
	lda #PARAM_ACTIVE_COLOR
	sta enable_color
	lda #PARAM_INACTIVE_COLOR
	sta disable_color
	pla
	jsr ui::instruments::print_flag
	rts
.endproc

; a = volx
; y = voly
.proc global_op_volume_active
	jsr graphics::drawing::goto_xy
	lda VERA_data0
	lda #PARAM_ACTIVE_COLOR
	sta VERA_data0
	lda VERA_data0
	lda #PARAM_ACTIVE_COLOR
	sta VERA_data0
	rts
.endproc

.proc global_op_volume_inactive
	jsr graphics::drawing::goto_xy
	lda VERA_data0
	lda #PARAM_INACTIVE_COLOR
	sta VERA_data0
	lda VERA_data0
	lda #PARAM_INACTIVE_COLOR
	sta VERA_data0
	rts
.endproc