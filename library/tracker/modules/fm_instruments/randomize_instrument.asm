; Randomize values for current instrument
.proc randomize_instrument
	ldx #sound::fm::NUM_INSTRUMENT_REGISTERS
	jsr math::push_entropy

	; Since we know how many instruments there are
	; it would probably save a good chunk of memory
	; to loop and simply jump $80 (I think that's right)
	; which would do the same thing. This is true
	; for the code that does things like loads
	; instruments into channel data.
	ldx zp_INSTRUMENT
	pla
	sta fm_instruments_global_volume,x
	pla
	sta fm_instruments_global_to_op_volume,x
	pla
	sta fm_instruments_pan_feedback_algo,x
	pla
	sta fm_instruments_modulation_sensitivity,x
	pla
	sta fm_instruments_detune_fine_frequency_mul_m1,x
	pla
	sta fm_instruments_detune_fine_frequency_mul_m2,x
	pla
	sta fm_instruments_detune_fine_frequency_mul_c1,x
	pla
	sta fm_instruments_detune_fine_frequency_mul_c2,x
	pla
	sta fm_instruments_volume_m1,x
	pla
	sta fm_instruments_volume_m2,x
	pla
	sta fm_instruments_volume_c1,x
	pla
	sta fm_instruments_volume_c2,x
	pla
	sta fm_instruments_ks_attack_m1,x
	pla
	sta fm_instruments_ks_attack_m2,x
	pla
	sta fm_instruments_ks_attack_c1,x
	pla
	sta fm_instruments_ks_attack_c2,x
	pla
	sta fm_instruments_am_decay1_m1,x
	pla
	sta fm_instruments_am_decay1_m2,x
	pla
	sta fm_instruments_am_decay1_c1,x
	pla
	sta fm_instruments_am_decay1_c2,x
	pla
	sta fm_instruments_detune_coarse_decay2_m1,x
	pla
	sta fm_instruments_detune_coarse_decay2_m2,x
	pla
	sta fm_instruments_detune_coarse_decay2_c1,x
	pla
	sta fm_instruments_detune_coarse_decay2_c2,x
	pla
	sta fm_instruments_decay1_release_m1,x
	pla
	sta fm_instruments_decay1_release_m2,x
	pla
	sta fm_instruments_decay1_release_c1,x
	pla
	sta fm_instruments_decay1_release_c2,x
	jsr load_instrument
	rts
.endproc
