; LUTs

INSTRUMENT_EDIT_LEFT = $01
INSTRUMENT_EDIT_RIGHT = $02
; Old before re-org
;OP_GLOBAL_VOL_M1 = $06
;OP_GLOBAL_VOL_M2 = $07
;OP_GLOBAL_VOL_C1 = $08
;OP_GLOBAL_VOL_C2 = $09
;AM_MOD_EN_M1 = $0C
;AM_MOD_EN_M2 = $17
;AM_MOD_EN_C1 = $22
;AM_MOD_EN_C2 = $2D

OP_GLOBAL_VOL_M1 = $06
OP_GLOBAL_VOL_C1 = $07
OP_GLOBAL_VOL_M2 = $08
OP_GLOBAL_VOL_C2 = $09
AM_MOD_EN_M1 = $0C
AM_MOD_EN_C1 = $17
AM_MOD_EN_M2 = $22
AM_MOD_EN_C2 = $2D


; Locations where instrument positions are
; (for cursor hopping)
; I should use constants here but I think it'd make it pretty 
; hard to read.
instrument_edit_x_pos_lut:
  ; Algo
  .byte $2A
  ; L, R, Phase, Amplitude
  .byte $29, $2A, $39, $4B
  ; Global Vol; Op Global Vol M1, M2, C1, C2
  .byte $29, $3D, $3F, $41, $43
  ; M1
  ; Volume, Feedback
  .byte $29, $4B
  ; AM Mod, Key Scale, Freq Mult
  .byte $2A, $39, $4B
  ; Detune Fine and Coarse
  .byte $2A, $39
  ; Attack, Release
  .byte $29, $39
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $29, $39, $4A 
  ; C1
  ; Volume
  .byte $29
  ; AM Mod, Key Scale, Freq Mult
  .byte $2A, $39, $4B
  ; Detune Fine and Coarse
  .byte $2A, $39
  ; Attack, Release
  .byte $29, $39
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $29, $39, $4A 
  ; M2
  ; Volume
  .byte $29
  ; AM Mod, Key Scale, Freq Mult
  .byte $2A, $39, $4B
  ; Detune Fine and Coarse
  .byte $2A, $39
  ; Attack, Release
  .byte $29, $39
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $29, $39, $4A 
  ; C2
  ; Volume
  .byte $29
  ; AM Mod, Key Scale, Freq Mult
  .byte $2A, $39, $4B
  ; Detune Fine and Coarse
  .byte $2A, $39
  ; Attack, Release
  .byte $29, $39
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $29, $39, $4A 

instrument_edit_y_pos_lut:
  ; Algo
  .byte $0D
  ; L, R, Phase, Amplitude
  .byte $0E,$0E,$0E,$0E
  ; Global Vol; Op Global Vol M1, M2, C1, C2
  .byte $10, $10, $10, $10, $10
  ; M1
  ; Volume, Feedback
  .byte $13, $13
  ; AM Mod, Key Scale, Freq Mult
  .byte $14, $14, $14
  ; Detune Fine and Coarse
  .byte $15, $15
  ; Attack, Release
  .byte $16, $16
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $17, $17, $17
  ; C1
  ; Volume
  .byte $1A
  ; AM Mod, Key Scale, Freq Mult
  .byte $1B, $1B, $1B
  ; Detune Fine and Coarse
  .byte $1C, $1C
  ; Attack, Release
  .byte $1D, $1D
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $1E, $1E, $1E
  ; M2
  ; Volume
  .byte $21
  ; AM Mod, Key Scale, Freq Mult
  .byte $22, $22, $22
  ; Detune Fine and Coarse
  .byte $23, $23
  ; Attack, Release
  .byte $24, $24
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $25, $25, $25
  ; C2
  ; Volume
  .byte $28
  ; AM Mod, Key Scale, Freq Mult
  .byte $29, $29, $29
  ; Detune Fine and Coarse
  .byte $2A, $2A
  ; Attack, Release
  .byte $2B, $2B
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $2C, $2C, $2C

; For each item, how many times do we meed to move
; to the next edit position to effectively move down
; to the item below the current one
instrument_edit_cursor_down_lut:
  ; Algo
  .byte $01
  ; L, R, Phase, Amplitude
  .byte $04, $03, $0A, $02
  ; Global Vol; Op Global Vol M1, M2, C1, C2
  .byte $05, $05, $04, $03, $02
 
  ; M1
  ; Volume, Feedback
  .byte $02, $03
  ; AM Mod, Key Scale, Freq Mult
  .byte $03, $03, $07
  ; Detune Fine and Coarse
  .byte $02, $02
  ; Attack, Release
  .byte $02, $02
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $03, $04, $04
  
  ; C1
  ; Volume
  .byte $01
  ; AM Mod, Key Scale, Freq Mult
  .byte $03, $03, $07
  ; Detune Fine and Coarse
  .byte $02, $02
  ; Attack, Release
  .byte $02, $02
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $03, $04, $04

  ; M2
  ; Volume
  .byte $01
  ; AM Mod, Key Scale, Freq Mult
  .byte $03, $03, $07
  ; Detune Fine and Coarse
  .byte $02, $02
  ; Attack, Release
  .byte $02, $02
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $03, $04, $04

  ; C2
  ; Volume
  .byte $01
  ; AM Mod, Key Scale, Freq Mult
  .byte $03, $03, $07
  ; Detune Fine and Coarse
  .byte $02, $02
  ; Attack, Release
  .byte $02, $02
  ; Decay 1 Rate, Level; Decay 2 Rate
  ; (At bottom)
  .byte $00, $00, $00

; For each item, how many times do we meed to move
; to the next edit position to effectively move up
; to the item above the current one
instrument_edit_cursor_up_lut:
  ; Algo
  ; (at top)
  .byte $00
  ; L, R, Phase, Amplitude
  .byte $01, $02, $00, $00
  ; Global Vol; Op Global Vol M1, M2, C1, C2
  .byte $04, $02, $03, $04, $05
 
  ; M1
  ; Volume, Feedback
  .byte $05, $07
  ; AM Mod, Key Scale, Freq Mult
  .byte $02, $0A, $03
  ; Detune Fine and Coarse
  .byte $03, $03
  ; Attack, Release
  .byte $02, $02
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $02, $02, $07
  
  ; C1
  ; Volume
  .byte $03
  ; AM Mod, Key Scale, Freq Mult
  .byte $01, $04, $04
  ; Detune Fine and Coarse
  .byte $03, $03
  ; Attack, Release
  .byte $02, $02
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $02, $02, $07

  ; M2
  ; Volume
  .byte $03
  ; AM Mod, Key Scale, Freq Mult
  .byte $01, $04, $04
  ; Detune Fine and Coarse
  .byte $03, $03
  ; Attack, Release
  .byte $02, $02
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $02, $02, $07

  ; C2
  ; Volume
  .byte $03
  ; AM Mod, Key Scale, Freq Mult
  .byte $01, $04, $04
  ; Detune Fine and Coarse
  .byte $03, $03
  ; Attack, Release
  .byte $02, $02
  ; Decay 1 Rate, Level; Decay 2 Rate
  .byte $02, $02, $07


; Register/Carrier offset LUTs
detune_fine_frequency_mul_lo:
  .byte <fm_instruments_detune_fine_frequency_mul_m1
  .byte <fm_instruments_detune_fine_frequency_mul_m2
  .byte <fm_instruments_detune_fine_frequency_mul_c1
  .byte <fm_instruments_detune_fine_frequency_mul_c2
detune_fine_frequency_mul_hi:
  .byte >fm_instruments_detune_fine_frequency_mul_m1
  .byte >fm_instruments_detune_fine_frequency_mul_m2
  .byte >fm_instruments_detune_fine_frequency_mul_c1
  .byte >fm_instruments_detune_fine_frequency_mul_c2

volume_lo:
  .byte <fm_instruments_volume_m1
  .byte <fm_instruments_volume_m2
  .byte <fm_instruments_volume_c1
  .byte <fm_instruments_volume_c2
volume_hi:
  .byte >fm_instruments_volume_m1
  .byte >fm_instruments_volume_m2
  .byte >fm_instruments_volume_c1
  .byte >fm_instruments_volume_c2

ks_attack_lo:
  .byte <fm_instruments_ks_attack_m1
  .byte <fm_instruments_ks_attack_m2
  .byte <fm_instruments_ks_attack_c1
  .byte <fm_instruments_ks_attack_c2
ks_attack_hi:
  .byte >fm_instruments_ks_attack_m1
  .byte >fm_instruments_ks_attack_m2
  .byte >fm_instruments_ks_attack_c1
  .byte >fm_instruments_ks_attack_c2

am_decay1_lo:
  .byte <fm_instruments_am_decay1_m1
  .byte <fm_instruments_am_decay1_m2
  .byte <fm_instruments_am_decay1_c1
  .byte <fm_instruments_am_decay1_c2
am_decay1_hi:
  .byte >fm_instruments_am_decay1_m1
  .byte >fm_instruments_am_decay1_m2
  .byte >fm_instruments_am_decay1_c1
  .byte >fm_instruments_am_decay1_c2

detune_coarse_decay2_lo:
  .byte <fm_instruments_detune_coarse_decay2_m1
  .byte <fm_instruments_detune_coarse_decay2_m2
  .byte <fm_instruments_detune_coarse_decay2_c1
  .byte <fm_instruments_detune_coarse_decay2_c2
detune_coarse_decay2_hi:
  .byte >fm_instruments_detune_coarse_decay2_m1
  .byte >fm_instruments_detune_coarse_decay2_m2
  .byte >fm_instruments_detune_coarse_decay2_c1
  .byte >fm_instruments_detune_coarse_decay2_c2

decay1_release_lo:
  .byte <fm_instruments_decay1_release_m1
  .byte <fm_instruments_decay1_release_m2
  .byte <fm_instruments_decay1_release_c1
  .byte <fm_instruments_decay1_release_c2
decay1_release_hi:
  .byte >fm_instruments_decay1_release_m1
  .byte >fm_instruments_decay1_release_m2
  .byte >fm_instruments_decay1_release_c1
  .byte >fm_instruments_decay1_release_c2