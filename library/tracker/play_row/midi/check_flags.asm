.proc check_flags
@check_note_flags:
  lda zp_ROW_NOTE
  cmp #NOTEREL
  beq @note_rel
  cmp #NOTEOFF
  beq @note_off
  cmp #NOTENULL
  beq @note_null
  ; If it's none of these, set note to active
  smb 7,zp_NOTE_FLAGS
  rts

; same as note off for now
@note_rel:
  smb 6,zp_NOTE_FLAGS
  ; Set channel release flag to on
  ldx zp_CHANNEL_COUNT
  lda sound::midi::channel_flags,x
  ora #%01000000
  sta sound::midi::channel_flags,x

  lda zp_ROW_VOLUME
  sta zp_ARG0
  jsr note_release

  rts
@note_off:
  ; Set note off flag, reset note play flag
  smb 5,zp_NOTE_FLAGS
  ; Set channel active flag to off
  ; and volume to zero
  ldx zp_CHANNEL_COUNT
  lda sound::midi::channel_flags,x
  and #%01111111
  sta sound::midi::channel_flags,x
  lda #$00
  sta sound::midi::channel_vol,x
	; Send MIDI all notes off
  lda sound::midi::channel_midi_channel,x
  sta zp_MIDI_CHANNEL
  ldx #sound::midi::message::cc::ALL_SOUND_OFF
	lda #$00
	jsr sound::midi::commands::send_cc
  rts
@note_null:
  ; Set null flag and continue since some 
  ; items (like volume) still need to function
  ; with null notes
  smb 4,zp_NOTE_FLAGS
	rts
.endproc