; Read volume from pattern data
; First we check for note-off and if we have one, skip
; (we handle note off later)

; If note was played
;   if colum has volume, use volume
;		if not, set max volume
; If note was not played
;   For anything in the volume col, do that immediately

.proc read_volume
  ATTENUATION = MATH_TEMP

  ldx zp_CHANNEL_COUNT
  ; Was a note played?
  bbr 7, zp_NOTE_FLAGS, @note_not_played
  
@note_played:
  ; check if volume column is null
  ; if so, use max volume
  lda zp_ROW_VOLUME
  cmp #VOLNULL
  bne @note_played_volume_not_null
@note_played_volume_null:
  lda #sound::midi::MAX_VOLUME
	sta sound::midi::channel_vol,x
  rts

@note_not_played:
  ; check if volume column is null
  ; and there is no note, do nothing.
  ; else set volume
  lda zp_ROW_VOLUME
  cmp #VOLNULL
  bne @note_not_played_volume_not_null
  rts

@note_played_volume_not_null:
	; If note was played and included volume, store that in the channel
	; When we go to evaluate the note, we'll send this volume with note-on
  lda zp_ROW_VOLUME
  cmp #sound::midi::MAX_VOLUME
  bgt @note_played_attenuate
  sta sound::midi::channel_vol,x
  rts
@note_played_attenuate:
  and #%01111111
	sta sound::midi::channel_attenuation,x
  lda #sound::midi::MAX_VOLUME
	sta sound::midi::channel_vol,x
	rts

@note_not_played_volume_not_null:
  ; Note wasn't played but we have volume
  ; If it's >=80, that's attenuation
  ; Else it's the MIDI volume
  lda zp_ROW_VOLUME
  cmp #sound::midi::MAX_VOLUME
  bgt @attenuate
@set_volume:
  lda sound::midi::channel_attenuation,x
  sta ATTENUATION
  lda sound::midi::channel_midi_channel,x
  sta zp_MIDI_CHANNEL
  lda zp_ROW_VOLUME
  suba ATTENUATION
	sta sound::midi::channel_vol,x
	ldx #sound::midi::message::cc::VOLUME_COARSE
	jsr sound::midi::commands::send_cc
  rts
@attenuate:
  and #%01111111
  sta sound::midi::channel_attenuation,x
  rts
.endproc
