; Read instrument from pattern data
; and, if need be, execute a program change
.proc read_instrument
  ; If we have a note-off skip
  ; we check for this before
  ; bbs 5, NOTE_FLAGS, @end 

  lda zp_ROW_INSTRUMENT
  cmp #INSTNULL
  beq @end
@inst_check:
  ; If instrument number is different, we need to
	; send a program change
  ldx zp_CHANNEL_COUNT
  lda sound::midi::channel_instrument,x
  cmp zp_ROW_INSTRUMENT
  beq @end
  ; If the instrument is different than the current
  ; instrument for the channel, load it in
@inst_update_channel:
	lda zp_ROW_INSTRUMENT
	sta sound::midi::channel_instrument,x
  lda sound::midi::channel_midi_channel,x
  sta zp_ARG0 ; MIDI channel
  jsr sound::midi::commands::program_change
@end:
  rts

.endproc