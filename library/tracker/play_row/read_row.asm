; Read row and put into some ZP addresses
.proc read_row
  push_rambank
  ; note
  ldy CHANNEL_OFFSET     
  lda (zp_ROW_POINTER),y
  ; If less than NOTEREL value, transpose applies
  cmp #NOTEREL
  bge @continue
@transpose:
  pha
  rambank zp_BANK_MISC
  pla
  clc
  adc global_transpose 
  ldx zp_PATTERN_NUMBER
  stx RAM_BANK
  ; If transpose results in a note above NOTEREL, just set to max pitch note
@check_max_note:
  cmp #NOTEREL
  blt @continue
  lda #NOTEREL - 1
@continue:
  sta zp_ROW_NOTE
  ; instrument
  iny
  lda (zp_ROW_POINTER),y
  sta zp_ROW_INSTRUMENT
  ; volume
  iny
  lda (zp_ROW_POINTER),y
  sta zp_ROW_VOLUME
  ; effect param
  iny
  lda (zp_ROW_POINTER),y
  sta zp_ROW_EFFECT_PARAMETER
  ; effect value
  iny
  lda (zp_ROW_POINTER),y
  sta zp_ROW_EFFECT_VALUE

  pop_rambank
  rts
.endproc