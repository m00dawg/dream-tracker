; Read instrument from pattern data
.proc read_instrument
  ; If we have a note-off skip
  ; we check for this before
  ; bbs 5, NOTE_FLAGS, @end 

  lda zp_ROW_INSTRUMENT
  cmp #INSTNULL
  beq @end
@inst_check:
  ; If instrument number is different, we need to
  ; load the instrument defaults into the channel
  ; Since we're doing FM, this may need to be a diff to save cycles
  ldx zp_CHANNEL_COUNT
  lda sound::fm::channel_instrument,x
  cmp zp_ROW_INSTRUMENT
  beq @end
  ; If the instrument is different than the current
  ; instrument for the channel, load it in
@inst_update_channel:
  lda zp_CHANNEL_COUNT
  sta zp_ARG0
  lda zp_ROW_INSTRUMENT
  sta zp_ARG1
  jsr sound::fm::update_channel_instrument
@end:
  rts

.endproc