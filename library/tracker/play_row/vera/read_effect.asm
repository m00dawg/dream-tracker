; Read effect from pattern
; zp_ADDR_RETURN must be set appropriately!
; For singular effects it should be the rts
; at the bottom of this routine.

; For tables/macros it will be different

; a = effect param
; y = effect value

.proc read_effect
  cmp #EFFNULL
  bne @evaluate_jump_table
  rts
@evaluate_jump_table:
  ; Effects expect to jump back to a return
  ldx #<@end
  stx zp_ADDR_RETURN
  ldx #>@end
  stx zp_ADDR_RETURN + 1

  ; Start large to small
  cmp #sound::GLOBAL_EFFECTS_START
  bge @global_effect  
  cmp #sound::vera::UNIQUE_EFFECTS_START
  bge @unqiue_effect
  cmp #sound::vera::REGISTER_EFFECTS_START
  bge @register_effect

  ; If nothing else, it's a shared effect so fall through
@shared_effect:
  cmp #sound::vera::MAX_SHARED_EFFECT + 1
  blt @valid_shared_effect
  rts

@valid_shared_effect:
  ; Cool 65C02 way of doing jump tables
  asl
  tax 
  lda zp_CHANNEL_COUNT  
  jmp (vera_shared_effects_jump_table,x)
  
@global_effect:
  cmp #sound::MAX_GLOBAL_EFFECT + 1
  blt @valid_global_effect
  rts
@valid_global_effect:
  ; Subtract the start of the global effect range
  ; to get the range of the jump table
  suba #sound::GLOBAL_EFFECTS_START
  ; Cool 65C02 way of doing jump tables
  asl
  tax
  jmp (global_effects_jump_table,x)

@unqiue_effect:
  cmp #sound::vera::MAX_UNIQUE_EFFECT + 1
  blt @valid_unique_effect
  rts
@valid_unique_effect:
  ; Subtract the start of the unique effect range
  ; after the direct register write range
  ; to get the range of the jump table
  suba #sound::vera::UNIQUE_EFFECTS_START
  ; Cool 65C02 way of doing jump tables
  asl
  tax
  lda zp_CHANNEL_COUNT   
  jmp (vera_unique_effects_jump_table,x)

@register_effect:
  cmp #sound::vera::REGISTER_EFFECTS_STOP + 1
  blt @valid_register_effect
  rts
@valid_register_effect:
  lda zp_CHANNEL_COUNT
  jsr sound::vera::effects::update_channel_register

@end:
  rts
.endproc