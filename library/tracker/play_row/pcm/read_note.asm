; Read note from pattern data
.proc read_note
  lda zp_ROW_INSTRUMENT
  cmp #INSTNULL
  beq @end

  lda zp_ROW_NOTE
  sta zp_PCM_SAMPLE_RATE
  jsr sound::pcm::calc_num_chonks

@note_play:
  ; Set play flag and channel active
  smb 7,zp_NOTE_FLAGS
  smb 7,zp_PCM_FLAGS
  stz zp_PCM_CHONK_COUNT
  stz VERA_audio_rate
  ; Reset buffer
  lda #%10000000
  sta VERA_audio_ctrl


@end:
  rts

.endproc