; Read instrument from pattern data
.proc read_instrument
  ; If we have a note-off skip
  ; we check for this before
  ; bbs 5, NOTE_FLAGS, @end 
  lda zp_ROW_INSTRUMENT
  cmp #INSTNULL
  beq @end
  sta zp_PCM_SAMPLE_NUMBER
  adda sound::pcm::bank_start
  sta zp_PCM_SAMPLE_PAGE
  lda #<sound::pcm::SAMPLE_ADDRESS
  sta zp_PCM_SAMPLE_ADDRESS
  lda #>sound::pcm::SAMPLE_ADDRESS
  sta zp_PCM_SAMPLE_ADDRESS + 1
@end:
  rts

.endproc