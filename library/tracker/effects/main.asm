; Global effects
.scope effects
	PATTERN_BREAK = $C2
	JUMP_TO_ORDER = $C3

	.include "library/tracker/effects/stop_song.asm"
	.include "library/tracker/effects/pattern_break.asm"
	.include "library/tracker/effects/set_speed.asm"
	.include "library/tracker/effects/jump_to_order.asm"
	.include "library/tracker/effects/set_global_transpose.asm"
	.include "library/tracker/effects/null_effect.asm"
	.include "library/tracker/effects/evaluate_scroll_effect.asm"
	.include "library/tracker/effects/change_background_color_r.asm"
	.include "library/tracker/effects/change_background_color_gb.asm"
.endscope