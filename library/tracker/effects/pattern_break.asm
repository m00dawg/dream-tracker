; Jump to end of pattern (and thus go to the next 
; pattern)

.proc pattern_break
return = zp_ADDR_RETURN
    lda #ROW_MAX - 1
    sta zp_ROW_NUMBER
    jmp (return)
.endproc