; Immediately jump to order
; y = order

.proc jump_to_order
return = zp_ADDR_RETURN

@start:
    dey
    sty zp_ORDER_NUMBER
    
    lda #ROW_MAX - 1
    sta zp_ROW_NUMBER

    jmp (return)
.endproc