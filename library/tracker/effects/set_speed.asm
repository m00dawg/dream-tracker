; Set song speed (generally as a pattern effect)
; y = speed

.proc set_speed
return = zp_ADDR_RETURN

@start:
    sty zp_SPEED
    jsr ui::print_speed
    jmp (return)
.endproc