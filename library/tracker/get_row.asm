; take the row count, add the number of bytes per row to get the
; address offset of the row

; Uses:
;
; a = row number
;
; zp_ARG0
; zp_MATH0
; zp_MATH1
; zp_MATH2

.proc get_row
  SKIP_BYTES       = zp_MATH2   ; return from add16
  ROW_POINTER      = zp_ARG0    ; what was zp_ROW_POINTER

@start:
  ; Now row number comes in 'a' since we need to use get_row for 
  ; both playback and infinite scroll
  ; lda zp_ROW_NUMBER
@reset_pointer:
  pha
  lda #<PATTERN_ADDRESS
  sta ROW_POINTER
  lda #>PATTERN_ADDRESS
  sta ROW_POINTER+1
  pla
  

  sta zp_MATH0
  lda #TOTAL_BYTES_PER_ROW
  sta zp_MATH1
  jsr math::multiply16
  sta SKIP_BYTES
  tya
  sta SKIP_BYTES+1

  ; this can be optimized to be in line with the above but for now...
  ; add SKIP_BYTES to ROW_POINTER (both 16-bit numbers)
  add16 SKIP_BYTES, ROW_POINTER, ROW_POINTER
  
@end:
  rts

.endproc
