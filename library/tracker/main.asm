; Core tracker engine routines
.segment "CODE"
.scope tracker
  .include "library/tracker/get_next_pattern.asm"
  .include "library/tracker/init_song.asm"
  .include "library/tracker/get_row.asm"
  .include "library/tracker/inc_row.asm"
  .include "library/tracker/inc_scroll_row.asm"
  .include "library/tracker/play_row/main.asm"
  .include "library/tracker/effects/main.asm"
  .include "library/tracker/interrupts/main.asm"
  .include "library/tracker/stop_song.asm"
  ; Moved to file module
  ;.include "library/tracker/save_song.asm"
  .include "library/tracker/load_song.asm"
  .include "library/tracker/exit.asm"



  .scope modules
    .include "library/tracker/modules/help/main.asm"
    .include "library/tracker/modules/vera_instruments/main.asm"
    .include "library/tracker/modules/fm_instruments/main.asm"
    .include "library/tracker/modules/envelopes/main.asm"
    .include "library/tracker/modules/orders/main.asm"
    .include "library/tracker/modules/play_song/main.asm"
    .include "library/tracker/modules/song_options/main.asm"
    .include "library/tracker/modules/edit_pattern/main.asm"
    .include "library/tracker/modules/samples/main.asm"
    .include "library/tracker/modules/file/main.asm"
    .include "library/tracker/modules/tables/main.asm"
    .include "library/tracker/modules/macros/main.asm"
    .include "library/tracker/modules/loading/main.asm"
  .endscope
    
.endscope
