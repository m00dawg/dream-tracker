.proc get_next_pattern
get_next_pattern:
  push_rambank
  lda zp_ROW_NUMBER
  clc
  adc #$01
  ; Good lord don't mess with this plus one or if you do
  ; know there be dragons! This caused a really weird 
  ; off by one issue with row length when playing a full
  ; song where it wouldn't get to 3F of the last row.
  ; Super weird so You've Been So Warned!!!!
  cmp #ROW_MAX + 1
  bne @return
  ; If the order value is 00, we're at the end so start over
  ldy zp_ORDER_NUMBER
  iny  
  rambank zp_BANK_MISC
  lda order_list,y
  beq @start_over
  ; Otherwise go to the next order
@load_next_pattern:
  stz zp_ROW_NUMBER
  sty zp_ORDER_NUMBER
  lda order_list,y
  sta RAM_BANK
  sta zp_PATTERN_NUMBER
  ; Do not draw next pattern (disabling this for now as it causes play stalls)
  ;jsr ui::print_pattern
  jsr ui::print_current_order
  jsr ui::print_current_pattern_number
  pop_rambank
  rts

@start_over:
  ldy #$00
  jmp @load_next_pattern

@return:
  pop_rambank
  rts
.endproc
