; Load a song from disk

; zp's used:
; zp_TMP0

.proc load_song
  ; Constants
  ;SONG_FILE_VERSION = zp_TMP0
  SAMPLE_TERMINATOR = $FF

  push_state_disable_interrupts

  jsr files::open_for_read

  ; Version of CMT save file format
  jsr CHRIN
  sta song_file_version

; Read Misc Bank
  rambank zp_BANK_MISC
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr read_page

  ; Load PSG mode to VERA
  ldx #$00
@channel_type_loop:
  lda psg_channel_modes,x
  sta sound::vera::channel_mode,x
  inx
  cpx #sound::vera::NUM_CHANNELS
  bne @channel_type_loop

; Read Envelopes
  rambank zp_BANK_ENVS
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr read_page

; Read VERA Instruments
  rambank zp_BANK_VINS
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr read_page
  
; Read FM Instruments
  rambank zp_BANK_FINS
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr read_page

; Patterns
@read_patterns_loop:
  ; This is the pattern number, which will get placed into hiram in
  ; the same page number
  jsr CHRIN
  beq @read_patterns_done
  sta RAM_BANK

  ; Reset pattern pointer back to default
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr read_page
  jmp @read_patterns_loop
@read_patterns_done:
  lda #<PATTERN_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PATTERN_ADDRESS
  sta zp_PAGE_POINTER + 1

@read_samples_loop:
  ; This is the sample number, which will be added to the 
  ; sample page offset and place at that location in BRAM
  jsr CHRIN
  cmp #SAMPLE_TERMINATOR
  beq @read_samples_done
  adda sound::pcm::bank_start
  sta RAM_BANK

  ; Reset pattern pointer back to default
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr read_page
  jmp @read_samples_loop
@read_samples_done:
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1

@end:
  jsr files::close_file
  rambank zp_BANK_MISC
  lda starting_speed
  sta zp_SPEED
  ; Return to previous CPU state
  plp
  rts
.endproc

.proc read_page
  NUM_BYTES = $80
  ldx #$40
@read_loop:
  phx
  clc
  lda #NUM_BYTES  ; Number of bytes to read (128)
  ldy zp_PAGE_POINTER + 1 ; High Address
  ldx zp_PAGE_POINTER     ; Low Address
  jsr MACPTR
  add16to8 zp_PAGE_POINTER, #NUM_BYTES, zp_PAGE_POINTER
  plx
  dex
  bne @read_loop
@end:
  rts
.endproc