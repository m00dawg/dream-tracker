# Pattern Effects

These are the new (0.70 and later) effects. Songs made with earlier versions of 
DreamTracker will be converted during loading.

Work is still being done to implement the listed effects which can be tracked by
looking at the "Implemented" column of the effect lists.

To see proposed effects, take a look at this 
[Google Doc](https://docs.google.com/spreadsheets/d/1G_BXF6qIVUCmQEDuQHMB39AGspkbu9xLFtxsQwqJwV0/edit?usp=sharing)

Pattern effects are used to modulate sounds in various ways 
and can be used in both patterns as well as (if/once supported) tables.

Effects have a byte parameter and a byte value. For instance `1234` would
be effect 12 with a value of 34.

Effects are currently divided into 3 groups:

  * **Shared Effects**: These are effects that work basically the same way across
    multiple sound options (typically PSG and FM though some effects also 
    operate on PCM)
  * **Unique Effects**: Effects which are unique to the soundchip where the 
    parameters values are reused across the sound options but do different things.
  * **Global Effects**: Effects which operate on the entire song. The parameters
    are the same regardless of the channel they are used in.

## Shared Effects

| Hex   | Effect                     | Supported  | Persistent | Implemented |
| ----- | -------------------------- | ---------- | ---------- | ----------- |
| 00xx  | Pitch Slide Down           | PSG/FM/PCM | No         | PSG/FM      |
| 01xx  | Pitch Slide Up             | PSG/FM/PCM | No         | PSG/FM      |
| 02xx  | Pitch Sweep Down           | PSG/FM/PCM | Yes        | PSG/FM      |
| 03xx  | Pitch Sweep Up             | PSG/FM/PCM | Yes        | PSG/FM      |
| 04xx  | Volume Slide Down/Up       | PSG/FM/PCM | No         | PSG         |
| 05xx  | Volume Sweep Down/Up       | PSG/FM     | Yes        | PSG         |
| 06xx  | Glissando (Glide)          | PSG/FM      | Yes        | PSG/FM      |
| 07-x  | Legato                     | PSG/MIDI/WT/FM | Yes        | PSG/MIDI/WT/FM |
| 08xy  | Vibrato                    | PSG/FM/PCM | Yes        | PSG         |
| 09xy  | Tremolo                    | PSG/FM/PCM | Yes        | PSG         |
| 0Axy  | Arpeggio                   | PSG/FM     | Yes        | PSG         |
| 0Bxy  | Arp Direction / Speed      | PSG/FM     | Yes        | PSG         |
| 0Cxx  | Panolo                     | PSG/FM     | Yes        | No          |
| 0Dxx  | Volume                     | PSG/FM/PCM | Yes        | No          |
| 0E-x  | Pan                        | PSG/FM     | Yes        | PSG         |
| 0Fxx  | Note Cut                   | PSG/FM/PCM | No         | No          |
| 10xx  | Note Delay                 | PSG/FM/PCM | No         | PSG/FM      |
| 11xx  | Channel Attenuation        | PSG/FM/PCM | Yes        | PSG/FM      |
| 12--  | Reset Channel              | PSG/FM/PCM | No         | PSG/FM      |
| 3Cxx  | Set Table Loop Mode        | PSG/FM     | Yes        | No          |
| 3Dxx  | Set Table Speed            | PSG/FM     | Yes        | No          |
| 3Exx  | Set Table                  | PSG/FM     | Yes        | No          |
| 3Fxx  | Run Macro                  | PSG/FM     | No         | No          |

### Vibrato/Tremolo/Pulsolo (LFOs)

Vibrato, Tremolo, and Pulsolo all work slightly differently than in other trackers.
You can think of it as a triangle ramp with `x` as length and `y` as depth.
Unlike most other trackers, **the lower the value of x, the
faster the speed**. The depth is actually how much to add or subtract from the root value.

Under the covers, `x` is the length of the ramp in one direction. When the effect is
first enabled, the length counter is divided by 2 to make the effect more centered
on the root. One each synth tick, the value of `y` is either added or subjected to the root 
depending on the ramp direction. The direction flips every-time the loop completes.

<pre>
y
|      /\        /\
|     /  \      /  \
|    /    \    /    \
|   /      \  /      \
|  /        \/        \
+----------------------- x
</pre>

This means `x` and `y` are somewhat coupled together.

The reason for this approach is it results in small and efficient code and allows the
potential for other modifications to the LFOs in the future.

#### Vibrato and Pitch Shift Multiplier (PSG)

The PSG vibrato responds to the instrument/channel pitch speed multiplier. This can
be used to great vibratos of a higher range than normal and even create some wild
arcade style sound effects.

### Legato

Continue automation while playing additional notes. For PSG, this means
LFOs, envelopes, etc. all continue to run. For FM it skips the note-off
before note-on.

#### MIDI / Wavetable

When using Legato in MIDI or Wavetable mode, `$01` causes turns off
note-off before note-on. This is similar to a sustain pedal and can
allow using multiple notes at the same time on a single tracker channel.

`$02` enables MIDI Legato. This may disable being able to play multiple
notes on the same MIDI channel depending on how it's implemented. 

### Arp Direction / Speed

First value controls direction. If 0, arp goes up; if 1, arp goes down. 
Second value controls speed, higher values mean slower arp, behaving
much like the song speed.

### Pan

Set panning of the channel.

  * `$00` = Center
  * `$01` = Left
  * `$02` = Right

### Channel Attenuation

Channel attenuation works a bit different when used as an effect vs in 
the volume column. The value specifies the amount of attenuation. 

`$00` is no attenuation and `$3F` would be max for PSG and `$7F` for FM

## Unique Effects

### VERA PSG

Note this list is not yet complete.

| Hex   | Effect                     | Persistent | Implemented |
| ----- | -------------------------- | ---------- | ----------- |
| 40xx  | Frequency Word Low         | No         | Yes (1)     |
| 41xx  | Frequency Word High        | No         | Yes (1)     |
| 42xx  | Pan and Volume             | No         | Yes (1)     |
| 43xx  | Wave and Pulse             | No         | Yes (1)     |
| 44xx  | Wave Width                 | Yes        | Yes         |
| 45xx  | Wave Slide                 | No         | Yes         |
| 46xx  | Wave Sweep Down/Up         | Yes        | Yes         |
| 47xx  | Pulsolo/Wavolo             | Yes        | Yes         |
| 48x-  | Finetune                   | Yes        | Yes         |
| 49xx  | Linear Finetune            | Yes        | Yes         |
| 4Axx  | Pitch Speed Multiplier     | Yes        | Yes         |
| ....  | ......................     | ...        | ...         |
| 51xx  | Vol Env Speed              | Yes        | Yes         |
| ....  | ......................     | ...        | ...         |
| 57xx  | Wave Env Speed             | Yes        | Yes         |
| ....  | ......................     | ...        | ...         |
| 5Dxx  | Pitch Env Speed            | Yes        | Yes         |

1. Effects `$40-$43` are directly mapped to the VERA's channel registers. At present
due to how the synth engine works, they may not work as expected and it's recommended
to use other effects.

#### Finetune

Finetune is signed so `10-70` is finetune up, `F0-80` is finetune down. At present,
the second digit does not have an affect. Finetune triggers on the current note, if
a note is provided with the effect; or the next note played.

Finetune is log, or rather is applied to the MIDI note/semitone prior to being
converted to VERA pitch. This means the tuning will be (close to) the same across
the note range.

#### Linear Finetune

Unlike the above, linear finetune is more precise as it operates directly on the
VERA pitch registers. Unlike regular finetune, however, the tuning will change
with pitch though it supports a wider range than finetune.

Linear finetune is applied after normal finetune so it's possible to use 
both finetunes at the same time if desired.

#### Pitch Speed Multiplier

This sets the pitch speed multiplier for the channel and works the same way as the
pitch speed multiplier of a PSG instrument. It changes the speed of sweeps, glide and 
vibrato are evaluated it. 

The value is how many left-shifts to move the given values.

#### Envelope Speeds

The envelope speed settings work much like the song speed (in vsync mode). 
Higher speed values mean a long delay before evaluating the next 
envelope value.

#### VERA Volume Column Effects

Since the volume range of the VERA is `00-3F`, the upper values
can be used for other things, such as panning, changing the wave
or setting the PWM.

| Range | Function            |
| ------| ------------------- |
| 00-3F | Volume              |
| 40-7F | Channel Attenuation |
| 80-BF | Set PWM             |
| C0    | Pan Center          |
| C1    | Pan Left            |
| C2    | Pan Right           |
| C3    | Set Wave to Pulse   |
| C4    | Set Wave to Saw     |
| C5    | Set Wave to Tri     |
| C6    | Set Wave to Noise   |

The range `40-7F` is used to set attenuation. This value is subtracted
from the channel volume before it is written out to the VERA. This can
be extremely useful if wanting to, for example, use multiple channels
to simulate delay. The attenuation is **always** applied to the volume 
and is global to the channel.

### FM (YM2151)

Effects `$40-57` are directly mapped to the YM2151's channel registers. 
Some effects may change multiple things. Consult 
Chapter 11 of the X16 Reference Docs for how these are organized.
The DreamTracker website keeps a mirror of the reference docs in
mkdocs format [here](https://x16docs.dreamtracker.org/#X16%20Reference%20-%2011%20-%20Sound%20Programming.html).

| Hex   | Effect                            | Persistent  | Implemented |
| ----- | --------------------------------- | ----------- | ----------- |
| 40xx  | M1 Fine Detune and Frequency Mult | Yes         | Yes         |
| 41xx  | M2 Fine Detune and Frequency Mult | Yes         | Yes         |
| 42xx  | C1 Fine Detune and Frequency Mult | Yes         | Yes         |
| 43xx  | C2 Fine Detune and Frequency Mult | Yes         | Yes         |
| 44xx  | M1 Volume                         | Yes         | Yes         |
| 45xx  | M2 Volume                         | Yes         | Yes         |
| 46xx  | C1 Volume                         | Yes         | Yes         |
| 47xx  | C2 Volume                         | Yes         | Yes         |
| 48xx  | M1 Key Scale and Attack           | Yes         | Yes         |
| 49xx  | M2 Key Scale and Attack           | Yes         | Yes         |
| 4Axx  | C1 Key Scale and Attack           | Yes         | Yes         |
| 4Bxx  | C2 Key Scale and Attack           | Yes         | Yes         |
| 4Cxx  | M1 AM Enable and Decay 1 Rate     | Yes         | Yes         |
| 4Dxx  | M2 AM Enable and Decay 1 Rate     | Yes         | Yes         |
| 4Exx  | C1 AM Enable and Decay 1 Rate     | Yes         | Yes         |
| 4Fxx  | C2 AM Enable and Decay 1 Rate     | Yes         | Yes         |
| 50xx  | M1 Coarse Detune and Decay 2 Rate | Yes         | Yes         |
| 51xx  | M2 Coarse Detune and Decay 2 Rate | Yes         | Yes         |
| 52xx  | C1 Coarse Detune and Decay 2 Rate | Yes         | Yes         |
| 53xx  | C2 Coarse Detune and Decay 2 Rate | Yes         | Yes         |
| 54xx  | M1 Sustain and Release            | Yes         | Yes         |
| 55xx  | M2 Sustain and Release            | Yes         | Yes         |
| 56xx  | C1 Sustain and Release            | Yes         | Yes         |
| 57xx  | C2 Sustain and Release            | Yes         | Yes         |
| 58xx  | Pan, Feedback and Algorithm       | Yes         | No          |
| 59xx  | Mod Sense (Vibrato/Tremolo)       | Yes         | No          |
| 5Axx  | LFO Waveform                      | Yes         | Yes         |
| 5Bxx  | LFO Frequency                     | Yes         | Yes         |
| 5Cxx  | LFO AM/PM Depth                   | Yes         | Yes         |
| 5Dxx  | Ch. 7 Noise Control               | Yes         | Yes         |
| 5Exx  | Global To Operator Volume         | Yes         | No          |

#### FM Volume Column Effects

Since the volume range of the YM2151 is `00-7F`, the remaining range
will likely end up being used for channel attenuation. For now values
above `80` don't do anything (though it's recommended not to use them
as they will be allocated to something in the future).

| Range | Function            |
| ------| ------------------- |
| 00-7F | Volume              |

### PCM

TBD

### MIDI / Wavetable

Note that MIDI and Wavetable output share the same channels with the PSG and
need to be enabled. At present, the only way to do this is in the pattern editor
by pressing COMMAND-M (Left ALT-M).

| Hex   | Effect                            | Persistent  | Implemented |
| ----- | --------------------------------- | ----------- | ----------- |
| 40xx  | Set MIDI Channel                  | Yes         | Yes         |
| 41xx  | Set MIDI CC Parameter             | Yes         | Yes         |
| 42xx  | Set MIDI CC Value                 | Yes         | Yes         |

#### MIDI Volume Column Effects

At present, the extra volume range above `$7F` attenuates the channel, much 
like how the PSG works.

| Range | Function            |
| ------| ------------------- |
| 00-7F | Volume              |
| 80-FF | Attenuation         |

## Global Effects

| Hex   | Effect                      | Persistent | Implemented |
| ----- | --------------------------- | ---------- | ----------- |
| C0--  | Stop Song                   | No         | Yes         |
| C1xx  | Song Speed                  | No         | Yes         |
| C2xx  | Pattern Break               | No         | Yes         |
| C3xx  | Jump to Order               | No         | Yes         |
| C4xx  | Jump to Row                 | No         | No          |
| C5xx  | Global Transpose            | No         | Yes         |
| C6xx  | Set BPM (VIA Only)          | No         | No          |
| C7xx  | Set Engine Speed (VIA/MIDI) | No         | No          |
| C8-r  | Change Background Red Value | Yes        | No          |
| C9gb  | Change Background G/B Value | Yes        | No          |

## Slides and Sweeps

Slides are single shot. That is to say, they run once when you put
specify the effect for a channel. If you want the effect to fire 
multiple times, you have to specify it. This is by design because
it allows for intentional coarse stair-step style sounds as well
as slicer/stacatto, among other things.

Sweeps, however, are continual and run until they are turned off
by specifying 00 (for example `0500`). They also run at the same
speed as the sound engine which means they are _much_ smoother and
faster. They can be thought of like a simple linear envelope in this
way. If you want to make a kick-drum from a square-wave, you'll
want to use a sweep.

Some effects combine slide/sweet and direction in one command. This
is because the VERA only supports volume and PWM from 00-3F. We can
use the top bits to add more function. If that sounds complicated,
don't worry, just use the table below: 

| Range | Function         |
| ------| ---------------- |
| 00-3F | Slide/Sweep Down |
| 40-7F | Slide/Sweep Up   |