# Using MIDI and Wavetable 

MIDI and Wavetable output shares the same channels as the VERA PSG channels.
To switch a channel to MIDI or Wavetable mode, press `CTRL-M` in the pattern editor
which cycles through the options.

Eventually an effect will allow for switching the channel type mid-song.

MIDI mode is for dealing with external instruments or effect boxes. Wavetable
more is for the LittleDreamer or another Wavetable add-on card for the MIDI expansion
card (or the emulator if using `-sf2` is enabled).

At present, External MIDI is hardcoded to address `$9FC0` and Wavetable, `$9FC8`.
Eventually this will be configurable so that any of the IO address ranges can be used.

Note that MIDI channels are not hard coupled to DreamTracker channels. To set the MIDI
channel of a tracker channel, use effect `$40`. Multiple tracker channels can use the
same MIDI channel, but be aware some things (like NOTEOFF) would work across those channels.

By default, much like the default behavior of FM, a note-off is sent before note-on. This
allows for playing a melody without having to constantly issue note-release or note-off before
the next note.

This can be changed by setting effect `$07` to either `$01` or `$02`. `$01` is tracker-legato,
which simply turns off the note-off before note-on. `$02` is MIDI legato, which sends a 
Control-Change (CC) to enable it. The behavior depends on the MIDI device being used.

More to come as this is a super new feature to help celebrate the launching of the 
official MIDI card.