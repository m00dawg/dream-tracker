# YM2151 Instruments

The YM2151 is a Yamaha OPM FM synthesizer on a chip which supports
8 channels each with 4-operators that can be arranged in 8 differnt
ways.

FM support was *just* added to DreamTracker so check back often
for more insight.

## Note Release

The default mode is to "auto-release" which will issue a note-release
right before a note-on. This seems to be how most trackers manage it
since otherwise you need to manually send a release. An channel effect
will be provided to disable this, effectively allowing for "legato"
and manual release, which can be done via the pattern data or could
be done via a table effect (once those exist)

## How DreamTracker Handles "Attenuation"

For those that are aware of the YM2151 already and use
it with other trackers (such as Furnace), just be aware that
DreamTracker presents the volume and envelopes as zero being
min (rather than zero being max). This is opposite from the 
YM2151 itself (where zero is max volume for example). 

This was done by design so that the FM and PSG behave the same
way from the perspective of the tracker. Additionally this presents
a more standard view of the ADSDR envelopes.

Internally, the relevant registers are XOR'd before being sent 
to the chip though this is a detail musicians won't need to worry about.

## The Instruments View

One can get to the actual FM Instruments by pressing `F4` twice. Pressing
it once pulls up the PSG Instruments and pressing it again toggles over
to the FM Instruments screen.

![FM Instruments Example](images/fm_instruments1.png)

At first glance there is a lot going on though the modulators all have
the same settings, with M1 having an additional feedback parameter.

The top section is specifies the algorithm, the arrangemnts of which
are along the bottom of the screen as well as panning, and the 
phase modulation and amplitude modulation sensitivity. 

The global volume section specifies the default volume for the instrument
and which operators will use the global volume instead of their own
value. Typically the operators connected to the output would be the
ones you might want to use.

At present, this features doesn't actually work :) So the implementation
may change. In particular it would be useful if the volumes scaled in
relation to the global rather than being absolute.

Moving on, the next sections are for each of the modulators. The YM2151
calls each of the 4 operators M1, M2, C1 and C2. They are color-coded 
to correspond to the algorithms at the bottom fo the screen (apologies
for any folks that are color-blind - I'm not sure if the relative shades
are sufficient or not and this is an open bug to fix).

Within each section you can specify the volume, whether or not to use
amplitude modulation (AM) instead of frequency/phase (FM), key scale, detune, and the ADSDR envelope. M1 has the additional feedback option. 
Increasing this gives the operator an increasingly grungy/noisy sound.

The key-scale parameter controls how the envelope is influenced by the
key (note/pitch) being played.

### ADSDR

Each operator has 5 envelope stages which are, in order:

  * Attack
  * Decay 1
  * Sustain (Sometimes called Decay 1 Level)
  * Decay 2
  * Release

#### Attack ($00-$1F)

Setting attack to `00` results in an instant attack. That is the operator
will come on instantly at whatever the volume is set to. Increasing attack
increasing the length of time it takes fro the operator to ramp up to
the set volume, up to a max setting of `1F`.

#### Decay 1 ($00-$1F)

After the attach phase comes the first decay. Higher values mean a longer
decay.

#### Sustain ($00-0F)

Technically called Decay 1 Level, this is the level of volume after the

Time for bed! Will continue later.

### Detune
Detunes the operator from the channel's main pitch. Values 0 and 4=no detuning. Values 1-3=detune up, 5-7 = detune down.
The amount of detuning varies with pitch. It decreases as the channel's pitch increases.

(From https://github.com/X16Community/x16-docs/blob/master/X16%20Reference%20-%2009%20-%20Sound%20Programming.md)