# VERA Channels

VERA Channels are setup as a structure of arrays. The offset of each array
is the channel number ($00-$10, 00-15). This holds the intended state of 
the channel. Both the row playback and synth tick updates will read and
manipulate the channel.

## Volume and Wave Envelope Flags:

## Pitch Envelope Flags:


```
  .scope vera
    ; Channel registers
    ; Doesn't seem like I can figure out structs 
    ; here either
    channel_instrument:                .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
                                       .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
    ; bit 7: active, rest are NOPs ATM
    channel_flags:                     .byte $00,$00,$00,$00,$00,$00,$00,$00
                                       .byte $00,$00,$00,$00,$00,$00,$00,$00
    channel_note:                      .res $10
    channel_pitch_low:                 .res $10
    channel_pitch_high:                .res $10
    channel_vol:                       .res $10
    channel_wave:                      .res $10
    channel_vol_envelope_num:          .res $10
    channel_vol_envelope_flags:        .res $10
    channel_vol_envelope_length:       .res $10
    channel_vol_envelope_loop_start:   .res $10
    channel_vol_envelope_loop_end:     .res $10
    channel_vol_envelope_position:     .res $10
    channel_wave_envelope_num:         .res $10
    channel_wave_envelope_flags:       .res $10
    channel_wave_envelope_length:      .res $10
    channel_wave_envelope_loop_start:  .res $10
    channel_wave_envelope_loop_end:    .res $10
    channel_wave_envelope_position:    .res $10
    channel_pitch_envelope_num:        .res $10
    channel_pitch_envelope_flags:      .res $10
    channel_pitch_envelope_length:     .res $10
    channel_pitch_envelope_loop_start: .res $10
    channel_pitch_envelope_loop_end:   .res $10
    channel_pitch_envelope_position:   .res $10
  .endscope
```
